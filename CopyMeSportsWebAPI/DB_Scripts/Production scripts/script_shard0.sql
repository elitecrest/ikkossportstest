USE [master]
GO
/****** Object:  Database [ElasticScaleStarterKit_Shard0]    Script Date: 21-10-2016 11:55:08 ******/
CREATE DATABASE [ElasticScaleStarterKit_Shard0]
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [ElasticScaleStarterKit_Shard0].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [ElasticScaleStarterKit_Shard0] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [ElasticScaleStarterKit_Shard0] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [ElasticScaleStarterKit_Shard0] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [ElasticScaleStarterKit_Shard0] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [ElasticScaleStarterKit_Shard0] SET ARITHABORT OFF 
GO
ALTER DATABASE [ElasticScaleStarterKit_Shard0] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [ElasticScaleStarterKit_Shard0] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [ElasticScaleStarterKit_Shard0] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [ElasticScaleStarterKit_Shard0] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [ElasticScaleStarterKit_Shard0] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [ElasticScaleStarterKit_Shard0] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [ElasticScaleStarterKit_Shard0] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [ElasticScaleStarterKit_Shard0] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [ElasticScaleStarterKit_Shard0] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [ElasticScaleStarterKit_Shard0] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [ElasticScaleStarterKit_Shard0] SET  ENABLE_BROKER 
GO
ALTER DATABASE [ElasticScaleStarterKit_Shard0] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [ElasticScaleStarterKit_Shard0] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [ElasticScaleStarterKit_Shard0] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [ElasticScaleStarterKit_Shard0] SET ALLOW_SNAPSHOT_ISOLATION ON 
GO
ALTER DATABASE [ElasticScaleStarterKit_Shard0] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [ElasticScaleStarterKit_Shard0] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [ElasticScaleStarterKit_Shard0] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [ElasticScaleStarterKit_Shard0] SET RECOVERY FULL 
GO
ALTER DATABASE [ElasticScaleStarterKit_Shard0] SET  MULTI_USER 
GO
ALTER DATABASE [ElasticScaleStarterKit_Shard0] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [ElasticScaleStarterKit_Shard0] SET DB_CHAINING OFF 
GO
USE [ElasticScaleStarterKit_Shard0]
GO
/****** Object:  Schema [__ShardManagement]    Script Date: 21-10-2016 11:55:08 ******/
CREATE SCHEMA [__ShardManagement]
GO
/****** Object:  StoredProcedure [__ShardManagement].[spAddShardLocal]    Script Date: 21-10-2016 11:55:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [__ShardManagement].[spAddShardLocal]
@input xml,
@result int output
as
begin
	declare @lsmVersionMajorClient int, 
			@lsmVersionMinorClient int,
			@operationId uniqueidentifier,
			@undo int,
			@shardMapId uniqueidentifier,
			@shardId uniqueidentifier,
			@name nvarchar(50),
			@sm_kind int,
			@sm_keykind int,
			@shardVersion uniqueidentifier,
			@protocol int,
			@serverName nvarchar(128),
			@port int,
			@databaseName nvarchar(128),
			@shardStatus  int,
			@errorMessage nvarchar(max),
			@errorNumber int,
			@errorSeverity int,
			@errorState int,
			@errorLine int,
			@errorProcedure nvarchar(128)
	select 
		@lsmVersionMajorClient = x.value('(LsmVersion/MajorVersion)[1]', 'int'), 
		@lsmVersionMinorClient = x.value('(LsmVersion/MinorVersion)[1]', 'int'),
		@operationId = x.value('(@OperationId)[1]', 'uniqueidentifier'),
		@undo = x.value('(@Undo)[1]', 'int'),
		@shardMapId = x.value('(ShardMap/Id)[1]', 'uniqueidentifier'),
		@name = x.value('(ShardMap/Name)[1]', 'nvarchar(50)'),
		@sm_kind = x.value('(ShardMap/Kind)[1]', 'int'),
		@sm_keykind = x.value('(ShardMap/KeyKind)[1]', 'int'),
		@shardId = x.value('(Shard/Id)[1]', 'uniqueidentifier'),
		@shardVersion = x.value('(Shard/Version)[1]', 'uniqueidentifier'),
		@protocol = x.value('(Shard/Location/Protocol)[1]', 'int'),
		@serverName = x.value('(Shard/Location/ServerName)[1]', 'nvarchar(128)'),
		@port = x.value('(Shard/Location/Port)[1]', 'int'),
		@databaseName = x.value('(Shard/Location/DatabaseName)[1]', 'nvarchar(128)'),
		@shardStatus = x.value('(Shard/Status)[1]', 'int')
	from 
		@input.nodes('/AddShardLocal') as t(x)

	if (@lsmVersionMajorClient is null or @lsmVersionMinorClient is null or @shardMapId is null or @operationId is null or @name is null or @sm_kind is null or @sm_keykind is null or 
		@shardId is null or @shardVersion is null or @protocol is null or @serverName is null or 
		@port is null or @databaseName is null or @shardStatus is null)
		goto Error_MissingParameters;

	if (@lsmVersionMajorClient <> __ShardManagement.fnGetStoreVersionMajorLocal())
		goto Error_LSMVersionMismatch;

	if exists (
		select 
			ShardMapId
		from
			__ShardManagement.ShardMapsLocal
		where
			ShardMapId = @shardMapId and LastOperationId = @operationId)
		goto Success_Exit;

	begin try
		insert into 
			__ShardManagement.ShardMapsLocal 
			(ShardMapId, Name, MapType, KeyType, LastOperationId)
		values 
			(@shardMapId, @name, @sm_kind, @sm_keykind, @operationId)
	end try
	begin catch
	if (@undo != 1)
	begin
		set @errorMessage = error_message();
		set @errorNumber = error_number();
		set @errorSeverity = error_severity();
		set @errorState = error_state();
		set @errorLine = error_line();
		set @errorProcedure  = isnull(error_procedure(), '-');					
			select @errorMessage = N'Error %d, Level %d, State %d, Procedure %s, Line %d, Message: ' + @errorMessage
			raiserror (@errorMessage, @errorSeverity, 1, @errorNumber, @errorSeverity, @errorState, @errorProcedure, @errorLine);
			rollback transaction; -- To avoid extra error message in response.
			goto Error_UnexpectedError;
	end
	end catch

	begin try
		insert into 
			__ShardManagement.ShardsLocal(
			ShardId, 
			Version, 
			ShardMapId, 
			Protocol, 
			ServerName, 
			Port, 
			DatabaseName, 
			Status,
			LastOperationId)
		values (
			@shardId, 
			@shardVersion, 
			@shardMapId,
			@protocol, 
			@serverName, 
			@port, 
			@databaseName, 
			@shardStatus,
			@operationId)
	end try
	begin catch
	if (@undo != 1)
	begin
		set @errorMessage = error_message();
		set @errorNumber = error_number();
		set @errorSeverity = error_severity();
		set @errorState = error_state();
		set @errorLine = error_line();
		set @errorProcedure  = isnull(error_procedure(), '-');
					
			select @errorMessage = N'Error %d, Level %d, State %d, Procedure %s, Line %d, Message: ' + @errorMessage
			raiserror (@errorMessage, @errorSeverity, 1, @errorNumber, @errorSeverity, @errorState, @errorProcedure, @errorLine);
			rollback transaction; -- To avoid extra error message in response.
			goto Error_UnexpectedError;
	end
	end catch 

	goto Success_Exit;

Error_MissingParameters:
	set @result = 50
	exec __ShardManagement.spGetStoreVersionLocalHelper
	goto Exit_Procedure;

Error_LSMVersionMismatch:
	set @result = 51
	exec __ShardManagement.spGetStoreVersionLocalHelper
	goto Exit_Procedure;

Error_UnexpectedError:
	set @result = 53
	goto Exit_Procedure;

Success_Exit:
	set @result = 1
	goto Exit_Procedure;
	
Exit_Procedure:
end	

GO
/****** Object:  StoredProcedure [__ShardManagement].[spBulkOperationShardMappingsLocal]    Script Date: 21-10-2016 11:55:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [__ShardManagement].[spBulkOperationShardMappingsLocal]
@input xml,
@result int output
as
begin
	declare @lsmVersionMajorClient int,
			@lsmVersionMinorClient int,
			@operationId uniqueidentifier,
			@operationCode int,
			@undo int,
			@stepsCount int,
			@shardMapId uniqueidentifier,
			@sm_kind int,
			@shardId uniqueidentifier,
			@shardVersion uniqueidentifier

	select 
		@lsmVersionMajorClient = x.value('(LsmVersion/MajorVersion)[1]', 'int'),
		@lsmVersionMinorClient = x.value('(LsmVersion/MinorVersion)[1]', 'int'),
		@operationId = x.value('(@OperationId)[1]', 'uniqueidentifier'),
		@undo = x.value('(@Undo)[1]', 'int'),
		@stepsCount = x.value('(@StepsCount)[1]', 'int'),
		@shardMapId = x.value('(ShardMap/Id)[1]', 'uniqueidentifier'),
		@shardId = x.value('(Shard/Id)[1]', 'uniqueidentifier'),
		@shardVersion = x.value('(Shard/Version)[1]', 'uniqueidentifier')
	from 
		@input.nodes('/BulkOperationShardMappingsLocal') as t(x)

	if (@lsmVersionMajorClient is null or @lsmVersionMinorClient is null or @operationId is null or @stepsCount is null or @shardMapId is null or @shardId is null or @shardVersion is null)
		goto Error_MissingParameters;

	if (@lsmVersionMajorClient <> __ShardManagement.fnGetStoreVersionMajorLocal())
		goto Error_LSMVersionMismatch;

	if exists (
		select 
			ShardId
		from
			__ShardManagement.ShardsLocal
		where
			ShardMapId = @shardMapId and ShardId = @shardId and Version = @shardVersion and LastOperationId = @operationId)
		goto Success_Exit;

	update __ShardManagement.ShardsLocal
	set
		Version = @shardVersion,
		LastOperationId = @operationId
	where
		ShardMapId = @shardMapId and ShardId = @shardId

	declare	@currentStep xml,
			@stepIndex int = 1,
			@stepType int,
			@stepMappingId uniqueidentifier

	while (@stepIndex <= @stepsCount)
	begin
		select 
			@currentStep = x.query('(./Step[@Id = sql:variable("@stepIndex")])[1]') 
		from 
			@input.nodes('/BulkOperationShardMappingsLocal/Steps') as t(x)

		select
			@stepType = x.value('(@Kind)[1]', 'int'),
			@stepMappingId = x.value('(Mapping/Id)[1]', 'uniqueidentifier')
		from
			@currentStep.nodes('./Step') as t(x)
	
		if (@stepType is null or @stepMappingId is null)
			goto Error_MissingParameters;

		if (@stepType = 1)
		begin
			delete
				__ShardManagement.ShardMappingsLocal
			where
				ShardMapId = @shardMapId and MappingId = @stepMappingId
		end
		else
		if (@stepType = 3)
		begin
			declare @stepMinValue varbinary(128),
					@stepMaxValue varbinary(128),
					@stepMappingStatus int

			select 
				@stepMinValue = convert(varbinary(128), x.value('(Mapping/MinValue)[1]', 'varchar(258)'), 1),
				@stepMaxValue = convert(varbinary(128), x.value('(Mapping/MaxValue[@Null="0"])[1]', 'varchar(258)'), 1),
				@stepMappingStatus = x.value('(Mapping/Status)[1]', 'int')
			from
				@currentStep.nodes('./Step') as t(x)

			if (@stepMinValue is null or @stepMappingStatus is null)
				goto Error_MissingParameters;

			begin try
				insert into
					__ShardManagement.ShardMappingsLocal
					(MappingId, 
					 ShardId, 
					 ShardMapId, 
					 MinValue, 
					 MaxValue, 
					 Status,
					 LastOperationId)
				values
					(@stepMappingId, 
					 @shardId, 
					 @shardMapId, 
					 @stepMinValue, 
					 @stepMaxValue, 
					 @stepMappingStatus,
					 @operationId)
			end try
			begin catch
			if (@undo != 1)
			begin
				declare @errorMessage nvarchar(max) = error_message(),
					@errorNumber int = error_number(),
					@errorSeverity int = error_severity(),
					@errorState int = error_state(),
					@errorLine int = error_line(),
					@errorProcedure nvarchar(128) = isnull(error_procedure(), '-');
					
					select @errorMessage = N'Error %d, Level %d, State %d, Procedure %s, Line %d, Message: ' + @errorMessage
					raiserror (@errorMessage, @errorSeverity, 1, @errorNumber, @errorSeverity, @errorState, @errorProcedure, @errorLine);
					rollback transaction; -- To avoid extra error message in response.
					goto Error_UnexpectedError;
			end
			end catch

			set @stepMinValue = null
			set @stepMaxValue = null
			set @stepMappingStatus = null

		end

		set @stepType = null
		set @stepMappingId = null

		set @stepIndex = @stepIndex + 1
	end

	goto Success_Exit;

Error_MissingParameters:
	set @result = 50
	exec __ShardManagement.spGetStoreVersionLocalHelper
	goto Exit_Procedure;

Error_LSMVersionMismatch:
	set @result = 51
	exec __ShardManagement.spGetStoreVersionLocalHelper
	goto Exit_Procedure;

Error_UnexpectedError:
	set @result = 53
	goto Exit_Procedure;

Success_Exit:
	set @result = 1
	goto Exit_Procedure;
	
Exit_Procedure:
end	

GO
/****** Object:  StoredProcedure [__ShardManagement].[spFindShardMappingByKeyLocal]    Script Date: 21-10-2016 11:55:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [__ShardManagement].[spFindShardMappingByKeyLocal]
@input xml,
@result int output
as
begin
	declare @lsmVersionMajorClient int,
			@lsmVersionMinorClient int,
			@shardMapId uniqueidentifier,
			@keyValue varbinary(128)

	select 
		@lsmVersionMajorClient = x.value('(LsmVersion/MajorVersion)[1]', 'int'),
		@lsmVersionMinorClient = x.value('(LsmVersion/MinorVersion)[1]', 'int'),
		@shardMapId = x.value('(ShardMap/Id)[1]', 'uniqueidentifier'),
		@keyValue = convert(varbinary(128), x.value('(Key/Value)[1]', 'varchar(258)'), 1)
	from 
		@input.nodes('/FindShardMappingByKeyLocal') as t(x)
	
	if (@lsmVersionMajorClient is null or @lsmVersionMinorClient is null or @shardMapId is null or @keyValue is null)
		goto Error_MissingParameters;

	if (@lsmVersionMajorClient <> __ShardManagement.fnGetStoreVersionMajorLocal())
		goto Error_LSMVersionMismatch;

	declare @mapType int

	select 
		@mapType = MapType
	from
		__ShardManagement.ShardMapsLocal
	where
		ShardMapId = @shardMapId

	if (@mapType is null)
		goto Error_ShardMapNotFound;

	if (@mapType = 1)
	begin	
		select
			3, m.MappingId, m.ShardMapId, m.MinValue, m.MaxValue, m.Status, m.LockOwnerId,  -- fields for SqlMapping
			s.ShardId, s.Version, s.ShardMapId, s.Protocol, s.ServerName, s.Port, s.DatabaseName, s.Status -- fields for SqlShard, ShardMapId is repeated here
		from
			__ShardManagement.ShardMappingsLocal m
		join 
			__ShardManagement.ShardsLocal s
		on 
			m.ShardId = s.ShardId
		where
			m.ShardMapId = @shardMapId and 
			m.MinValue = @keyValue
	end
	else
	begin
		select 
			3, m.MappingId, m.ShardMapId, m.MinValue, m.MaxValue, m.Status, m.LockOwnerId,  -- fields for SqlMapping
			s.ShardId, s.Version, s.ShardMapId, s.Protocol, s.ServerName, s.Port, s.DatabaseName, s.Status -- fields for SqlShard, ShardMapId is repeated here
		from 
			__ShardManagement.ShardMappingsLocal m 
		join 
			__ShardManagement.ShardsLocal s 
		on 
			m.ShardId = s.ShardId
		where
			m.ShardMapId = @shardMapId and 
			m.MinValue <= @keyValue and (m.MaxValue is null or m.MaxValue > @keyValue)
	end

	if (@@rowcount = 0)
		goto Error_KeyNotFound;

	set @result = 1
	goto Exit_Procedure;

Error_ShardMapNotFound:
	set @result = 102
	goto Exit_Procedure;

Error_KeyNotFound:
	set @result = 304
	goto Exit_Procedure;

Error_MissingParameters:
	set @result = 50
	exec __ShardManagement.spGetStoreVersionLocalHelper
	goto Exit_Procedure;

Error_LSMVersionMismatch:
	set @result = 51
	exec __ShardManagement.spGetStoreVersionLocalHelper
	goto Exit_Procedure;

Exit_Procedure:
end	

GO
/****** Object:  StoredProcedure [__ShardManagement].[spGetAllShardMappingsLocal]    Script Date: 21-10-2016 11:55:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [__ShardManagement].[spGetAllShardMappingsLocal]
@input xml,
@result int output
as
begin
	declare @lsmVersionMajorClient int, 
			@lsmVersionMinorClient int,
			@shardMapId uniqueidentifier,
			@shardId uniqueidentifier,
			@minValue varbinary(128),
			@maxValue varbinary(128)

	select 
		@lsmVersionMajorClient = x.value('(LsmVersion/MajorVersion)[1]', 'int'),
		@lsmVersionMinorClient = x.value('(LsmVersion/MinorVersion)[1]', 'int'),
		@shardMapId = x.value('(ShardMap/Id)[1]', 'uniqueidentifier'),
		@shardId = x.value('(Shard/Id)[1]', 'uniqueidentifier'),
		@minValue = convert(varbinary(128), x.value('(Range[@Null="0"]/MinValue)[1]', 'varchar(258)'), 1),
		@maxValue = convert(varbinary(128), x.value('(Range[@Null="0"]/MaxValue[@Null="0"])[1]', 'varchar(258)'), 1)
	from 
		@input.nodes('/GetAllShardMappingsLocal') as t(x)

	if (@lsmVersionMajorClient is null or @lsmVersionMinorClient is null or @shardMapId is null or @shardId is null)
		goto Error_MissingParameters;

	if (@lsmVersionMajorClient <> __ShardManagement.fnGetStoreVersionMajorLocal())
		goto Error_LSMVersionMismatch;

	declare @mapType int

	select 
		@mapType = MapType
	from
		__ShardManagement.ShardMapsLocal
	where
		ShardMapId = @shardMapId

	if (@mapType is null)
		goto Error_ShardMapNotFound;

	declare @minValueCalculated varbinary(128) = 0x,
			@maxValueCalculated varbinary(128) = null

	if (@minValue is not null)
		set @minValueCalculated = @minValue

	if (@maxValue is not null)
		set @maxValueCalculated = @maxValue

	if (@mapType = 1)
	begin	
		select 
			3, m.MappingId, m.ShardMapId, m.MinValue, m.MaxValue, m.Status, m.LockOwnerId,  -- fields for SqlMapping
			s.ShardId, s.Version, s.ShardMapId, s.Protocol, s.ServerName, s.Port, s.DatabaseName, s.Status -- fields for SqlShard, ShardMapId is repeated here
		from 
			__ShardManagement.ShardMappingsLocal m 
			join 
			__ShardManagement.ShardsLocal s 
			on 
				m.ShardId = s.ShardId
		where
			m.ShardMapId = @shardMapId and 
			m.ShardId = @shardId and 
			MinValue >= @minValueCalculated and 
			((@maxValueCalculated is null) or (MinValue < @maxValueCalculated))
		order by 
			m.MinValue
	end
	else
	begin
		select 
			3, m.MappingId, m.ShardMapId, m.MinValue, m.MaxValue, m.Status, m.LockOwnerId,  -- fields for SqlMapping
			s.ShardId, s.Version, s.ShardMapId, s.Protocol, s.ServerName, s.Port, s.DatabaseName, s.Status -- fields for SqlShard, ShardMapId is repeated here
		from 
			__ShardManagement.ShardMappingsLocal m 
			join 
			__ShardManagement.ShardsLocal s 
			on 
				m.ShardId = s.ShardId
		where
			m.ShardMapId = @shardMapId and 
			m.ShardId = @shardId and 
			((MaxValue is null) or (MaxValue > @minValueCalculated)) and 
			((@maxValueCalculated is null) or (MinValue < @maxValueCalculated))
		order by 
			m.MinValue
	end

	set @result = 1
	goto Exit_Procedure;

Error_ShardMapNotFound:
	set @result = 102
	goto Exit_Procedure;

Error_MissingParameters:
	set @result = 50
	exec __ShardManagement.spGetStoreVersionLocalHelper
	goto Exit_Procedure;

Error_LSMVersionMismatch:
	set @result = 51
	exec __ShardManagement.spGetStoreVersionLocalHelper
	goto Exit_Procedure;

Exit_Procedure:
end	

GO
/****** Object:  StoredProcedure [__ShardManagement].[spGetAllShardsLocal]    Script Date: 21-10-2016 11:55:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [__ShardManagement].[spGetAllShardsLocal]
@input xml,
@result int output
as
begin
	declare @lsmVersionMajorClient int,
			@lsmVersionMinorClient int

	select 
		@lsmVersionMajorClient = x.value('(LsmVersion/MajorVersion)[1]', 'int'),
		@lsmVersionMinorClient = x.value('(LsmVersion/MinorVersion)[1]', 'int')
	from 
		@input.nodes('/GetAllShardsLocal') as t(x)

	if (@lsmVersionMajorClient is null or @lsmVersionMinorClient is null)
		goto Error_MissingParameters;

	if (@lsmVersionMajorClient <> __ShardManagement.fnGetStoreVersionMajorLocal())
		goto Error_LSMVersionMismatch;

	select 
		1, ShardMapId, Name, MapType, KeyType
	from 
		__ShardManagement.ShardMapsLocal

	select 
		2, ShardId, Version, ShardMapId, Protocol, ServerName, Port, DatabaseName, Status 
	from
		__ShardManagement.ShardsLocal

	goto Success_Exit;

Error_MissingParameters:
	set @result = 50
	exec __ShardManagement.spGetStoreVersionLocalHelper
	goto Exit_Procedure;

Error_LSMVersionMismatch:
	set @result = 51
	exec __ShardManagement.spGetStoreVersionLocalHelper
	goto Exit_Procedure;

Success_Exit:
	set @result = 1
	goto Exit_Procedure;

Exit_Procedure:
end	

GO
/****** Object:  StoredProcedure [__ShardManagement].[spGetStoreVersionLocalHelper]    Script Date: 21-10-2016 11:55:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [__ShardManagement].[spGetStoreVersionLocalHelper]
as
begin
	select
		5, StoreVersionMajor, StoreVersionMinor
	from 
		__ShardManagement.ShardMapManagerLocal
end

GO
/****** Object:  StoredProcedure [__ShardManagement].[spKillSessionsForShardMappingLocal]    Script Date: 21-10-2016 11:55:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [__ShardManagement].[spKillSessionsForShardMappingLocal]
@input xml,
@result int output
as
begin
	declare @lsmVersionMajorClient int,
			@lsmVersionMinorClient int,
			@patternForKill nvarchar(128)

	select 
		@lsmVersionMajorClient = x.value('(LsmVersion/MajorVersion)[1]', 'int'),
		@lsmVersionMinorClient = x.value('(LsmVersion/MinorVersion)[1]', 'int'),
		@patternForKill = x.value('(Pattern)[1]', 'nvarchar(128)')
	from 
		@input.nodes('/KillSessionsForShardMappingLocal') as t(x)

	if (@lsmVersionMajorClient is null or @lsmVersionMinorClient is null or @patternForKill is null)
		goto Error_MissingParameters;

	if (@lsmVersionMajorClient <> __ShardManagement.fnGetStoreVersionMajorLocal())
		goto Error_LSMVersionMismatch;

	declare @tvKillCommands table (spid smallint primary key, commandForKill nvarchar(10))

	insert into 
		@tvKillCommands (spid, commandForKill) 
	values 
		(0, N'')

	insert into 
		@tvKillCommands(spid, commandForKill) 
		select 
			session_id, 'kill ' + convert(nvarchar(10), session_id)
		from 
			sys.dm_exec_sessions 
		where 
			session_id > 50 and program_name like '%' + @patternForKill + '%'

	declare @currentSpid int, 
			@currentCommandForKill nvarchar(10)

	declare @current_error int

	select top 1 
		@currentSpid = spid, 
		@currentCommandForKill = commandForKill 
	from 
		@tvKillCommands 
	order by 
		spid desc

	while (@currentSpid > 0)
	begin
		begin try
			exec (@currentCommandForKill)

			delete 
				@tvKillCommands 
			where 
				spid = @currentSpid

			select top 1 
				@currentSpid = spid, 
				@currentCommandForKill = commandForKill 
			from 
				@tvKillCommands 
			order by 
				spid desc
		end try
		begin catch
			if (error_number() <> 6106)
				goto Error_UnableToKillSessions;
		end catch
	end

	set @result = 1
	goto Exit_Procedure;
	
Error_UnableToKillSessions:
	set @result = 305
	goto Exit_Procedure;

Error_MissingParameters:
	set @result = 50
	exec __ShardManagement.spGetStoreVersionLocalHelper
	goto Exit_Procedure;

Error_LSMVersionMismatch:
	set @result = 51
	exec __ShardManagement.spGetStoreVersionLocalHelper
	goto Exit_Procedure;

Exit_Procedure:
end	

GO
/****** Object:  StoredProcedure [__ShardManagement].[spRemoveShardLocal]    Script Date: 21-10-2016 11:55:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [__ShardManagement].[spRemoveShardLocal]
@input xml,
@result int output
as
begin
	declare @lsmVersionMajorClient int, 
			@lsmVersionMinorClient int,
			@operationId uniqueidentifier,
			@shardMapId uniqueidentifier,
			@shardId uniqueidentifier

	select 
		@lsmVersionMajorClient = x.value('(LsmVersion/MajorVersion)[1]', 'int'),
		@lsmVersionMinorClient = x.value('(LsmVersion/MinorVersion)[1]', 'int'),
		@operationId = x.value('(@OperationId)[1]', 'uniqueidentifier'),
		@shardMapId = x.value('(ShardMap/Id)[1]', 'uniqueidentifier'),
		@shardId = x.value('(Shard/Id)[1]', 'uniqueidentifier')
	from 
		@input.nodes('/RemoveShardLocal') as t(x)

	if (@lsmVersionMajorClient is null or @lsmVersionMinorClient is null or @operationId is null or @shardMapId is null or @shardId is null)
		goto Error_MissingParameters;

	if (@lsmVersionMajorClient <> __ShardManagement.fnGetStoreVersionMajorLocal())
		goto Error_LSMVersionMismatch;

	delete from
		__ShardManagement.ShardsLocal 
	where
		ShardMapId = @shardMapId and ShardId = @shardId

	delete from
		__ShardManagement.ShardMapsLocal 
	where
		ShardMapId = @shardMapId

	set @result = 1
	goto Exit_Procedure;

Error_MissingParameters:
	set @result = 50
	exec __ShardManagement.spGetStoreVersionLocalHelper
	goto Exit_Procedure;

Error_LSMVersionMismatch:
	set @result = 51
	exec __ShardManagement.spGetStoreVersionLocalHelper
	goto Exit_Procedure;

Exit_Procedure:
end	

GO
/****** Object:  StoredProcedure [__ShardManagement].[spUpdateShardLocal]    Script Date: 21-10-2016 11:55:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create procedure [__ShardManagement].[spUpdateShardLocal]
@input xml,
@result int output
as
begin
	declare @lsmVersionMajorClient int, 
			@lsmVersionMinorClient int,
			@operationId uniqueidentifier,
			@shardMapId uniqueidentifier,
			@shardId uniqueidentifier,
			@shardVersion uniqueidentifier,
			@protocol int,
			@serverName nvarchar(128),
			@port int,
			@databaseName nvarchar(128),
			@shardStatus int

	select 
		@lsmVersionMajorClient = x.value('(LsmVersion/MajorVersion)[1]', 'int'),
		@lsmVersionMinorClient = x.value('(LsmVersion/MinorVersion)[1]', 'int'),
		@operationId = x.value('(@OperationId)[1]', 'uniqueidentifier'),
		@shardMapId = x.value('(ShardMap/Id)[1]', 'uniqueidentifier'),
		@shardId = x.value('(Shard/Id)[1]', 'uniqueidentifier'),
		@shardVersion = x.value('(Shard/Version)[1]', 'uniqueidentifier'),
		@protocol = x.value('(Shard/Location/Protocol)[1]', 'int'),
		@serverName = x.value('(Shard/Location/ServerName)[1]', 'nvarchar(128)'),
		@port = x.value('(Shard/Location/Port)[1]', 'int'),
		@databaseName = x.value('(Shard/Location/DatabaseName)[1]', 'nvarchar(128)'),
		@shardStatus = x.value('(Shard/Status)[1]', 'int')
	from 
		@input.nodes('/UpdateShardLocal') as t(x)

	if (@lsmVersionMajorClient is null or @lsmVersionMinorClient is null or @operationId is null or 
		@shardMapId is null or @shardId is null or @shardVersion is null or @shardStatus is null or
		@protocol is null or @serverName is null or @port is null or @databaseName is null)
		goto Error_MissingParameters;

	if (@lsmVersionMajorClient <> __ShardManagement.fnGetStoreVersionMajorLocal())
		goto Error_LSMVersionMismatch;

	update 
		__ShardManagement.ShardsLocal
	set
		Version = @shardVersion,
		Status = @shardStatus,
		Protocol = @protocol,
		ServerName = @serverName,
		Port = @port,
		DatabaseName = @databaseName,
		LastOperationId = @operationId
	where
		ShardMapId = @shardMapId and ShardId = @shardId

	if (@@rowcount = 0)
		goto Error_ShardDoesNotExist;

	set @result = 1
	goto Exit_Procedure;

Error_MissingParameters:
	set @result = 50
	exec __ShardManagement.spGetStoreVersionLocalHelper
	goto Exit_Procedure;

Error_LSMVersionMismatch:
	set @result = 51
	exec __ShardManagement.spGetStoreVersionLocalHelper
	goto Exit_Procedure;

Error_ShardDoesNotExist:
	set @result = 202
	goto Exit_Procedure;

Exit_Procedure:
end	

GO
/****** Object:  StoredProcedure [__ShardManagement].[spValidateShardLocal]    Script Date: 21-10-2016 11:55:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [__ShardManagement].[spValidateShardLocal]
@input xml,
@result int output
as
begin
	declare @lsmVersionMajorClient int,
			@lsmVersionMinorClient int,
			@shardMapId uniqueidentifier,
			@shardId uniqueidentifier,
			@shardVersion uniqueidentifier
	select 
		@lsmVersionMajorClient = x.value('(LsmVersion/MajorVersion)[1]', 'int'), 
		@lsmVersionMinorClient = x.value('(LsmVersion/MinorVersion)[1]', 'int'),
		@shardMapId = x.value('(ShardMapId)[1]', 'uniqueidentifier'),
		@shardId = x.value('(ShardId)[1]', 'uniqueidentifier'),
		@shardVersion = x.value('(ShardVersion)[1]', 'uniqueidentifier')
	from 
		@input.nodes('/ValidateShardLocal') as t(x)

	if (@lsmVersionMajorClient is null or @lsmVersionMinorClient is null or @shardMapId is null or @shardId is null or @shardVersion is null)
		goto Error_MissingParameters;

	if (@lsmVersionMajorClient <> __ShardManagement.fnGetStoreVersionMajorLocal())
		goto Error_LSMVersionMismatch;

	declare @currentShardMapId uniqueidentifier
	
	select 
		@currentShardMapId = ShardMapId 
	from 
		__ShardManagement.ShardMapsLocal
	where 
		ShardMapId = @shardMapId

	if (@currentShardMapId is null)
		goto Error_ShardMapNotFound;

	declare @currentShardVersion uniqueidentifier

	select 
		@currentShardVersion = Version 
	from 
		__ShardManagement.ShardsLocal
	where 
		ShardMapId = @shardMapId and ShardId = @shardId

	if (@currentShardVersion is null)
		goto Error_ShardDoesNotExist;

	if (@currentShardVersion <> @shardVersion)
		goto Error_ShardVersionMismatch;

	set @result = 1
	goto Exit_Procedure;

Error_ShardMapNotFound:
	set @result = 102
	goto Exit_Procedure;

Error_ShardDoesNotExist:
	set @result = 202
	goto Exit_Procedure;

Error_ShardVersionMismatch:
	set @result = 204
	goto Exit_Procedure;

Error_MissingParameters:
	set @result = 50
	exec __ShardManagement.spGetStoreVersionLocalHelper
	goto Exit_Procedure;

Error_LSMVersionMismatch:
	set @result = 51
	exec __ShardManagement.spGetStoreVersionLocalHelper
	goto Exit_Procedure;

Exit_Procedure:
end	

GO
/****** Object:  StoredProcedure [__ShardManagement].[spValidateShardMappingLocal]    Script Date: 21-10-2016 11:55:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [__ShardManagement].[spValidateShardMappingLocal]
@input xml,
@result int output
as
begin
	declare @lsmVersionMajorClient int,
			@lsmVersionMinorClient int,
			@shardMapId uniqueidentifier,
			@mappingId uniqueidentifier

	select
		@lsmVersionMajorClient = x.value('(LsmVersion/MajorVersion)[1]', 'int'),
		@lsmVersionMinorClient = x.value('(LsmVersion/MinorVersion)[1]', 'int'),
		@shardMapId = x.value('(ShardMapId)[1]', 'uniqueidentifier'),
		@mappingId = x.value('(MappingId)[1]', 'uniqueidentifier')
	from
		@input.nodes('/ValidateShardMappingLocal') as t(x)

	if (@lsmVersionMajorClient is null or @lsmVersionMinorClient is null or @shardMapId is null or @mappingId is null)
		goto Error_MissingParameters;

	if (@lsmVersionMajorClient <> __ShardManagement.fnGetStoreVersionMajorLocal())
		goto Error_LSMVersionMismatch;

	declare @currentShardMapId uniqueidentifier
	
	select 
		@currentShardMapId = ShardMapId 
	from 
		__ShardManagement.ShardMapsLocal
	where 
		ShardMapId = @shardMapId

	if (@currentShardMapId is null)
		goto Error_ShardMapNotFound;

	declare @m_status_current int

	select 
		@m_status_current = Status
	from
		__ShardManagement.ShardMappingsLocal
	where
		ShardMapId = @shardMapId and MappingId = @mappingId
			
	if (@m_status_current is null)
		goto Error_MappingDoesNotExist;

	if (@m_status_current <> 1)
		goto Error_MappingIsOffline;

	set @result = 1
	goto Exit_Procedure;

Error_ShardMapNotFound:
	set @result = 102
	goto Exit_Procedure;

Error_MappingDoesNotExist:
	set @result = 301
	goto Exit_Procedure;

Error_MappingIsOffline:
	set @result = 309
	goto Exit_Procedure;

Error_MissingParameters:
	set @result = 50
	exec __ShardManagement.spGetStoreVersionLocalHelper
	goto Exit_Procedure;

Error_LSMVersionMismatch:
	set @result = 51
	exec __ShardManagement.spGetStoreVersionLocalHelper
	goto Exit_Procedure;

Exit_Procedure:
end	

GO
/****** Object:  StoredProcedure [dbo].[AddFeedback]    Script Date: 21-10-2016 11:55:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AddFeedback] --1,'RaviKiran',1,2,3,'Its good app'
	@PhoneID varchar(100),
	@Name [varchar](100),
	@emailaddress varchar(50),
	@Comment [varchar](4096),
	@UserId int,
	@DeviceType varchar(20),
	@UserRating int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @Id int
	insert into [Feedback] ([DeviceId], [Name],[Comment], [Status], [CreatedBy], [CreateDate],UserId,DeviceType,Rating) 
	values (@PhoneID, @Name, @Comment, 1, 1, GETUTCDATE(),@UserId,@DeviceType, @UserRating)
	set @Id = @@Identity
	select @Id as result

END

GO
/****** Object:  StoredProcedure [dbo].[AddPackagesToFavourites]    Script Date: 21-10-2016 11:55:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AddPackagesToFavourites] --7
	-- Add the parameters for the stored procedure here
	@PackageId int,
	@UserId int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @Id int
    -- Insert statements for procedure here
	--set @videoId = (select top 1 VideoId from LibraryIntroVideos where LibraryId = @LibraryId and IsIntroVideo = 1 order by ID desc)
	insert into FavouritePackages(PackageId, UserId, CreatedDate)
	values(@PackageId,@UserId,GetUtcDate())
	set @Id = @@Identity

	update package
	set IsFavourite = 1
	where packageId = @PackageId

	select @Id as result
END










GO
/****** Object:  StoredProcedure [dbo].[AddPayment]    Script Date: 21-10-2016 11:55:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AddPayment] 
	-- Add the parameters for the stored procedure here
	@PackageId int,
	@Token nvarchar(max),
	@DeviceType varchar(100),
	@PaymentType varchar(1024),
	@IsSubscription int,
	@UserId int
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @CurrentDate DateTime
	declare @ExpiryDate DateTime
	declare @Id int
	declare @skuid varchar(100)

	if(@IsSubscription = 2)
	Begin
	set @CurrentDate = (select GetUTCDATE())
	set @ExpiryDate = ( select DateADD(day,30,@CurrentDate))
	set @skuid = (select SKUID from pricingcatalog PC inner join Package P on PC.ID = P.PricingCatalogId where P.PackageId = @PackageId)
	Update Package
	set IsPaid = 1
	where PackageId = @PackageId
	End
	else
	Begin
	set @CurrentDate = (select GetUTCDATE())
	set @ExpiryDate = ( select DateADD(day,30,@CurrentDate))
	set @skuid = (select SKUID from pricingcatalog PC inner join Libraries L on PC.ID = L.PricingCatalogId where L.LibraryId = @PackageId)
	update libraries
	set IsPaid = 1
	where libraryId = @PackageId
	End
	
	insert into paymentinfo(Token, UserId, PackageId, IsSubscription, CreatedDate, ExpiryDate, DeviceType, PaymentType,SKUID)
	values(@Token,@UserId,@PackageId,@IsSubscription,@CurrentDate,@ExpiryDate,@DeviceType,@PaymentType,@skuid)
	--if(@IsSubscription = 1)
	--Begin
	--update libraries
	--set IsPaid = 1
	--where libraryId = @PackageId
	--End
	--else
	--Begin
	--Update Package
	--set IsPaid = 1
	--where PackageId = @PackageId
	--End
	set @Id = @@Identity
	select @Id as result
END







GO
/****** Object:  StoredProcedure [dbo].[AddUserInfo]    Script Date: 21-10-2016 11:55:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AddUserInfo] 
	-- Add the parameters for the stored procedure here
	@userId int,
	@name nvarchar(256),
	@SportId int,
	@email nvarchar(256),
	@facebookId nvarchar(1000),
	@profilepicurl nvarchar(max),
	@Gender int,
	@LoginType int,
	@DeviceId varchar(512),
	@DeviceType int,
	@Password varchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @Id int
    -- Insert statements for procedure here
	insert into Users(UserId, Name, SportId, email, FacebookId, ProfilePicUrl, Gender, LoginType, DeviceId, DeviceType, CreatedDate, CreatedBy, [Password])
	values(@userId,@name, @SportId, @email, @facebookId, @profilepicurl, @Gender, @LoginType, @DeviceId, @DeviceType, GetUtcDate(), @userId, @Password)
	--set @Id  = @@Identity

	select U.UserId, U.Name, U.SportId, U.email, U.ProfilePicUrl, U.FacebookId, U.Gender, U.LoginType, U.DeviceId, S.SportName, U.DeviceType, S.BannerImageUrl, U.[Password]
	 from Users U inner join Sports S  on S.SportId = U.SportId where UserId =  @userId
END





GO
/****** Object:  StoredProcedure [dbo].[AddVideoInfoToPackage]    Script Date: 21-10-2016 11:55:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[AddVideoInfoToPackage]
	-- Add the parameters for the stored procedure here
	@VideoId int,
	@PreVideoId int,
	@PostVideoId int,
	@PackageId int,
	@CreatedBy int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	declare @Id int
	declare @introvideourl1 varchar(1024)
	declare @introvideourl2 varchar(1024)
	set @introvideourl1 = (select VideoURL from Videos where ID = @PreVideoId)
	set @introvideourl2 = (select VideoURL from Videos where ID = @PostVideoId)
insert into PackageVideoInfo(PackageId,trainingVideoId,PreIntroVideoId,PostIntroVideoId,Createddate, CreatedBy)
values(@PackageId, @VideoId, @PreVideoId, @PostVideoId, getutcdate(), @CreatedBy)
set @Id = @@Identity

insert into packageintrovideos(VideoId, PackageId,CreatedDate,IsIntroVideo)
values(@VideoId, @PackageId, GetUtcDate(), 0)

update videos
set IntroVideoUrl = @introvideourl1,
IntroVideoURL2 = @introvideourl2,VideoType = 1
where ID = @VideoId
select @Id as result
END



GO
/****** Object:  StoredProcedure [dbo].[CreateAdminUser]    Script Date: 21-10-2016 11:55:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CreateAdminUser] 
	-- Add the parameters for the stored procedure here
	@Id int,
	@FirstName varchar(100),
	@LastName varchar(100),
	@ChannelName varchar(100),
	@EmailAddress varchar(100),
	@Password varchar(512),
	@SportId int,
	@ProfilePicUrl varchar(512),
	@VideoUrl varchar(1024),
	@ThumbNailUrl varchar(1024),
	@description varchar(512),
	@PricingCatalogId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
declare @userexists int
declare @count int
declare @videoId int
    -- Insert statements for procedure here
	set @count = (select count(*) from Libraries where EmailAddress = @EmailAddress)

	if(@count > 0)
	Begin
	set @Id = 0
	select @count as UserExists, @Id as UserId
	End
	else
	Begin
	insert into Libraries(LibraryName,FirstName, LastName, SportId, EmailAddress,[Password],CreatedDate,IsPackageAvailable,LibraryImageUrl,IsPaid,PricingCatalogId)
	values(@ChannelName,@FirstName,@LastName,@SportId,@EmailAddress,@Password,GetUtcDate(),0,@ProfilePicUrl, 0,@PricingCatalogId)
	set @Id = @@Identity

	insert into videos(MovementName, ActorName, [Format], VideoURL, ThumbNailUrl,[Status], CreatedBy, CreateDate,Playcount,VideoType)
	values('Channel Intro Video', 'Channel', 'mp4',@VideoUrl,@ThumbNailUrl,1,@Id,getutcdate(),1,2)
	set @videoId = @@Identity

	insert into libraryintrovideos(VideoId,LibraryId, CreatedDate, IsIntroVideo, [Description])
	values(@videoId,@Id,GetUtcDate(),1,@description)
	-- Update the sport details

	update sports
	set IsGreyOut = 1
	where sportId = @SportId

	
	select @count as UserExists, @Id as UserId
	End
END


GO
/****** Object:  StoredProcedure [dbo].[CreateAdminVideo]    Script Date: 21-10-2016 11:55:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[CreateAdminVideo] 
	-- Add the parameters for the stored procedure here
	@VideoURL varchar(1024),
	@MomentName varchar(50),
	@CreatedBy int,
	@ThumbNailURL varchar(1024),
	@VideoMode int,
	@Id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	if(@Id > 0)
	Begin
	set @Id = 0
	select @Id as Result
	End
	else
	Begin
	insert into Videos(MovementName, ActorName, [Format], VideoURL, ThumbNailURL, [Status], CreatedBy, CreateDate, PlayCount, videotype, VideoMode)
	values(@MomentName,'Athlete','mp4',@VideoURL,@ThumbNailURL,1,@CreatedBy,GetUtcDate(),30,0, @VideoMode)
	set @Id = @@Identity
	Select @Id as Result
	End
	
END




GO
/****** Object:  StoredProcedure [dbo].[CreatePackage]    Script Date: 21-10-2016 11:55:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CreatePackage]
	-- Add the parameters for the stored procedure here
	@VideoURL varchar(1024),
	@CreatedBy int,
	@ThumbNailURL varchar(1024),
	@PackageName varchar(256),
	@Description nvarchar(max),
	@PricingCatalogId int,
	@Id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @packageId int
	declare @videoId int
    -- Insert statements for procedure here
	insert into package(PackageName,LibraryId,CreatedDate,[Description], PricingCatalogId,IsPaid)
values(@PackageName,@CreatedBy,GetUtcDate(),@Description,@PricingCatalogId,0)

set @packageId = @@Identity

insert into videos(MovementName,[Format],VideoURL,ThumbNailURL,[Status],CreatedBy,CreateDate,PlayCount,VideoType)
values(@PackageName,'mp4',@VideoURL,@ThumbNailURL,1,@CreatedBy,GetUtcDate(),30,3)

set @videoId = @@Identity

insert into packageintrovideos(VideoId,PackageId,CreatedDate,IsIntroVideo)
values(@videoId,@packageId,GetUtcDate(),1)

update libraries
set IsPackageAvailable = 1
where LibraryId = @CreatedBy

select @packageId as result
END


GO
/****** Object:  StoredProcedure [dbo].[DeleteFavouritePackage]    Script Date: 21-10-2016 11:55:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DeleteFavouritePackage] --10,32
	-- Add the parameters for the stored procedure here
	@PackageId int,
	@UserId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
	Delete from favouritepackages where packageId = @PackageId

	update package
	set IsFavourite = 0
	where packageId = @PackageId

	select @PackageId as result
END



GO
/****** Object:  StoredProcedure [dbo].[DeletePackage]    Script Date: 21-10-2016 11:55:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[DeletePackage] 
	-- Add the parameters for the stored procedure here
	@PackageId int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @videoId int
   delete from packageintrovideos where packageId = @PackageId
   delete from package where packageId = @PackageId
END








GO
/****** Object:  StoredProcedure [dbo].[DeleteVideos]    Script Date: 21-10-2016 11:55:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DeleteVideos] --10,32
	-- Add the parameters for the stored procedure here
	@VideoId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @cnt1 int
	declare @cnt2 int
	set @cnt1 = (select count(*) from packageintrovideos where videoId = @VideoId)
	set @cnt2 = (select count(*) from Libraryintrovideos where videoId = @VideoId)
    -- Insert statements for procedure here

	if(@cnt1 = 0 AND @cnt2 = 0)
	Begin
	Delete from Videos where Id = @VideoId
	End
	else if(@cnt1 > 0 AND @cnt2 = 0)
	Begin
	Delete from packageintrovideos where videoId = @videoId
	Delete from Videos where Id = @VideoId
	end
	else if(@cnt1 = 0 AND @cnt2 > 0)
	Begin
	Delete from Libraryintrovideos where videoId = @videoId
	Delete from Videos where Id = @VideoId
	End
	else
	Begin
	Delete from packageintrovideos where videoId = @videoId
	Delete from Libraryintrovideos where videoId = @videoId
	Delete from Videos where Id = @VideoId
	End
	

	select @VideoId as result
END



GO
/****** Object:  StoredProcedure [dbo].[GetAdminUserInfo]    Script Date: 21-10-2016 11:55:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetAdminUserInfo] --'mrkiran@gmail.com','789456',10000,0
	-- Add the parameters for the stored procedure here
	@EmailAddress Varchar(50),
	@Password varchar(50)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @UserCount int
    -- Insert statements for procedure here
	--set @UserCount = (select count(*) from Parents where ChurchId = @ChurchId AND EmailAddress = @EmailAddress AND [Password] = @Password)

	
	select LibraryId, LibraryName as ChannelName,EmailAddress,SportId,FirstName, LastName, [Password] from Libraries where EmailAddress = @EmailAddress AND [Password] = @Password
	
END

--select * from libraries


GO
/****** Object:  StoredProcedure [dbo].[GetAllPackagesVideos]    Script Date: 21-10-2016 11:55:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetAllPackagesVideos]
	-- Add the parameters for the stored procedure here
	@PackageId int,
	@UserId int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @videoId int
    -- Insert statements for procedure here
	--set @videoId = (select top 1 VideoId from LibraryIntroVideos where LibraryId = @LibraryId and IsIntroVideo = 1 order by ID desc)
select P.PackageId, P.PackageName, P.[Description], P.LibraryId, V.[Format],V.URL as VideoURL, 
V.ThumbNailURL, P.CreatedDate from packageintrovideos PV
inner join package P on P.PackageId = PV.PackageId
inner join Video V on V.Id = PV.VideoId
where P.PackageId = @PackageId
END







GO
/****** Object:  StoredProcedure [dbo].[GetAllVideos]    Script Date: 21-10-2016 11:55:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetAllVideos]  
	@UserID [int]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	select v.[ID], c.segment, c.category, c.name as subcategory, v.movementname, 
	v.actorname, v.orientation, v.facing, 
    v.averagerating, v.url, v.thumbnailurl, v.[type],v.PlayCount,isnull(p.videoid,0) as videoId
    from SubCategory c, Video v 
	left join payment p on p.videoid = v.id and p.userid = @UserID
	where v.[status] = 1 and c.[Status] = 1 
	and c.id = v.SubCategoryID and c.id <> 287 	
    order by v.[ID]
    
END




GO
/****** Object:  StoredProcedure [dbo].[GetAllVideosSearch]    Script Date: 21-10-2016 11:55:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetAllVideosSearch] --[GetAllVideosSearch]  0,'Sports','Golf','ALL','',0,10,0
	@UserID [int],
	@Segment  varchar(50) ,
	@Category  varchar(50) ,
	@SubCategory  varchar(50),
	@SearchText varchar(100),
	@Offset [int],
	@max [int],
	@isSubscribed bit

AS
BEGIN
 
	SET NOCOUNT ON;	

 BEGIN tran
	
declare @MaxVar varchar(max)

set  @MaxVar =  ' select v.[ID], c.segment, c.category, c.name as subcategory, v.movementname, v.actorname, v.orientation, v.facing, 
    v.averagerating, v.url, v.thumbnailurl, v.[type],v.PlayCount 
    from SubCategory c, Video v  
	where v.[status] = 1 and c.[Status] = 1 
	and c.id = v.SubCategoryID and c.id <> 287  '

if(@Segment != 'ALL')
set  @MaxVar = @MaxVar + ' and c.Segment LIKE ''%'+ @Segment +'%'''

if(@Category != 'ALL')
set @MaxVar = @MaxVar +' and c.Category LIKE  ''%' + @Category +'%'''

if(@SubCategory != 'ALL')
set @MaxVar = @MaxVar +' and c.Name LIKE ''%' + @SubCategory +'%'''

if(@SearchText != '')
set @MaxVar = @MaxVar + ' and v.movementname LIKE ''%'+ @SearchText +'%'' or v.actorname LIKE ''%'+ @SearchText +'%'''

if(@isSubscribed = 0)
set @MaxVar = @MaxVar + ' and v.[type]=12'

set @MaxVar = @MaxVar +' order by v.[ID] desc'
set @MaxVar = @MaxVar +' OFFSET '+  cast(@Offset as varchar(10))  + ' ROWS FETCH NEXT '+ cast(@max as varchar(10)) +' ROWS ONLY' 
 
exec (@MaxVar)

commit
 
END


GO
/****** Object:  StoredProcedure [dbo].[GetBestTimes]    Script Date: 21-10-2016 11:55:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetBestTimes]  
 @eventid int,
 @age int,
 @gender int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	select * from Besttimes where event_type_id = @eventid and age = @age and gender = @gender
END


GO
/****** Object:  StoredProcedure [dbo].[GetCatalog]    Script Date: 21-10-2016 11:55:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetCatalog] 
	@LastID [int]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
	select [ID], Segment, Category, Name as SubCategory from SubCategory where ID > @LastID and status = 1;
END


GO
/****** Object:  StoredProcedure [dbo].[GetCategories]    Script Date: 21-10-2016 11:55:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetCategories] 
	@Segment [varchar](50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
	select distinct Category from SubCategory where Segment = @Segment and status = 1 order by Category;
END


GO
/****** Object:  StoredProcedure [dbo].[GetChannelIntroVideos]    Script Date: 21-10-2016 11:55:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetChannelIntroVideos]
	-- Add the parameters for the stored procedure here
	@LibraryId int,
	@UserId int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @videoId int
    -- Insert statements for procedure here
	--set @videoId = (select top 1 VideoId from LibraryIntroVideos where LibraryId = @LibraryId and IsIntroVideo = 1 order by ID desc)
	--select v.[ID], v.movementname, 
	--v.actorname, L.[Description],
 --   v.VideoUrl, v.thumbnailurl, v.PlayCount
 --   from Videos v 
	--inner join  LibraryIntroVideos L on L.VideoId = v.Id 
	--inner joinwhere v.Id = @videoId

	select v.[ID], v.movementname, 
	v.actorname, LV.[Description],
    v.VideoUrl, v.thumbnailurl, v.PlayCount, PC.Price, PC.SKUID
    from Libraries L inner join LibraryIntroVideos LV on L.LibraryId = LV.LibraryId
	inner join Videos V on  V.ID = LV.VideoId
	inner join PricingCatalog PC on PC.ID = L.PricingCatalogId
	where LV.LibraryId = @LibraryId AND LV.IsIntroVideo = 1
END
 



GO
/****** Object:  StoredProcedure [dbo].[GetChannelsBySportId]    Script Date: 21-10-2016 11:55:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetChannelsBySportId] 
	-- Add the parameters for the stored procedure here
	@Id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT L.LibraryId, L.LibraryName, L.SportId, L.LibraryImageUrl, L.IsPaid,L.PricingCatalogId, P.Price, P.SKUID from 
	Libraries L inner join PricingCatalog P on L.PricingCatalogId = P.Id
	where sportId = @Id and IsPackageAvailable = 1 order by LibraryName 
END





GO
/****** Object:  StoredProcedure [dbo].[GetDeviceInfo]    Script Date: 21-10-2016 11:55:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetDeviceInfo] 
	-- Add the parameters for the stored procedure here
	@DeviceId varchar(50)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Inserdt statements for procedure here
	SELECT ID, PhoneID, [Type], [Status],CreatedBy,CreateDate from Device where PhoneId = @DeviceId
END




GO
/****** Object:  StoredProcedure [dbo].[GetEventTypes]    Script Date: 21-10-2016 11:55:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetEventTypes]  
@age int ,
@gender int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
	 select et.* from EventTypes et
	 inner join eventage ea on ea.eventid = et.id
	 where ea.age = @age
END


GO
/****** Object:  StoredProcedure [dbo].[GetFavouritePackages]    Script Date: 21-10-2016 11:55:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetFavouritePackages] 
	-- Add the parameters for the stored procedure here
	@UserId int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @videoId int
    -- Insert statements for procedure here
	--set @videoId = (select top 1 VideoId from LibraryIntroVideos where LibraryId = @LibraryId and IsIntroVideo = 1 order by ID desc)
	select Distinct P.PackageId, P.PackageName, P.[Description], P.LibraryId, V.[Format],V.VideoURL as VideoURL, 
V.ThumbNailURL, P.CreatedDate, P.PricingCatalogId, PC.Price,PC.SKUID,P.Rating,P.IsFavourite, P.IsPaid
from package  P inner join PackageIntroVideos PL on P.PackageId = PL.PackageId
inner join videos V on V.Id = PL.VideoId
inner join FavouritePackages FP on FP.PackageId = P.PackageId
inner join PricingCatalog PC on PC.Id = P.PricingCatalogId
where FP.UserId = @UserId and PL.IsIntroVideo=1
END


GO
/****** Object:  StoredProcedure [dbo].[GetFeaturedVideo]    Script Date: 21-10-2016 11:55:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetFeaturedVideo]  --'2015-03-19',1,1,9213
	@Date [datetime],
	@UserID [int],
	@CategoryID [int],
	@deviceId [int]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
   
 		SELECT p.id, p.title, p.name, p.[Description], p.thumbnailurl, p.url, p.[Type],p.[Status],EventStartDate,EventEndDate,
		t.Description ActionType
		from Promotion p
		inner join [type] t on p.type = t.id 
		where 
		--startdate <= @Date and enddate >= @Date and
		 [status] = 1 and [type]=17
 -- End


END












GO
/****** Object:  StoredProcedure [dbo].[GetLibrariesBySportId]    Script Date: 21-10-2016 11:55:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetLibrariesBySportId] 
	-- Add the parameters for the stored procedure here
	@Id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * from Libraries where sportId = @Id and IsPackageAvailable = 1 order by LibraryName 
END




GO
/****** Object:  StoredProcedure [dbo].[GetLibraryIntroVideos]    Script Date: 21-10-2016 11:55:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetLibraryIntroVideos]
	-- Add the parameters for the stored procedure here
	@LibraryId int,
	@UserId int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @videoId int
    -- Insert statements for procedure here
	set @videoId = (select top 1 VideoId from LibraryIntroVideos where LibraryId = @LibraryId and IsIntroVideo = 1 order by ID desc)
	select v.[ID], c.segment, c.category, c.name as subcategory, v.movementname, 
	v.actorname, v.orientation, v.facing, L.[Description],
    v.averagerating, v.url as VideoUrl, v.thumbnailurl, v.[type],v.PlayCount
    from Video v inner join SubCategory c on v.SubCategoryId = c.Id
	inner join  LibraryIntroVideos L on L.VideoId = v.Id where v.Id = @videoId
END







GO
/****** Object:  StoredProcedure [dbo].[GetPackageByPackageId]    Script Date: 21-10-2016 11:55:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetPackageByPackageId] --54
	-- Add the parameters for the stored procedure here
	@PackageId int
--select * from package order by packageid desc
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @videoId int
    -- Insert statements for procedure here
	--set @videoId = (select top 1 VideoId from LibraryIntroVideos where LibraryId = @LibraryId and IsIntroVideo = 1 order by ID desc)
	select  P.PackageId, P.PackageName, P.[Description], P.LibraryId, V.[format], V.VideoURL as VideoURL,
V.ThumbNailURL, P.PricingCatalogId, P.Createddate,V.IntroVideoURL, PC.Price,PC.SKUID
from packageintrovideos  PL inner join Package P on P.packageId = PL.packageId
inner join Videos V on V.Id = PL.VideoId
inner join pricingcatalog PC on PC.Id = P.PricingCatalogId
where PL.PackageId = @PackageId and PL.IsIntroVideo= 1
END

GO
/****** Object:  StoredProcedure [dbo].[GetPackagesByAdminUserId]    Script Date: 21-10-2016 11:55:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO











-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetPackagesByAdminUserId] --1
	-- Add the parameters for the stored procedure here
	@LibraryId int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @videoId int
    -- Insert statements for procedure here
	--set @videoId = (select top 1 VideoId from LibraryIntroVideos where LibraryId = @LibraryId and IsIntroVideo = 1 order by ID desc)
	select P.PackageId, P.PackageName, P.[Description], P.LibraryId, V.[format], V.VideoURL as VideoURL,
V.ThumbNailURL, P.PricingCatalogId, P.Createddate,V.IntroVideoURL, PC.Price,PC.SKUID
from Package P inner join packageintrovideos PL on P.packageId = PL.packageId
inner join Videos V on V.Id = PL.VideoId
inner join pricingcatalog PC on PC.Id = P.PricingCatalogId
where P.LibraryId = @LibraryId and PL.IsIntroVideo=1
END









GO
/****** Object:  StoredProcedure [dbo].[GetPackagesIntroVideos]    Script Date: 21-10-2016 11:55:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetPackagesIntroVideos] --29,6
	-- Add the parameters for the stored procedure here
	@LibraryId int,
	@UserId int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @videoId int
    -- Insert statements for procedure here
	--set @videoId = (select top 1 VideoId from LibraryIntroVideos where LibraryId = @LibraryId and IsIntroVideo = 1 order by ID desc)
select P.PackageId, P.PackageName, P.[Description], P.LibraryId, V.[Format],V.VideoURL as VideoURL, 
V.ThumbNailURL, P.CreatedDate, P.PricingCatalogId, PC.Price, PC.SKUID, isnull(FP.PackageId,0) as IsFavourite, P.Rating,
isnull(Pr.UserId,0) as IsPaid
from package  P inner join PackageIntroVideos PL on P.PackageId = PL.PackageId
inner join videos V on V.Id = PL.VideoId
inner join PricingCatalog PC on PC.Id = P.PricingCatalogId
left join Favouritepackages FP on FP.PackageId = p.PackageId and  FP.userid = @UserId
left join paymentinfo Pr on P.packageId = Pr.PackageId and Pr.UserId = @UserId and Pr.IsSubscription = 2
where P.LibraryId = @LibraryId and PL.IsIntroVideo=1
END


GO
/****** Object:  StoredProcedure [dbo].[GetPayment]    Script Date: 21-10-2016 11:55:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetPayment]  
 @UserId int 
 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	 select * from payment where plantype = 21 and userid = @UserId
	 
END


GO
/****** Object:  StoredProcedure [dbo].[GetPaymentInfo]    Script Date: 21-10-2016 11:55:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetPaymentInfo]
	-- Add the parameters for the stored procedure here
	@PackageId int,
	@UserId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select top 1 * from PaymentInfo  where UserId = @UserId and PackageId = @PackageId order by createddate desc
END



GO
/****** Object:  StoredProcedure [dbo].[GetPendingVideo]    Script Date: 21-10-2016 11:55:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetPendingVideo] 
	@VideoID [int]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
    select v.[ID], c.segment, c.category, c.name as subcategory, v.movementname, v.actorname, v.orientation, v.facing, v.averagerating, v.url, v.thumbnailurl,v.PlayCount
    from SubCategory c, Video v where v.[ID] = @VideoID and v.status = 7 and c.[Status] = 1 and c.id = v.SubCategoryID
    
END


GO
/****** Object:  StoredProcedure [dbo].[GetPromotionalContentById]    Script Date: 21-10-2016 11:55:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetPromotionalContentById] 
@Id int 
 
AS
BEGIN
	 
	SET NOCOUNT ON;
	
	Select p.*,a.Description ActionType  from Promotion p
	inner join  [Type] a on a.id = p.Type
	where p.id = @Id
    
END


GO
/****** Object:  StoredProcedure [dbo].[GetSports]    Script Date: 21-10-2016 11:55:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetSports] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
declare @cnt int
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	set @cnt = 1
	--While @cnt < 16
	--Begin
	--if((select count(*) from libraries where sportid = @cnt) > 0)
	--Begin
	--update sports
	--set IsGreyOut = 1
	--where SportId = @cnt
	--End
	--End
    -- Insert statements for procedure here
	SELECT * from Sports order by SportName
END




GO
/****** Object:  StoredProcedure [dbo].[GetSportsDetails]    Script Date: 21-10-2016 11:55:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetSportsDetails]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @sport1 int
	declare @sport2 int
	declare @sport3 int
	declare @sport4 int
	declare @sport5 int
	declare @sport6 int
	declare @sport7 int
	declare @sport8 int
	declare @sport9 int
	declare @sport10 int
	declare @sport11 int
	declare @sport12 int
declare @sport13 int
declare @sport14 int
	declare @sport15 int

	set @sport1 = (select count(*) from libraries where sportid = 1 AND IspackageAvailable = 1)
	set @sport2 = (select count(*) from libraries where sportid = 2 AND IspackageAvailable = 1)
	set @sport3 = (select count(*) from libraries where sportid = 3 AND IspackageAvailable = 1)
	set @sport4 = (select count(*) from libraries where sportid = 4 AND IspackageAvailable = 1)
	set @sport5 = (select count(*) from libraries where sportid = 5 AND IspackageAvailable = 1)
	set @sport6 = (select count(*) from libraries where sportid = 6 AND IspackageAvailable = 1)
	set @sport7 = (select count(*) from libraries where sportid = 7 AND IspackageAvailable = 1)
	set @sport8 = (select count(*) from libraries where sportid = 8 AND IspackageAvailable = 1)
	set @sport9 = (select count(*) from libraries where sportid = 9 AND IspackageAvailable = 1)
	set @sport10 = (select count(*) from libraries where sportid = 10 AND IspackageAvailable = 1)
	set @sport11 = (select count(*) from libraries where sportid = 11 AND IspackageAvailable = 1)
	set @sport12 = (select count(*) from libraries where sportid = 12 AND IspackageAvailable = 1)
	set @sport13 = (select count(*) from libraries where sportid = 13 AND IspackageAvailable = 1)
	set @sport14 = (select count(*) from libraries where sportid = 14 AND IspackageAvailable = 1)
	set @sport15 = (select count(*) from libraries where sportid = 15 AND IspackageAvailable = 1)
    -- Insert statements for procedure here
	if(@sport1 > 0)
	Begin
	update sports
	set IsGreyOut = 1
	where sportId = 1
	End

	if(@sport2 > 0)
	Begin
	update sports
	set IsGreyOut = 1
	where sportId = 2
	End

	if(@sport3 > 0)
	Begin
	update sports
	set IsGreyOut = 1
	where sportId = 3
	End

	if(@sport4 > 0)
	Begin
	update sports
	set IsGreyOut = 1
	where sportId = 4
	End

	if(@sport5 > 0)
	Begin
	update sports
	set IsGreyOut = 1
	where sportId = 5
	End

	if(@sport6 > 0)
	Begin
	update sports
	set IsGreyOut = 1
	where sportId = 6
	End

	if(@sport7 > 0)
	Begin
	update sports
	set IsGreyOut = 1
	where sportId = 7
	End

	if(@sport8 > 0)
	Begin
	update sports
	set IsGreyOut = 1
	where sportId = 8
	End

	if(@sport9 > 0)
	Begin
	update sports
	set IsGreyOut = 1
	where sportId = 9
	End

	if(@sport10 > 0)
	Begin
	update sports
	set IsGreyOut = 1
	where sportId = 10
	End

	if(@sport11 > 0)
	Begin
	update sports
	set IsGreyOut = 1
	where sportId = 11
	End

	if(@sport12 > 0)
	Begin
	update sports
	set IsGreyOut = 1
	where sportId = 12
	End

	if(@sport13 > 0)
	Begin
	update sports
	set IsGreyOut = 1
	where sportId = 13
	End

	if(@sport14 > 0)
	Begin
	update sports
	set IsGreyOut = 1
	where sportId = 14
	End

	if(@sport15 > 0)
	Begin
	update sports
	set IsGreyOut = 1
	where sportId = 15
	End

	--select SportId, SportName, BannerImageUrl, "SportsImageUrl" =
	--CASE SportsImageUrl
	--   when IsGreyOut = 1 then SportsImageUrl
	--   when IsGreyOut = 0 then SportsImageUrl1
	--END  
	--from Sports

	select * from Sports order by SportName
END





GO
/****** Object:  StoredProcedure [dbo].[GetTrainingPlanVideos]    Script Date: 21-10-2016 11:55:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetTrainingPlanVideos]  
	@TrainingPlanId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
	select v.[ID], c.segment, c.category, c.name as subcategory, v.movementname, v.actorname, v.orientation, v.facing, v.averagerating, v.url, v.thumbnailurl,v.ownerId,v.PlayCount
    from SubCategory c, Video v,TrainingVideos tv where v.[status] = 1 and c.[Status] = 1 and c.id = v.SubCategoryID and tv.videoid = v.id	 
	and tv.TrainingPlanId = @TrainingPlanId

END


GO
/****** Object:  StoredProcedure [dbo].[GetVideos]    Script Date: 21-10-2016 11:55:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetVideos] --25
	-- Add the parameters for the stored procedure here
	@UserId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	select ID, MovementName, ActorName, [Format], VideoURL, ThumbNailURL,[Status], CreateDate, PlayCount, IntroVideoUrl, VideoMode
	from videos where ((createdby = @UserId) and (VideoType = 0 or VideoType = 1))
    -- Insert statements for procedure here
	--SELECT <@Param1, sysname, @p1>, <@Param2, sysname, @p2>
END



GO
/****** Object:  StoredProcedure [dbo].[GetVideosByPackageId]    Script Date: 21-10-2016 11:55:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO













-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetVideosByPackageId]
	-- Add the parameters for the stored procedure here
	@PackageId int,
	@UserId int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @videoId int
    -- Insert statements for procedure here
	--set @videoId = (select top 1 VideoId from LibraryIntroVideos where LibraryId = @LibraryId and IsIntroVideo = 1 order by ID desc)
select V.Id, v.movementname, 
	v.actorname, V.[Format],V.VideoURL as VideoURL, 
V.ThumbNailURL,V.PlayCount, P.CreatedDate, v.IntroVideoUrl, v.IntroVideoUrl2 from packageintrovideos PV
inner join package P on P.PackageId = PV.PackageId
inner join Videos V on V.Id = PV.VideoId
where P.PackageId = @PackageId and PV.IsIntroVideo = 0
END


--select * from Video











GO
/****** Object:  StoredProcedure [dbo].[GetVideoUrlInfo]    Script Date: 21-10-2016 11:55:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetVideoUrlInfo]  
@PackageId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
	select V.VideoURL, V.IntroVideoURL, V.IntroVideoURL2 from Videos V inner join packageintrovideos P on V.Id = P.VideoId where P.PackageId = @PackageId and P.IsIntroVideo=0

END



GO
/****** Object:  StoredProcedure [dbo].[IsPaidChannel]    Script Date: 21-10-2016 11:55:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[IsPaidChannel] 
	-- Add the parameters for the stored procedure here
	@UserId int,
	@ChannelId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT Top 1 * from paymentinfo where PackageId = @ChannelId AND UserId = @UserId AND IsSubscription = 1 order by ID desc
END



GO
/****** Object:  StoredProcedure [dbo].[IsPaidPackage]    Script Date: 21-10-2016 11:55:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[IsPaidPackage] 
	-- Add the parameters for the stored procedure here
	@UserId int,
	@ChannelId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT Top 1 * from paymentinfo where PackageId = @ChannelId AND UserId = @UserId AND IsSubscription = 2 order by ID desc
END



GO
/****** Object:  StoredProcedure [dbo].[MobileUserExists]    Script Date: 21-10-2016 11:55:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[MobileUserExists] --'mrkiran31@gmail.com','123456'
	-- Add the parameters for the stored procedure here
		@EmailAddress Varchar(50),
		@Password varchar(50)
		
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @cnt int
	declare @emailcount int
	declare @pwdcount int
	declare @Id int
	--select * from libraries
	set @cnt =  (select count(*) from Users where  Email = @EmailAddress AND [Password] = @Password)
	if(@cnt > 0)
	Begin
	set @Id = (select UserId from Users where  Email = @EmailAddress AND [Password] = @Password)
	 select  @cnt as result, @Id as UserId
	End
	else
	Begin
	set @emailcount = (select count(*) from Users where Email = @EmailAddress)
	if(@emailcount > 0)
	Begin
	set @cnt = -2
	set @Id = 0
	 select  @cnt as result, @Id as UserId
	End
	else
	Begin
	set @cnt = -1
	set @Id = 0
	select  @cnt as result, @Id as UserId
	End
	End
    -- Insert statements for procedure here
	--SELECT <@Param1, sysname, @p1>, <@Param2, sysname, @p2>
END


GO
/****** Object:  StoredProcedure [dbo].[PackageRating]    Script Date: 21-10-2016 11:55:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PackageRating] --7
	-- Add the parameters for the stored procedure here
	@PackageId int,
	@Rating int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @Id int
    -- Insert statements for procedure here
	--set @videoId = (select top 1 VideoId from LibraryIntroVideos where LibraryId = @LibraryId and IsIntroVideo = 1 order by ID desc)
	Update Package
	set Rating = @Rating
	where PackageId = @PackageId
	

	select @Rating as result
END







GO
/****** Object:  StoredProcedure [dbo].[PricingCatalogPack]    Script Date: 21-10-2016 11:55:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PricingCatalogPack]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM PRICINGCATALOG
END



GO
/****** Object:  StoredProcedure [dbo].[spGetAllUsers]    Script Date: 21-10-2016 11:55:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[spGetAllUsers]

as

begin 

select * from Users
end


GO
/****** Object:  StoredProcedure [dbo].[spGetUser]    Script Date: 21-10-2016 11:55:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE procedure [dbo].[spGetUser] 

@Id int

as

begin

select U.UserId, U.SportId, U.Name, U.email, U.ProfilePicUrl, U.FacebookId, U.Gender, U.LoginType, U.DeviceId, S.SportName, U.DeviceType, S.BannerImageUrl
	 from Users U inner join Sports S  on S.SportId = U.SportId where U.UserId =  @Id
end





GO
/****** Object:  StoredProcedure [dbo].[UpdateAdminVideo]    Script Date: 21-10-2016 11:55:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UpdateAdminVideo] 
	-- Add the parameters for the stored procedure here
	@VideoURL varchar(1024),
	@MomentName varchar(50),
	@CreatedBy int,
	@ThumbNailURL varchar(1024),
	@VideoMode int,
	@Id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	update Videos
	set MovementName =@MomentName, ActorName = 'Athlete',
	 [Format] = 'mp4', VideoURL = @VideoURL, 
	ThumbNailURL = @ThumbNailURL, [Status] = 1,
	VideoMode = @VideoMode,
	 UpdatedBy = @CreatedBy, UpdateDate = GetUtcDate(), PlayCount = 30
	 where Id = @Id
	
	
	Select @Id as Result
	
	
END



GO
/****** Object:  StoredProcedure [dbo].[UpdateLibraryId]    Script Date: 21-10-2016 11:55:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE procedure [dbo].[UpdateLibraryId] 
@UserId int,
--@LibraryId int
@SportId int

as

begin

update users
set SportId = @SportId
where UserId = @UserId

--update libraries
--set sportId = @SportId
--where libraryId = @LibraryId

select U.UserId, U.Name, U.SportId, U.email, U.ProfilePicUrl, U.FacebookId, U.Gender, U.LoginType, U.DeviceId, S.SportName, U.DeviceType, S.BannerImageUrl
	 from Users U inner join Sports S  on S.SportId = U.SportId where UserId =  @UserId

end

--select * from sports





GO
/****** Object:  StoredProcedure [dbo].[UpdatePackage]    Script Date: 21-10-2016 11:55:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UpdatePackage]
	-- Add the parameters for the stored procedure here
	--@VideoURL varchar(1024),
	@CreatedBy int,
	--@ThumbNailURL varchar(1024),
	@PackageName varchar(256),
	@Description nvarchar(max),
	@PricingCatalogId int,
	@Id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
	update package
	set PackageName = @PackageName,
	[Description] = @Description,
	PricingCatalogId = @PricingCatalogId,
	createddate = getutcdate()
	where PackageId = @Id
	
select @Id as result
END







GO
/****** Object:  StoredProcedure [dbo].[UpdateSportId]    Script Date: 21-10-2016 11:55:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





Create procedure [dbo].[UpdateSportId] 
@UserId int,
--@LibraryId int
@SportId int

as

begin

update users
set SportId = @SportId
where UserId = @UserId

--update libraries
--set sportId = @SportId
--where libraryId = @LibraryId

select U.UserId, U.Name, U.SportId, U.email, U.ProfilePicUrl, U.FacebookId, U.Gender, U.LoginType, U.DeviceId, S.SportName, U.DeviceType, S.BannerImageUrl
	 from Users U inner join Sports S  on S.SportId = U.SportId where UserId =  @UserId

end

--select * from sports





GO
/****** Object:  StoredProcedure [dbo].[UserExists]    Script Date: 21-10-2016 11:55:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UserExists] --'mrkiran@hotmail.com','789456'
	-- Add the parameters for the stored procedure here
		@EmailAddress Varchar(50),
		@Password varchar(50)
		
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @cnt int
	declare @emailcount int
	declare @pwdcount int
	--select * from libraries
	set @cnt =  (select count(*) from Libraries where  EmailAddress = @EmailAddress AND [Password] = @Password)
	if(@cnt > 0)
	Begin
	 select  @cnt as result
	End
	else
	Begin
	set @emailcount = (select count(*) from Libraries where EmailAddress = @EmailAddress)
	if(@emailcount > 0)
	Begin
	set @cnt = -2
	 select  @cnt as result
	End
	else
	Begin
	set @cnt = -1
	select  @cnt as result
	End
	End
    -- Insert statements for procedure here
	--SELECT <@Param1, sysname, @p1>, <@Param2, sysname, @p2>
END



GO
/****** Object:  UserDefinedFunction [__ShardManagement].[fnGetStoreVersionMajorLocal]    Script Date: 21-10-2016 11:55:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create function [__ShardManagement].[fnGetStoreVersionMajorLocal]()
returns int
as
begin
	return (select StoreVersionMajor from __ShardManagement.ShardMapManagerLocal)
end

GO
/****** Object:  Table [__ShardManagement].[ShardMapManagerLocal]    Script Date: 21-10-2016 11:55:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [__ShardManagement].[ShardMapManagerLocal](
	[StoreVersionMajor] [int] NOT NULL,
	[StoreVersionMinor] [int] NOT NULL,
 CONSTRAINT [pkShardMapManagerLocal_StoreVersionMajor] PRIMARY KEY CLUSTERED 
(
	[StoreVersionMajor] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [__ShardManagement].[ShardMappingsLocal]    Script Date: 21-10-2016 11:55:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [__ShardManagement].[ShardMappingsLocal](
	[MappingId] [uniqueidentifier] NOT NULL,
	[ShardId] [uniqueidentifier] NOT NULL,
	[ShardMapId] [uniqueidentifier] NOT NULL,
	[MinValue] [varbinary](128) NOT NULL,
	[MaxValue] [varbinary](128) NULL,
	[Status] [int] NOT NULL,
	[LockOwnerId] [uniqueidentifier] NOT NULL DEFAULT ('00000000-0000-0000-0000-000000000000'),
	[LastOperationId] [uniqueidentifier] NOT NULL DEFAULT ('00000000-0000-0000-0000-000000000000'),
 CONSTRAINT [pkShardMappingsLocal_MappingId] PRIMARY KEY CLUSTERED 
(
	[MappingId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [__ShardManagement].[ShardMapsLocal]    Script Date: 21-10-2016 11:55:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [__ShardManagement].[ShardMapsLocal](
	[ShardMapId] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[MapType] [int] NOT NULL,
	[KeyType] [int] NOT NULL,
	[LastOperationId] [uniqueidentifier] NOT NULL DEFAULT ('00000000-0000-0000-0000-000000000000'),
 CONSTRAINT [pkShardMapsLocal_ShardMapId] PRIMARY KEY CLUSTERED 
(
	[ShardMapId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [__ShardManagement].[ShardsLocal]    Script Date: 21-10-2016 11:55:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [__ShardManagement].[ShardsLocal](
	[ShardId] [uniqueidentifier] NOT NULL,
	[Version] [uniqueidentifier] NOT NULL,
	[ShardMapId] [uniqueidentifier] NOT NULL,
	[Protocol] [int] NOT NULL,
	[ServerName] [nvarchar](128) NOT NULL,
	[Port] [int] NOT NULL,
	[DatabaseName] [nvarchar](128) NOT NULL,
	[Status] [int] NOT NULL,
	[LastOperationId] [uniqueidentifier] NOT NULL DEFAULT ('00000000-0000-0000-0000-000000000000'),
 CONSTRAINT [pkShardsLocal_ShardId] PRIMARY KEY CLUSTERED 
(
	[ShardId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[AdminUsers]    Script Date: 21-10-2016 11:55:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AdminUsers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EmailAddress] [varchar](100) NULL,
	[UserName] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[Password] [varchar](512) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BestTimes]    Script Date: 21-10-2016 11:55:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BestTimes](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Value] [nvarchar](50) NOT NULL,
	[Gender] [int] NULL,
	[Stroke] [varchar](51) NULL,
	[Age] [int] NULL,
	[Event_Type_Id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Device]    Script Date: 21-10-2016 11:55:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Device](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PhoneID] [varchar](50) NOT NULL,
	[Type] [int] NOT NULL,
	[Status] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[UpdatedBy] [int] NULL,
	[UpdateDate] [datetime] NULL,
	[isFirst] [bit] NULL
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EventAge]    Script Date: 21-10-2016 11:55:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EventAge](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EventId] [int] NULL,
	[Age] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[EventTypes]    Script Date: 21-10-2016 11:55:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EventTypes](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[SubCategory] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FavouritePackages]    Script Date: 21-10-2016 11:55:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FavouritePackages](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PackageId] [int] NULL,
	[UserId] [int] NULL,
	[CreatedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[Feedback]    Script Date: 21-10-2016 11:55:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Feedback](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NULL,
	[PhoneID] [int] NULL,
	[Question1] [int] NULL,
	[Question2] [int] NULL,
	[Question3] [int] NULL,
	[Comment] [varchar](4096) NULL,
	[Status] [int] NULL,
	[CreatedBy] [int] NULL,
	[CreateDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[UpdateDate] [datetime] NULL,
	[UserId] [int] NULL,
	[DeviceId] [varchar](100) NULL,
	[DeviceType] [varchar](20) NULL,
	[Rating] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Libraries]    Script Date: 21-10-2016 11:55:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Libraries](
	[LibraryId] [int] IDENTITY(1,1) NOT NULL,
	[LibraryName] [nvarchar](256) NOT NULL,
	[SportId] [int] NOT NULL,
	[LibraryImageUrl] [varchar](512) NULL,
	[EmailAddress] [varchar](100) NULL,
	[Password] [varchar](256) NULL,
	[FirstName] [varchar](100) NULL,
	[LastName] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[IsPackageAvailable] [int] NULL,
	[IsPaid] [int] NULL,
	[PricingCatalogId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[LibraryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LibraryIntroVideos]    Script Date: 21-10-2016 11:55:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LibraryIntroVideos](
	[VideoId] [int] NOT NULL,
	[LibraryId] [int] NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime] NULL,
	[IsIntroVideo] [int] NULL,
	[Description] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[LoginType]    Script Date: 21-10-2016 11:55:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LoginType](
	[Id] [int] NOT NULL,
	[LoginType] [varchar](50) NULL
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Package]    Script Date: 21-10-2016 11:55:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Package](
	[PackageId] [int] IDENTITY(1,1) NOT NULL,
	[PackageName] [varchar](256) NULL,
	[LibraryId] [int] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[Description] [nvarchar](max) NULL,
	[PricingCatalogId] [int] NULL,
	[IsFavourite] [int] NULL,
	[Rating] [int] NULL,
	[IsPaid] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[PackageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PackageIntroVideos]    Script Date: 21-10-2016 11:55:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PackageIntroVideos](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[VideoId] [int] NOT NULL,
	[PackageId] [int] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[IsIntroVideo] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[PackageVideoInfo]    Script Date: 21-10-2016 11:55:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PackageVideoInfo](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PackageId] [int] NOT NULL,
	[TrainingVideoId] [int] NOT NULL,
	[PreIntroVideoId] [int] NULL,
	[PostIntroVideoId] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [int] NULL
)

GO
/****** Object:  Table [dbo].[Payment]    Script Date: 21-10-2016 11:55:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Payment](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NULL,
	[PaymentToken] [varchar](max) NULL,
	[DeviceType] [int] NULL,
	[PaymentType] [varchar](50) NULL,
	[VideoId] [int] NULL,
	[ExpiryDate] [datetime] NULL,
	[PlanType] [int] NULL,
	[CreatedBy] [int] NULL,
	[CreateDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PaymentInfo]    Script Date: 21-10-2016 11:55:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PaymentInfo](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Token] [nvarchar](max) NULL,
	[UserId] [int] NULL,
	[PackageId] [int] NULL,
	[IsSubscription] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ExpiryDate] [datetime] NULL,
	[PaymentType] [varchar](1024) NULL,
	[DeviceType] [int] NULL,
	[SKUID] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PricingCatalog]    Script Date: 21-10-2016 11:55:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PricingCatalog](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Price] [varchar](50) NULL,
	[SKUID] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[pricingtype] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Promotion]    Script Date: 21-10-2016 11:55:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Promotion](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](100) NULL,
	[Name] [varchar](100) NULL,
	[Description] [varchar](1024) NULL,
	[ThumbNailURL] [varchar](1024) NULL,
	[URL] [varchar](1024) NULL,
	[Type] [int] NULL,
	[Status] [int] NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[CreateDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[UpdateDate] [datetime] NULL,
	[EventStartDate] [datetime] NULL,
	[EventEndDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sports]    Script Date: 21-10-2016 11:55:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sports](
	[SportId] [int] NOT NULL,
	[SportName] [varchar](50) NULL,
	[SportsImageUrl] [varchar](512) NULL,
	[BannerImageUrl] [varchar](512) NULL,
	[IsGreyOut] [int] NULL,
	[SportsImageUrl1] [varchar](1024) NULL,
 CONSTRAINT [PK_Sports_SportId] PRIMARY KEY CLUSTERED 
(
	[SportId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Status]    Script Date: 21-10-2016 11:55:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Status](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Value] [varchar](50) NULL,
	[CreatedBy] [int] NULL,
	[CreateDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[StrokeTypes]    Script Date: 21-10-2016 11:55:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StrokeTypes](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[SubCategory]    Script Date: 21-10-2016 11:55:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SubCategory](
	[ID] [int] IDENTITY(0,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Category] [varchar](50) NOT NULL,
	[Segment] [varchar](50) NOT NULL,
	[Status] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[UpdatedBy] [int] NULL,
	[UpdateDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TrainingPlan]    Script Date: 21-10-2016 11:55:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TrainingPlan](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Text] [nvarchar](4000) NOT NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[Training_Plan_Template_Id] [int] NULL,
	[Day] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[TrainingPlanTemplate]    Script Date: 21-10-2016 11:55:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TrainingPlanTemplate](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[BestTime1] [int] NOT NULL,
	[BestTime2] [int] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[Event_Type_Id] [int] NULL,
	[Gender] [int] NULL,
	[Age] [varchar](5) NULL,
	[Default] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TrainingVideos]    Script Date: 21-10-2016 11:55:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TrainingVideos](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TrainingPlanId] [int] NOT NULL,
	[VideoId] [int] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[Type]    Script Date: 21-10-2016 11:55:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Type](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Users]    Script Date: 21-10-2016 11:55:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Users](
	[UserId] [int] NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
	[email] [nvarchar](256) NOT NULL,
	[ProfilePicUrl] [nvarchar](max) NOT NULL,
	[FacebookId] [nvarchar](1000) NOT NULL,
	[Gender] [int] NULL,
	[LoginType] [int] NULL,
	[DeviceId] [varchar](512) NULL,
	[DeviceType] [int] NULL,
	[createddate] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[SportId] [int] NULL,
	[Password] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserTrainingPlan]    Script Date: 21-10-2016 11:55:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserTrainingPlan](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[training_plan_id] [int] NOT NULL,
	[user_id] [int] NOT NULL,
	[createdBy] [int] NULL,
	[createdDate] [datetime] NULL,
	[start_date] [datetime] NULL,
	[Best_current_time1] [time](7) NULL,
	[Best_current_time2] [time](7) NULL,
	[question_text] [bit] NULL,
	[question_id ] [int] NULL,
	[record_your_video] [bit] NULL,
	[recorded_video_id] [int] NULL,
	[view_your_results] [bit] NULL,
	[question_ans] [nvarchar](50) NULL,
	[started] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[UserTrainingQuestions]    Script Date: 21-10-2016 11:55:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserTrainingQuestions](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Text] [nvarchar](2000) NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[UserTrainingRecordedVideos]    Script Date: 21-10-2016 11:55:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserTrainingRecordedVideos](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[training_video_id] [int] NOT NULL,
	[recorded_video_id] [int] NULL,
	[createddate] [datetime] NULL,
	[createdby] [int] NULL,
	[userid] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[UserVideos]    Script Date: 21-10-2016 11:55:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserVideos](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MovementName] [varchar](100) NULL,
	[VideoUrl] [varchar](1000) NULL,
	[ThumbNailUrl] [varchar](1000) NULL,
	[ActorName] [varchar](100) NULL,
	[UserId] [int] NULL,
	[PackageId] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL,
	[VideoFormat] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Video]    Script Date: 21-10-2016 11:55:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Video](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ShardID] [int] NOT NULL,
	[SubCategoryID] [int] NOT NULL,
	[MovementName] [varchar](50) NOT NULL,
	[ActorName] [varchar](50) NULL,
	[OwnerID] [int] NOT NULL,
	[Format] [varchar](50) NULL,
	[Speed] [varchar](50) NULL,
	[Quality] [varchar](50) NULL,
	[Size] [int] NULL,
	[Duration] [int] NULL,
	[Orientation] [int] NULL,
	[Facing] [int] NULL,
	[AverageRating] [int] NULL,
	[URL] [varchar](1024) NULL,
	[ThumbNailURL] [varchar](1024) NULL,
	[AccessKey] [varchar](1024) NULL,
	[Type] [int] NOT NULL,
	[Status] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[UpdatedBy] [int] NULL,
	[UpdateDate] [datetime] NULL,
	[PlayCount] [int] NULL,
	[PackageId] [int] NULL,
	[IntroVideoUrl] [varchar](1024) NULL,
	[IntroVideoUrl2] [varchar](1024) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[VideoFlagging]    Script Date: 21-10-2016 11:55:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VideoFlagging](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Video] [int] NOT NULL,
	[User] [int] NOT NULL,
	[FlagId] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[UpdatedBy] [int] NULL,
	[UpdateDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[VideoRating]    Script Date: 21-10-2016 11:55:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VideoRating](
	[ID] [int] IDENTITY(0,1) NOT NULL,
	[Video] [int] NOT NULL,
	[User] [int] NOT NULL,
	[Rating] [int] NOT NULL,
	[Type] [int] NOT NULL,
	[Status] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[UpdatedBy] [int] NULL,
	[UpdateDate] [datetime] NULL
)

GO
/****** Object:  Table [dbo].[Videos]    Script Date: 21-10-2016 11:55:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Videos](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MovementName] [varchar](50) NOT NULL,
	[ActorName] [varchar](50) NULL,
	[Format] [varchar](50) NULL,
	[Duration] [int] NULL,
	[VideoURL] [varchar](1024) NULL,
	[ThumbNailURL] [varchar](1024) NULL,
	[Status] [int] NULL,
	[CreatedBy] [int] NULL,
	[CreateDate] [datetime] NOT NULL,
	[UpdatedBy] [int] NULL,
	[UpdateDate] [datetime] NULL,
	[PlayCount] [int] NULL,
	[IntroVideoUrl] [varchar](1024) NULL,
	[IntroVideoUrl2] [varchar](1024) NULL,
	[VideoType] [int] NULL,
	[VideoMode] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[videoSamples]    Script Date: 21-10-2016 11:55:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[videoSamples](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[VideoUrl] [varchar](512) NULL,
	[CreatedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[VideoTypes]    Script Date: 21-10-2016 11:55:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[VideoTypes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[VideoType] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[VideoUsage]    Script Date: 21-10-2016 11:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VideoUsage](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Video] [int] NULL,
	[User] [int] NULL,
	[Count] [int] NULL,
	[Status] [int] NULL,
	[CreatedBy] [int] NULL,
	[CreateDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[UpdateDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
INSERT [__ShardManagement].[ShardMapManagerLocal] ([StoreVersionMajor], [StoreVersionMinor]) VALUES (1, 2)
INSERT [__ShardManagement].[ShardMappingsLocal] ([MappingId], [ShardId], [ShardMapId], [MinValue], [MaxValue], [Status], [LockOwnerId], [LastOperationId]) VALUES (N'57518a61-da43-4b4e-9aae-5e2cc1555a43', N'7ea4c450-ebc8-4c6a-addc-1b431f07398a', N'd6fcafac-814b-4547-ab64-1f986b044f8e', 0x80000000, 0x80001388, 1, N'00000000-0000-0000-0000-000000000000', N'9437837d-500f-40e4-8ccb-34043e5df973')
INSERT [__ShardManagement].[ShardMapsLocal] ([ShardMapId], [Name], [MapType], [KeyType], [LastOperationId]) VALUES (N'd6fcafac-814b-4547-ab64-1f986b044f8e', N'UserIdShardMap', 2, 1, N'bf543340-3096-4d45-a543-cae483e4ce0a')
INSERT [__ShardManagement].[ShardsLocal] ([ShardId], [Version], [ShardMapId], [Protocol], [ServerName], [Port], [DatabaseName], [Status], [LastOperationId]) VALUES (N'7ea4c450-ebc8-4c6a-addc-1b431f07398a', N'5d01064d-7909-4451-8850-67cf39f2b433', N'd6fcafac-814b-4547-ab64-1f986b044f8e', 0, N'graa3njllc.database.windows.net,1433', 0, N'ElasticScaleStarterKit_Shard0', 1, N'9437837d-500f-40e4-8ccb-34043e5df973')
SET IDENTITY_INSERT [dbo].[Feedback] ON 

INSERT [dbo].[Feedback] ([ID], [Name], [PhoneID], [Question1], [Question2], [Question3], [Comment], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [UserId], [DeviceId], [DeviceType], [Rating]) VALUES (1, N'Crystal Hyd', NULL, NULL, NULL, NULL, N'hi', 1, 1, CAST(N'2016-10-17 12:34:35.087' AS DateTime), NULL, NULL, 2, N'null', N'null', 4)
SET IDENTITY_INSERT [dbo].[Feedback] OFF
SET IDENTITY_INSERT [dbo].[Libraries] ON 

INSERT [dbo].[Libraries] ([LibraryId], [LibraryName], [SportId], [LibraryImageUrl], [EmailAddress], [Password], [FirstName], [LastName], [CreatedDate], [IsPackageAvailable], [IsPaid], [PricingCatalogId]) VALUES (1, N'KING Sports', 13, N'https://ikkosvideos.blob.core.windows.net/imagesstore/7387798670870.png', NULL, NULL, NULL, NULL, NULL, 1, 1, 12)
INSERT [dbo].[Libraries] ([LibraryId], [LibraryName], [SportId], [LibraryImageUrl], [EmailAddress], [Password], [FirstName], [LastName], [CreatedDate], [IsPackageAvailable], [IsPaid], [PricingCatalogId]) VALUES (3, N'Planet Swim', 13, N'https://ikkosmultisports.blob.core.windows.net/asset-a34c59bf-8bdc-43b3-bfa4-95786974a065/fn908798081.png?sv=2012-02-12&sr=c&si=5c6fbea4-e1c7-4a4d-b0eb-1b70d5c79506&sig=OU7%2B2fGwxzrzW4LcosNeILgCaVn5J7G%2Bln6jD30Res8%3D&se=2017-09-15T09%3A48%3A04Z', NULL, NULL, NULL, NULL, NULL, 0, 0, 12)
INSERT [dbo].[Libraries] ([LibraryId], [LibraryName], [SportId], [LibraryImageUrl], [EmailAddress], [Password], [FirstName], [LastName], [CreatedDate], [IsPackageAvailable], [IsPaid], [PricingCatalogId]) VALUES (6, N'Machine Aquatics', 13, N'https://ikkosmultisports.blob.core.windows.net/asset-4437a67d-0c2f-403b-a3ec-79a28a6a1713/fn1701924324.png?sv=2012-02-12&sr=c&si=726a7a4e-4000-418f-a9e7-9fb0020e7a12&sig=p%2B7T%2B76Rgr7DekZJC6441R6MqqTE3P5kMbww09CkoN0%3D&se=2017-09-15T09%3A47%3A35Z', NULL, NULL, NULL, NULL, NULL, 0, 0, 12)
INSERT [dbo].[Libraries] ([LibraryId], [LibraryName], [SportId], [LibraryImageUrl], [EmailAddress], [Password], [FirstName], [LastName], [CreatedDate], [IsPackageAvailable], [IsPaid], [PricingCatalogId]) VALUES (7, N'Kim Brackin', 13, N'https://ikkosmultisports.blob.core.windows.net/asset-76f157b4-c887-4895-a4b3-e21b13a083da/fn217339468.png?sv=2012-02-12&sr=c&si=32cccfc5-9ea7-4fd2-a776-a812a6360bf8&sig=lFDF8nxEx7LeYEJddWe6ohBrQZ9imT3Uz0536LuALy8%3D&se=2017-09-15T09%3A46%3A32Z', N'gmc@ikkostraining.com', N'123456', NULL, NULL, NULL, 1, 0, 12)
INSERT [dbo].[Libraries] ([LibraryId], [LibraryName], [SportId], [LibraryImageUrl], [EmailAddress], [Password], [FirstName], [LastName], [CreatedDate], [IsPackageAvailable], [IsPaid], [PricingCatalogId]) VALUES (8, N'A S C A', 13, N'https://ikkosvideos.blob.core.windows.net/imagesstore/42645449289476.png', NULL, NULL, NULL, NULL, NULL, 1, 0, 12)
INSERT [dbo].[Libraries] ([LibraryId], [LibraryName], [SportId], [LibraryImageUrl], [EmailAddress], [Password], [FirstName], [LastName], [CreatedDate], [IsPackageAvailable], [IsPaid], [PricingCatalogId]) VALUES (9, N'Brackin Elite Swim', 13, N'https://copymesportsmediaservice.blob.core.windows.net/libraries/BrackinElite.png', N'gmc1@ikkostraining.com', N'123456', NULL, NULL, NULL, 0, 0, 12)
INSERT [dbo].[Libraries] ([LibraryId], [LibraryName], [SportId], [LibraryImageUrl], [EmailAddress], [Password], [FirstName], [LastName], [CreatedDate], [IsPackageAvailable], [IsPaid], [PricingCatalogId]) VALUES (10, N'Go Swim', 13, N'https://copymesportsmediaservice.blob.core.windows.net/libraries/goswimlogo.png', NULL, NULL, NULL, NULL, NULL, 0, 0, 12)
INSERT [dbo].[Libraries] ([LibraryId], [LibraryName], [SportId], [LibraryImageUrl], [EmailAddress], [Password], [FirstName], [LastName], [CreatedDate], [IsPackageAvailable], [IsPaid], [PricingCatalogId]) VALUES (11, N'King Aquatics', 13, N'https://ikkosmultisports.blob.core.windows.net/asset-1b1dc2a2-2bd6-49dd-af3a-b3cb6b506f39/fn3285962.png?sv=2012-02-12&sr=c&si=6f0fb532-2bbe-4475-853b-5c76b88b8f80&sig=8p4MVqT0SjeI%2F63r0aQ7s2OhfHP78NDWzHmacfDTWzY%3D&se=2017-09-15T09%3A47%3A04Z', N'mrkiran@hotmail.com', N'123456789', N'Ravi Kiran', N'Manda', CAST(N'2016-09-06 08:51:15.490' AS DateTime), 0, 0, 12)
INSERT [dbo].[Libraries] ([LibraryId], [LibraryName], [SportId], [LibraryImageUrl], [EmailAddress], [Password], [FirstName], [LastName], [CreatedDate], [IsPackageAvailable], [IsPaid], [PricingCatalogId]) VALUES (19, N'Testing Channel', 13, N'https://copymesports.blob.core.windows.net:443/asset-34ad3f7c-035c-4de1-894b-0c284f2dfc25/fn1785952394.png?sv=2012-02-12&sr=c&si=6946f3df-5749-4555-b465-5001721e83b3&sig=UW%2FuLjzkzdUZgNCeOvPgYAP8fyg334XsSNETwmjvgPk%3D&se=2017-09-13T09%3A10%3A52Z', N'copymesportstest@gmail.com', N'123456', N'Admin', N'ASCA', CAST(N'2016-09-08 16:44:45.987' AS DateTime), 0, 0, 12)
INSERT [dbo].[Libraries] ([LibraryId], [LibraryName], [SportId], [LibraryImageUrl], [EmailAddress], [Password], [FirstName], [LastName], [CreatedDate], [IsPackageAvailable], [IsPaid], [PricingCatalogId]) VALUES (20, N'testing', 13, N'', N'mrkiran31@gmail.com', N'123456', N'Rkiran', N'Manda', CAST(N'2016-09-13 06:13:59.010' AS DateTime), 0, 0, 12)
INSERT [dbo].[Libraries] ([LibraryId], [LibraryName], [SportId], [LibraryImageUrl], [EmailAddress], [Password], [FirstName], [LastName], [CreatedDate], [IsPackageAvailable], [IsPaid], [PricingCatalogId]) VALUES (23, N'Testing Channel from portal', 13, N'https://copymesportsmediaservice.blob.core.windows.net/libraries/goswimlogo.png', N'mravikiran@live.com', N'123456', N'Ravi Kiran', N'Manda', CAST(N'2016-09-26 06:05:46.740' AS DateTime), 0, 0, 12)
INSERT [dbo].[Libraries] ([LibraryId], [LibraryName], [SportId], [LibraryImageUrl], [EmailAddress], [Password], [FirstName], [LastName], [CreatedDate], [IsPackageAvailable], [IsPaid], [PricingCatalogId]) VALUES (24, N'Testing Channel from portal', 13, N'https://copymesportsmediaservice.blob.core.windows.net/libraries/goswimlogo.png', N'mrkiran@gmail.com', N'123456', N' Kiran', N'Manda', CAST(N'2016-09-26 06:22:25.340' AS DateTime), 0, 0, 12)
INSERT [dbo].[Libraries] ([LibraryId], [LibraryName], [SportId], [LibraryImageUrl], [EmailAddress], [Password], [FirstName], [LastName], [CreatedDate], [IsPackageAvailable], [IsPaid], [PricingCatalogId]) VALUES (26, N'The Labs USA', 13, N'https://ikkosvideos.blob.core.windows.net/imagesstore/98858180558176.png', NULL, NULL, NULL, NULL, NULL, 0, 0, 12)
INSERT [dbo].[Libraries] ([LibraryId], [LibraryName], [SportId], [LibraryImageUrl], [EmailAddress], [Password], [FirstName], [LastName], [CreatedDate], [IsPackageAvailable], [IsPaid], [PricingCatalogId]) VALUES (29, N'The Labs USA', 1, N'https://ikkosvideos.blob.core.windows.net/imagesstore/98858180558176.png', N'crestelite@gmail.com', N'123456', NULL, NULL, NULL, 1, 0, 12)
INSERT [dbo].[Libraries] ([LibraryId], [LibraryName], [SportId], [LibraryImageUrl], [EmailAddress], [Password], [FirstName], [LastName], [CreatedDate], [IsPackageAvailable], [IsPaid], [PricingCatalogId]) VALUES (30, N'test', 13, N'https://ikkosmultisports.blob.core.windows.net:443/asset-117135da-450d-408b-af14-ede30d070c79/fn793200831.jpeg?sv=2012-02-12&sr=c&si=9c32e474-d8a2-493b-8a93-1916082c30b6&sig=fr2b%2BmpoxjZkbkd3%2BUlOoUyMVR1sOQ2DF7j1tXB68dA%3D&se=2017-10-12T13%3A00%3A31Z', N'test@gmail.com', N'123456', N'test', N'test', CAST(N'2016-10-12 13:00:54.203' AS DateTime), 0, 0, 12)
SET IDENTITY_INSERT [dbo].[Libraries] OFF
SET IDENTITY_INSERT [dbo].[LibraryIntroVideos] ON 

INSERT [dbo].[LibraryIntroVideos] ([VideoId], [LibraryId], [Id], [CreatedDate], [IsIntroVideo], [Description]) VALUES (7028, 7, 1, CAST(N'2016-08-19 09:05:14.627' AS DateTime), 1, N'Welcome to your first day of greatness.We''re here to help you get the best out of you. Stick with us, and we''ll be a great team! Watching the learning video of the day before you sleep helps your brain process the information faster and more deeply. Watching in the morning reinforces that processing to speed your improvement. Committing to these few minutes is the easiest thing you can do to get better! 30 minutes before bed: Backstroke front. On the next page, select the large ''GO'' button to begin learning')
INSERT [dbo].[LibraryIntroVideos] ([VideoId], [LibraryId], [Id], [CreatedDate], [IsIntroVideo], [Description]) VALUES (3003, 3, 2, CAST(N'2016-08-19 09:05:51.547' AS DateTime), 1, N'Take a look at the videos in the home screen every day. They cover lots of good information and are short and sweet. You can usually learn something great. Tip from Amanda Beard, 4x US Olympian, medalist and former world record holder in breaststroke. Talking about breaststroke: "You want to maximize each stroke, to do this I pretend to reach for my ears with my shoulders, this allows me to get the most out of each stroke. Morning, within 30 minutes after you wake up: backstroke front POOL: backstroke front Within 30 minutes before you go to bed: backstroke side')
INSERT [dbo].[LibraryIntroVideos] ([VideoId], [LibraryId], [Id], [CreatedDate], [IsIntroVideo], [Description]) VALUES (3032, 6, 3, CAST(N'2016-08-19 09:06:43.700' AS DateTime), 1, N'Determination is more important than perceived talent. Studies prove over and over that working towards a goal, intelligently, and with purpose is the way to success. Determination with great strategy wins. Tip from Sean Hutchison, 2008 US Olympic Coach and IKKOS CEO: Learn to do all of the strokes well. The coordination you gain will make you a much greater athlete, which, in turn, will make you better at your specific stroke. Morning, within 30 minutes after you wake up: backstroke side POOL: backstroke front Within 30 minutes before you go to bed: backstroke front')
INSERT [dbo].[LibraryIntroVideos] ([VideoId], [LibraryId], [Id], [CreatedDate], [IsIntroVideo], [Description]) VALUES (7000, 8, 4, CAST(N'2016-09-02 07:28:26.940' AS DateTime), 1, N'Thank you for visiting the ASCA Channel. Our channel has been designed to provide coaches and swimming instructors with a unique teaching tool aiding on the development of your learn-to-swim program.

Here you will find a curriculum of skills deployed through an innovative movement learning technology called IKKOS. Whether you are an elite or recreational athlete or coach, our proprietary learn-to-swim curriculum can be now accessed in a simple and effective way to help YOU become the best you can be.

Click on the Start Learning tab below and begin your journey with us!')
INSERT [dbo].[LibraryIntroVideos] ([VideoId], [LibraryId], [Id], [CreatedDate], [IsIntroVideo], [Description]) VALUES (2587, 8, 5, CAST(N'2016-09-02 07:29:08.267' AS DateTime), 0, N'Guy Edson walks you through the fast path to a a world class Freestyle kick.')
INSERT [dbo].[LibraryIntroVideos] ([VideoId], [LibraryId], [Id], [CreatedDate], [IsIntroVideo], [Description]) VALUES (4380, 9, 6, CAST(N'2016-09-02 07:30:24.183' AS DateTime), 1, N'Kim Brackin teaches Crossover turn on your back.')
INSERT [dbo].[LibraryIntroVideos] ([VideoId], [LibraryId], [Id], [CreatedDate], [IsIntroVideo], [Description]) VALUES (3032, 9, 7, CAST(N'2016-09-02 07:30:24.190' AS DateTime), 0, N'Kim Brackin walks you through the rotation and Exit phase of the Backstroke Crossover turn.')
INSERT [dbo].[LibraryIntroVideos] ([VideoId], [LibraryId], [Id], [CreatedDate], [IsIntroVideo], [Description]) VALUES (3037, 10, 8, CAST(N'2016-09-02 07:31:14.977' AS DateTime), 1, N'Glenn Mills teaching world class butterfly technique.')
INSERT [dbo].[LibraryIntroVideos] ([VideoId], [LibraryId], [Id], [CreatedDate], [IsIntroVideo], [Description]) VALUES (7003, 10, 9, CAST(N'2016-09-02 07:31:14.980' AS DateTime), 0, N'Kim Brackin walks you through the rotation and Exit phase of the Backstroke Crossover turn.')
INSERT [dbo].[LibraryIntroVideos] ([VideoId], [LibraryId], [Id], [CreatedDate], [IsIntroVideo], [Description]) VALUES (6998, 1, 10, CAST(N'2016-09-16 12:00:48.280' AS DateTime), 1, N'Welcome to the King Aquatics IKKOS Channel. Here you will find all of the secrets  to develop great swimming technique.
Thank you for visiting our channel and I hope you enjoy your time with us!')
INSERT [dbo].[LibraryIntroVideos] ([VideoId], [LibraryId], [Id], [CreatedDate], [IsIntroVideo], [Description]) VALUES (7016, 23, 11, CAST(N'2016-09-26 06:05:46.800' AS DateTime), 1, N'Testing Packages')
INSERT [dbo].[LibraryIntroVideos] ([VideoId], [LibraryId], [Id], [CreatedDate], [IsIntroVideo], [Description]) VALUES (7017, 24, 12, CAST(N'2016-09-26 06:22:25.403' AS DateTime), 1, N'Testing from portal')
INSERT [dbo].[LibraryIntroVideos] ([VideoId], [LibraryId], [Id], [CreatedDate], [IsIntroVideo], [Description]) VALUES (7026, 29, 14, CAST(N'2016-10-10 11:12:41.613' AS DateTime), 1, N'The Labs is a unique, holistic, data and science-based training facility that offers the best of land and swim training. The Labs utilizes expert coaching, leading-edge technology, state-of-art equipment and data feedback in a training environment that makes the very most of each athlete’s time and energy from beginners to elite and professional competitors.')
INSERT [dbo].[LibraryIntroVideos] ([VideoId], [LibraryId], [Id], [CreatedDate], [IsIntroVideo], [Description]) VALUES (7044, 30, 15, CAST(N'2016-10-12 13:00:54.220' AS DateTime), 1, N'testig')
SET IDENTITY_INSERT [dbo].[LibraryIntroVideos] OFF
INSERT [dbo].[LoginType] ([Id], [LoginType]) VALUES (1, N'Facebook')
INSERT [dbo].[LoginType] ([Id], [LoginType]) VALUES (2, N'UserEmail')
INSERT [dbo].[LoginType] ([Id], [LoginType]) VALUES (3, N'Twitter')
INSERT [dbo].[LoginType] ([Id], [LoginType]) VALUES (4, N'Instagram')
SET IDENTITY_INSERT [dbo].[Package] ON 

INSERT [dbo].[Package] ([PackageId], [PackageName], [LibraryId], [CreatedDate], [Description], [PricingCatalogId], [IsFavourite], [Rating], [IsPaid]) VALUES (3, N'Advanced Pack', 11, CAST(N'2016-08-19 09:47:36.000' AS DateTime), N'Breaststroke, Where it says "pool" below, that means to do the full IKKOS protocol at the pool. The other items referencing sleep are done at home, going through the first step of the IKKOS method - watching the audiovisual with your earbuds in. POOL: Breaststroke side 30 minutes before bed: Breaststroke side On the next page, select the large ''PLAY'' button to begin learning.', 5, 0, NULL, 0)
INSERT [dbo].[Package] ([PackageId], [PackageName], [LibraryId], [CreatedDate], [Description], [PricingCatalogId], [IsFavourite], [Rating], [IsPaid]) VALUES (4, N'Expert Pack', 6, CAST(N'2016-08-19 09:47:46.030' AS DateTime), N'Backstroke Day 3! Many people would''ve given up by now. You are serious about getting better! We will get along great. Keep focusing while you learn. Keep from talking to yourself as you watch and listen to the videos. The more you learn to relax and watch, the better you will get. Tip: "In addition to regular IKKOS learning, we often have swimmers watch a skill right before we get in for warm-up. Morning, within 30 minutes after you wake up: Backstroke side POOL: OFF Within 30 minutes before you go to bed: OFF Press the ''Go'' button on the next screen to get started!', 5, 0, NULL, 0)
INSERT [dbo].[Package] ([PackageId], [PackageName], [LibraryId], [CreatedDate], [Description], [PricingCatalogId], [IsFavourite], [Rating], [IsPaid]) VALUES (5, N'Basic', 4, CAST(N'2016-08-23 09:27:50.807' AS DateTime), N'Welcome to your first day of greatness.We''re here to help you get the best out of you. Stick with us, and we''ll be a great team! Watching the learning video of the day before you sleep helps your brain process the information faster and more deeply. Watching in the morning reinforces that processing to speed your improvement. Committing to these few minutes is the easiest thing you can do to get better! 30 minutes before bed: Backstroke front. On the next page, select the large button to begin learning', 11, 0, NULL, 0)
INSERT [dbo].[Package] ([PackageId], [PackageName], [LibraryId], [CreatedDate], [Description], [PricingCatalogId], [IsFavourite], [Rating], [IsPaid]) VALUES (6, N'Learning Freestyle Pull', 8, CAST(N'2016-08-23 09:28:11.953' AS DateTime), N'John Leonard teaches you all the secrets to a better Freestyle pull', 11, 0, NULL, 0)
INSERT [dbo].[Package] ([PackageId], [PackageName], [LibraryId], [CreatedDate], [Description], [PricingCatalogId], [IsFavourite], [Rating], [IsPaid]) VALUES (7, N'Mastering Freestyle Kick', 8, CAST(N'2016-08-23 09:28:30.527' AS DateTime), N'Guy Edson walks you through the fast path to a a world class', 1, 0, NULL, 1)
INSERT [dbo].[Package] ([PackageId], [PackageName], [LibraryId], [CreatedDate], [Description], [PricingCatalogId], [IsFavourite], [Rating], [IsPaid]) VALUES (8, N'Learning the Backstroke Pull', 1, CAST(N'2016-08-23 09:28:35.720' AS DateTime), N'Welcome to the King Aquatics IKKOS Channel. Here you will find all of the secrets  to develop great swimming technique.
Thank you for visiting our channel and I hope you enjoy your time with us!', 11, 0, 5, 1)
INSERT [dbo].[Package] ([PackageId], [PackageName], [LibraryId], [CreatedDate], [Description], [PricingCatalogId], [IsFavourite], [Rating], [IsPaid]) VALUES (9, N'Backstroke Crossover Turn Approach', 9, CAST(N'2016-08-23 09:29:50.237' AS DateTime), N'Kim Brackin teaches Crossover turn on your back', 11, 0, NULL, 0)
INSERT [dbo].[Package] ([PackageId], [PackageName], [LibraryId], [CreatedDate], [Description], [PricingCatalogId], [IsFavourite], [Rating], [IsPaid]) VALUES (10, N'Backstroke Crossover Rotation and Exit', 9, CAST(N'2016-08-23 09:29:57.900' AS DateTime), N'Kim Brackin walks you through the rotation and Exit phase of the Backstroke Crossover turn.', 2, 0, NULL, 0)
INSERT [dbo].[Package] ([PackageId], [PackageName], [LibraryId], [CreatedDate], [Description], [PricingCatalogId], [IsFavourite], [Rating], [IsPaid]) VALUES (11, N'Butterfly Stroke Technique', 10, CAST(N'2016-08-23 09:30:03.753' AS DateTime), N'Glenn Mills teaching world class butterfly technique.', 11, 0, NULL, 0)
INSERT [dbo].[Package] ([PackageId], [PackageName], [LibraryId], [CreatedDate], [Description], [PricingCatalogId], [IsFavourite], [Rating], [IsPaid]) VALUES (12, N'Swimming Start', 10, CAST(N'2016-08-23 09:30:08.700' AS DateTime), N'Learn the best start technique with Glenn Mills.', 2, 0, NULL, 0)
INSERT [dbo].[Package] ([PackageId], [PackageName], [LibraryId], [CreatedDate], [Description], [PricingCatalogId], [IsFavourite], [Rating], [IsPaid]) VALUES (13, N'Blowing Bubbles', 8, CAST(N'2016-09-09 07:16:39.460' AS DateTime), N'In shallow water, hold your breath, then crouch down so your head gets under water. Stay in that position for a few seconds, then rise up. but exhale under water through the nose so you blow bubbles.', 2, 0, 4, 1)
INSERT [dbo].[Package] ([PackageId], [PackageName], [LibraryId], [CreatedDate], [Description], [PricingCatalogId], [IsFavourite], [Rating], [IsPaid]) VALUES (14, N'test package', 11, CAST(N'2016-09-14 12:21:22.957' AS DateTime), N'Testing packages', 1, 0, NULL, 0)
INSERT [dbo].[Package] ([PackageId], [PackageName], [LibraryId], [CreatedDate], [Description], [PricingCatalogId], [IsFavourite], [Rating], [IsPaid]) VALUES (15, N'testing packages', 11, CAST(N'2016-09-14 12:25:43.690' AS DateTime), N'Testing packages', 1, 0, NULL, 0)
INSERT [dbo].[Package] ([PackageId], [PackageName], [LibraryId], [CreatedDate], [Description], [PricingCatalogId], [IsFavourite], [Rating], [IsPaid]) VALUES (16, N'lohit package', 19, CAST(N'2016-09-16 08:42:01.447' AS DateTime), N'testing package', 5, 0, 5, 0)
INSERT [dbo].[Package] ([PackageId], [PackageName], [LibraryId], [CreatedDate], [Description], [PricingCatalogId], [IsFavourite], [Rating], [IsPaid]) VALUES (17, N'asdasd', 19, CAST(N'2016-09-20 07:21:21.657' AS DateTime), N'adad', 5, 0, 3, 0)
INSERT [dbo].[Package] ([PackageId], [PackageName], [LibraryId], [CreatedDate], [Description], [PricingCatalogId], [IsFavourite], [Rating], [IsPaid]) VALUES (18, N'asdasd', 19, CAST(N'2016-09-20 07:23:21.840' AS DateTime), N'adada', 3, 0, NULL, 0)
INSERT [dbo].[Package] ([PackageId], [PackageName], [LibraryId], [CreatedDate], [Description], [PricingCatalogId], [IsFavourite], [Rating], [IsPaid]) VALUES (23, N'testing package', 19, CAST(N'2016-09-24 09:43:44.867' AS DateTime), N'testing for package', 4, 0, NULL, 0)
INSERT [dbo].[Package] ([PackageId], [PackageName], [LibraryId], [CreatedDate], [Description], [PricingCatalogId], [IsFavourite], [Rating], [IsPaid]) VALUES (24, N'test', 19, CAST(N'2016-09-24 09:57:06.300' AS DateTime), N'test', 4, 0, 4, 0)
INSERT [dbo].[Package] ([PackageId], [PackageName], [LibraryId], [CreatedDate], [Description], [PricingCatalogId], [IsFavourite], [Rating], [IsPaid]) VALUES (25, N'', 0, CAST(N'2016-09-24 10:07:20.263' AS DateTime), N'', 4, 0, NULL, 0)
INSERT [dbo].[Package] ([PackageId], [PackageName], [LibraryId], [CreatedDate], [Description], [PricingCatalogId], [IsFavourite], [Rating], [IsPaid]) VALUES (26, N'TestingIkkosAdmin', 19, CAST(N'2016-09-24 10:13:55.947' AS DateTime), N'Test testing ikkos admin portal', 4, 0, NULL, 0)
INSERT [dbo].[Package] ([PackageId], [PackageName], [LibraryId], [CreatedDate], [Description], [PricingCatalogId], [IsFavourite], [Rating], [IsPaid]) VALUES (27, N'testing volley', 19, CAST(N'2016-09-26 08:29:42.573' AS DateTime), N'basic volley ball training', 4, 0, 4, 0)
INSERT [dbo].[Package] ([PackageId], [PackageName], [LibraryId], [CreatedDate], [Description], [PricingCatalogId], [IsFavourite], [Rating], [IsPaid]) VALUES (28, N'basic volley package', 25, CAST(N'2016-09-26 09:02:22.030' AS DateTime), N'test volley', 10, 0, NULL, 0)
INSERT [dbo].[Package] ([PackageId], [PackageName], [LibraryId], [CreatedDate], [Description], [PricingCatalogId], [IsFavourite], [Rating], [IsPaid]) VALUES (29, N'basic volley package22', 19, CAST(N'2016-09-26 09:07:57.257' AS DateTime), N'test volley basic 2', 8, 0, NULL, 0)
INSERT [dbo].[Package] ([PackageId], [PackageName], [LibraryId], [CreatedDate], [Description], [PricingCatalogId], [IsFavourite], [Rating], [IsPaid]) VALUES (30, N'basic volley package23', 25, CAST(N'2016-09-26 09:10:13.367' AS DateTime), N'test 23', 9, 0, NULL, 0)
INSERT [dbo].[Package] ([PackageId], [PackageName], [LibraryId], [CreatedDate], [Description], [PricingCatalogId], [IsFavourite], [Rating], [IsPaid]) VALUES (31, N'Box Squat Front', 29, CAST(N'2016-10-10 11:48:16.717' AS DateTime), N'Swim labs is a unique swim training facility designed to teach swimmers of all ages and abilities – beginners to competitive athletes – the absolute best mechanics for water safety and swimming success. ', 2, 0, 5, 0)
INSERT [dbo].[Package] ([PackageId], [PackageName], [LibraryId], [CreatedDate], [Description], [PricingCatalogId], [IsFavourite], [Rating], [IsPaid]) VALUES (32, N'Box Squat Front', 26, CAST(N'2016-10-10 11:52:39.440' AS DateTime), N'Swim labs is a unique swim training facility designed to teach swimmers of all ages and abilities – beginners to competitive athletes – the absolute best mechanics for water safety and swimming success. ', 2, 0, NULL, 0)
INSERT [dbo].[Package] ([PackageId], [PackageName], [LibraryId], [CreatedDate], [Description], [PricingCatalogId], [IsFavourite], [Rating], [IsPaid]) VALUES (33, N'Turn Front  and Side', 7, CAST(N'2016-10-10 12:30:14.040' AS DateTime), N'Kim Brackin teaches Crossover turn on your back.', 2, 0, 3, 1)
SET IDENTITY_INSERT [dbo].[Package] OFF
SET IDENTITY_INSERT [dbo].[PackageIntroVideos] ON 

INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (4, 7000, 3, CAST(N'2016-08-21 17:49:49.917' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (5, 2587, 4, CAST(N'2016-08-21 17:50:21.797' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (6, 3003, 4, CAST(N'2016-08-21 17:50:37.610' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (7, 3037, 3, CAST(N'2016-08-21 17:50:56.767' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (8, 3037, 6, CAST(N'2016-09-02 06:23:40.393' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (9, 4380, 7, CAST(N'2016-09-02 06:24:11.123' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (10, 4380, 7, CAST(N'2016-09-02 06:25:20.890' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (11, 6999, 8, CAST(N'2016-09-02 06:26:21.010' AS DateTime), 1)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (12, 7000, 9, CAST(N'2016-09-02 06:35:11.483' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (13, 7000, 6, CAST(N'2016-09-02 06:36:00.260' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (14, 2587, 9, CAST(N'2016-09-02 06:36:26.070' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (15, 3037, 10, CAST(N'2016-09-02 06:36:57.277' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (16, 3037, 11, CAST(N'2016-09-02 06:37:17.313' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (17, 2587, 11, CAST(N'2016-09-02 06:37:33.950' AS DateTime), 1)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (18, 7003, 12, CAST(N'2016-09-02 06:38:20.567' AS DateTime), 1)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (19, 4380, 12, CAST(N'2016-09-02 06:38:41.700' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (20, 7007, 13, CAST(N'2016-09-09 09:01:34.753' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (21, 7010, 13, CAST(N'2016-09-09 09:01:34.790' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (22, 7008, 13, CAST(N'2016-09-09 09:01:34.793' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (23, 7009, 13, CAST(N'2016-09-09 09:01:34.807' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (24, 7006, 13, CAST(N'2016-09-09 16:37:34.213' AS DateTime), 1)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (28, 2587, 8, CAST(N'2016-09-16 12:28:26.470' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (29, 6995, 8, CAST(N'2016-09-16 12:28:26.503' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (30, 6996, 8, CAST(N'2016-09-16 12:28:26.507' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (31, 6997, 8, CAST(N'2016-09-16 12:28:26.513' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (37, 35, 22, CAST(N'2016-09-20 08:45:19.220' AS DateTime), 1)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (38, 7011, 16, CAST(N'2016-09-23 07:01:29.383' AS DateTime), 1)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (39, 9, 17, CAST(N'2016-09-23 07:01:55.140' AS DateTime), 1)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (40, 10, 18, CAST(N'2016-09-23 07:02:21.227' AS DateTime), 1)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (41, 7012, 23, CAST(N'2016-09-24 09:43:44.953' AS DateTime), 1)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (42, 8, 23, CAST(N'2016-09-24 09:43:45.673' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (43, 7013, 24, CAST(N'2016-09-24 09:57:06.357' AS DateTime), 1)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (44, 8, 24, CAST(N'2016-09-24 09:57:07.060' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (46, 7015, 26, CAST(N'2016-09-24 10:13:55.997' AS DateTime), 1)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (47, 8, 26, CAST(N'2016-09-24 10:14:46.210' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (48, 7019, 27, CAST(N'2016-09-26 08:29:42.637' AS DateTime), 1)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (49, 10, 27, CAST(N'2016-09-26 08:29:43.563' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (50, 15, 27, CAST(N'2016-09-26 08:29:43.893' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (51, 21, 27, CAST(N'2016-09-26 08:29:44.587' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (54, 7027, 29, CAST(N'2016-09-26 09:07:57.277' AS DateTime), 1)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (55, 8, 29, CAST(N'2016-09-26 09:07:57.973' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (56, 15, 29, CAST(N'2016-09-26 09:07:58.663' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (57, 17, 29, CAST(N'2016-09-26 09:07:59.390' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (61, 7035, 31, CAST(N'2016-10-10 11:48:16.840' AS DateTime), 1)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (62, 7029, 31, CAST(N'2016-10-10 11:48:18.870' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (63, 7032, 31, CAST(N'2016-10-10 11:48:19.607' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (68, 7041, 33, CAST(N'2016-10-10 12:30:14.807' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (69, 7042, 33, CAST(N'2016-10-10 12:30:15.557' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (70, 7043, 33, CAST(N'2016-10-17 11:48:40.067' AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[PackageIntroVideos] OFF
SET IDENTITY_INSERT [dbo].[PaymentInfo] ON 

INSERT [dbo].[PaymentInfo] ([Id], [Token], [UserId], [PackageId], [IsSubscription], [CreatedDate], [ExpiryDate], [PaymentType], [DeviceType], [SKUID]) VALUES (5, N'ewoJInNpZ25hdHVyZSIgPSAiQXdpUVRwNGYwRCtJbG52WU1QbmgxaTFHK1d0bitlN1RjM2JTbFpQOFI5VWRNdjU5Qll3djlDVURjR1UzYWVvdTFJVk1oOE5PMWx2YXdiWVRWZ3NuK1lPdkdKMU04MEhBUWJCUjI5U0lkNWFKeXZGdmg0ZTlRNHlCNXQwZ2dTMGVDdjk4ZS9KM3Q5VGxGK3lhUjJKY2dqV0ZQbEI5TlZPWnY3MzBnZzIxMnRlWWxsNlF1VWxZYnBsak5JaG1DSnkvT3BlTnpRdGMrejJCOHJpamJ1dU95dVgvWXJ5ZStITi81V2pXTXpIYVl6S3M2VXFEaWZnMWdVejFOdHJ3Ny9GVys0S3ptUlpzR2dWUzNJNjM0OW5naGV0WXZzOVJzeUovOHhKMlNxUm00Sy9GVWxXV0x0bTRZRld0dHZuUWJ0alNMWGN4M3g1SkFrc3MzdTJ1MmlKb0RmTUFBQVdBTUlJRmZEQ0NCR1NnQXdJQkFnSUlEdXRYaCtlZUNZMHdEUVlKS29aSWh2Y05BUUVGQlFBd2daWXhDekFKQmdOVkJBWVRBbFZUTVJNd0VRWURWUVFLREFwQmNIQnNaU0JKYm1NdU1Td3dLZ1lEVlFRTERDTkJjSEJzWlNCWGIzSnNaSGRwWkdVZ1JHVjJaV3h2Y0dWeUlGSmxiR0YwYVc5dWN6RkVNRUlHQTFVRUF3dzdRWEJ3YkdVZ1YyOXliR1IzYVdSbElFUmxkbVZzYjNCbGNpQlNaV3hoZEdsdmJuTWdRMlZ5ZEdsbWFXTmhkR2x2YmlCQmRYUm9iM0pwZEhrd0hoY05NVFV4TVRFek1ESXhOVEE1V2hjTk1qTXdNakEzTWpFME9EUTNXakNCaVRFM01EVUdBMVVFQXd3dVRXRmpJRUZ3Y0NCVGRHOXlaU0JoYm1RZ2FWUjFibVZ6SUZOMGIzSmxJRkpsWTJWcGNIUWdVMmxuYm1sdVp6RXNNQ29HQTFVRUN3d2pRWEJ3YkdVZ1YyOXliR1IzYVdSbElFUmxkbVZzYjNCbGNpQlNaV3hoZEdsdmJuTXhFekFSQmdOVkJBb01Da0Z3Y0d4bElFbHVZeTR4Q3pBSkJnTlZCQVlUQWxWVE1JSUJJakFOQmdrcWhraUc5dzBCQVFFRkFBT0NBUThBTUlJQkNnS0NBUUVBcGMrQi9TV2lnVnZXaCswajJqTWNqdUlqd0tYRUpzczl4cC9zU2cxVmh2K2tBdGVYeWpsVWJYMS9zbFFZbmNRc1VuR09aSHVDem9tNlNkWUk1YlNJY2M4L1cwWXV4c1FkdUFPcFdLSUVQaUY0MWR1MzBJNFNqWU5NV3lwb041UEM4cjBleE5LaERFcFlVcXNTNCszZEg1Z1ZrRFV0d3N3U3lvMUlnZmRZZUZScjZJd3hOaDlLQmd4SFZQTTNrTGl5a29sOVg2U0ZTdUhBbk9DNnBMdUNsMlAwSzVQQi9UNXZ5c0gxUEttUFVockFKUXAyRHQ3K21mNy93bXYxVzE2c2MxRkpDRmFKekVPUXpJNkJBdENnbDdaY3NhRnBhWWVRRUdnbUpqbTRIUkJ6c0FwZHhYUFEzM1k3MkMzWmlCN2o3QWZQNG83UTAvb21WWUh2NGdOSkl3SURBUUFCbzRJQjF6Q0NBZE13UHdZSUt3WUJCUVVIQVFFRU16QXhNQzhHQ0NzR0FRVUZCekFCaGlOb2RIUndPaTh2YjJOemNDNWhjSEJzWlM1amIyMHZiMk56Y0RBekxYZDNaSEl3TkRBZEJnTlZIUTRFRmdRVWthU2MvTVIydDUrZ2l2Uk45WTgyWGUwckJJVXdEQVlEVlIwVEFRSC9CQUl3QURBZkJnTlZIU01FR0RBV2dCU0lKeGNKcWJZWVlJdnM2N3IyUjFuRlVsU2p0ekNDQVI0R0ExVWRJQVNDQVJVd2dnRVJNSUlCRFFZS0tvWklodmRqWkFVR0FUQ0IvakNCd3dZSUt3WUJCUVVIQWdJd2diWU1nYk5TWld4cFlXNWpaU0J2YmlCMGFHbHpJR05sY25ScFptbGpZWFJsSUdKNUlHRnVlU0J3WVhKMGVTQmhjM04xYldWeklHRmpZMlZ3ZEdGdVkyVWdiMllnZEdobElIUm9aVzRnWVhCd2JHbGpZV0pzWlNCemRHRnVaR0Z5WkNCMFpYSnRjeUJoYm1RZ1kyOXVaR2wwYVc5dWN5QnZaaUIxYzJVc0lHTmxjblJwWm1sallYUmxJSEJ2YkdsamVTQmhibVFnWTJWeWRHbG1hV05oZEdsdmJpQndjbUZqZEdsalpTQnpkR0YwWlcxbGJuUnpMakEyQmdnckJnRUZCUWNDQVJZcWFIUjBjRG92TDNkM2R5NWhjSEJzWlM1amIyMHZZMlZ5ZEdsbWFXTmhkR1ZoZFhSb2IzSnBkSGt2TUE0R0ExVWREd0VCL3dRRUF3SUhnREFRQmdvcWhraUc5Mk5rQmdzQkJBSUZBREFOQmdrcWhraUc5dzBCQVFVRkFBT0NBUUVBRGFZYjB5NDk0MXNyQjI1Q2xtelQ2SXhETUlKZjRGelJqYjY5RDcwYS9DV1MyNHlGdzRCWjMrUGkxeTRGRkt3TjI3YTQvdncxTG56THJSZHJqbjhmNUhlNXNXZVZ0Qk5lcGhtR2R2aGFJSlhuWTR3UGMvem83Y1lmcnBuNFpVaGNvT0FvT3NBUU55MjVvQVE1SDNPNXlBWDk4dDUvR2lvcWJpc0IvS0FnWE5ucmZTZW1NL2oxbU9DK1JOdXhUR2Y4YmdwUHllSUdxTktYODZlT2ExR2lXb1IxWmRFV0JHTGp3Vi8xQ0tuUGFObVNBTW5CakxQNGpRQmt1bGhnd0h5dmozWEthYmxiS3RZZGFHNllRdlZNcHpjWm04dzdISG9aUS9PamJiOUlZQVlNTnBJcjdONFl0UkhhTFNQUWp2eWdhWndYRzU2QWV6bEhSVEJoTDhjVHFBPT0iOwoJInB1cmNoYXNlLWluZm8iID0gImV3b0pJbTl5YVdkcGJtRnNMWEIxY21Ob1lYTmxMV1JoZEdVdGNITjBJaUE5SUNJeU1ERTJMVEV3TFRFMklESXpPak16T2pVeElFRnRaWEpwWTJFdlRHOXpYMEZ1WjJWc1pYTWlPd29KSW5WdWFYRjFaUzFwWkdWdWRHbG1hV1Z5SWlBOUlDSXhZMk5tWldGa056Qm1NekJoTWpobE5ETTFOamM1WmpobVlqTTFNRFZpTVRCbFpqZzBNV1UxSWpzS0NTSnZjbWxuYVc1aGJDMTBjbUZ1YzJGamRHbHZiaTFwWkNJZ1BTQWlNVEF3TURBd01ESTBNamcwTkRVek1pSTdDZ2tpWW5aeWN5SWdQU0FpTVM0d0xqQWlPd29KSW5SeVlXNXpZV04wYVc5dUxXbGtJaUE5SUNJeE1EQXdNREF3TWpReU9EUTBOVE15SWpzS0NTSnhkV0Z1ZEdsMGVTSWdQU0FpTVNJN0Nna2liM0pwWjJsdVlXd3RjSFZ5WTJoaGMyVXRaR0YwWlMxdGN5SWdQU0FpTVRRM05qWTROakF6TVRreE9DSTdDZ2tpZFc1cGNYVmxMWFpsYm1SdmNpMXBaR1Z1ZEdsbWFXVnlJaUE5SUNKRU5VVkdSRVpDTXkwd05rTXdMVFF6TUVJdFFqRTNOaTAxUlRrM01qQTBOREEyUVRZaU93b0pJbkJ5YjJSMVkzUXRhV1FpSUQwZ0ltTnZiUzVwYTJ0dmN5NXRkV3gwYVhOd2IzSjBjeTVqYUdGdWJtVnNYekU1TGprNUlqc0tDU0pwZEdWdExXbGtJaUE5SUNJeE1UWTJNRGczT0RRNUlqc0tDU0ppYVdRaUlEMGdJbU52YlM1cGEydHZjeTVwYTJ0dmMybHZjeUk3Q2draWNIVnlZMmhoYzJVdFpHRjBaUzF0Y3lJZ1BTQWlNVFEzTmpZNE5qQXpNVGt4T0NJN0Nna2ljSFZ5WTJoaGMyVXRaR0YwWlNJZ1BTQWlNakF4TmkweE1DMHhOeUF3Tmpvek16bzFNU0JGZEdNdlIwMVVJanNLQ1NKd2RYSmphR0Z6WlMxa1lYUmxMWEJ6ZENJZ1BTQWlNakF4TmkweE1DMHhOaUF5TXpvek16bzFNU0JCYldWeWFXTmhMMHh2YzE5QmJtZGxiR1Z6SWpzS0NTSnZjbWxuYVc1aGJDMXdkWEpqYUdGelpTMWtZWFJsSWlBOUlDSXlNREUyTFRFd0xURTNJREEyT2pNek9qVXhJRVYwWXk5SFRWUWlPd3A5IjsKCSJlbnZpcm9ubWVudCIgPSAiU2FuZGJveCI7CgkicG9kIiA9ICIxMDAiOwoJInNpZ25pbmctc3RhdHVzIiA9ICIwIjsKfQ==', 1, 1, 1, CAST(N'2016-10-17 06:33:52.690' AS DateTime), CAST(N'2016-11-16 06:33:52.690' AS DateTime), N'IAP', 1, N'com.ikkos.multisports.channel_19.99')
INSERT [dbo].[PaymentInfo] ([Id], [Token], [UserId], [PackageId], [IsSubscription], [CreatedDate], [ExpiryDate], [PaymentType], [DeviceType], [SKUID]) VALUES (6, N'noopfkkphpibjabdlelhpjhg.AO-J1Ox8T39F5yb4cDIlWnA56jv4sQgPZGrcTrM5JDBO_G_LnH4z9uNs9s-BYLwqhmJMqDFTVyfThC1K3LBU0v8RE8r2sja8GswxvqdMUQFGOMoBXZl3Z5Jsji7goGRQo_am9c-8vPcJkMjXNKM3HrAYghKG4nSD5A', 6, 13, 1, CAST(N'2016-10-17 10:02:46.220' AS DateTime), CAST(N'2016-11-16 10:02:46.220' AS DateTime), N'IAP', 2, NULL)
INSERT [dbo].[PaymentInfo] ([Id], [Token], [UserId], [PackageId], [IsSubscription], [CreatedDate], [ExpiryDate], [PaymentType], [DeviceType], [SKUID]) VALUES (7, N'gbbjhpakkacljbhkhjakincc.AO-J1OyWhRFJ6hjNZF0xPFJjgYJ_F3BXupJaU2fjie-mvEx-b61gZKklbzC7bIXGD5b-iSxeLfYqrSl6e1nScTx53FT1jLDSuuAB4VghvUeRp1K26eqmrOekH9DBae3v_dpjEl2GOz0pOG29nGmvly0Ss-rT32lm8Q', 2, 13, 2, CAST(N'2016-10-17 10:30:43.363' AS DateTime), CAST(N'2016-11-16 10:30:43.363' AS DateTime), N'IAP', 2, N'com.ikkos.multisports.package_1.99')
INSERT [dbo].[PaymentInfo] ([Id], [Token], [UserId], [PackageId], [IsSubscription], [CreatedDate], [ExpiryDate], [PaymentType], [DeviceType], [SKUID]) VALUES (8, N'ewoJInNpZ25hdHVyZSIgPSAiQTB3RUVkampWZ2dqRm9iS3hSaEREUzZra1Z5MDJGaWY2cm9XcVpodWxUMUZtNVdROVIyUks2QUF2Rit0YU1IL0FNbjZ5NU9TUHkwQ1QzSm4xcnRZL2hoYUV5YUo3YUlIV1l6WkpqbEx2SlhiVktEUmdzS2g5T1h6ekpwVTk5L1ZOb0lDb2MwOWlycGwxY0VzLzNQZlJ4WW5rbHcyVEhxUlRKWWlnNGN6K0hmS01zNkJwWTBjRG5MVUdqdW9JeVM4Q3lSenFxWEFPUlNmdTgvU0NrWDFpdjltOWRXeEpONzM0VnNmazNXeGEvb1FTNXU0TnRFRTNhaUQvVnBVNFRxYTZ6NlhVcVBBY2hQNzJneldPV0N3UHdBQUFybktBaTlzdzI1SFdVTm1Yekd0THgvczdiMVRsdHhsVUxDU1N6OS94UXVvRXR5WXkySDRsTlhpTzVlcERCUUFBQVdBTUlJRmZEQ0NCR1NnQXdJQkFnSUlEdXRYaCtlZUNZMHdEUVlKS29aSWh2Y05BUUVGQlFBd2daWXhDekFKQmdOVkJBWVRBbFZUTVJNd0VRWURWUVFLREFwQmNIQnNaU0JKYm1NdU1Td3dLZ1lEVlFRTERDTkJjSEJzWlNCWGIzSnNaSGRwWkdVZ1JHVjJaV3h2Y0dWeUlGSmxiR0YwYVc5dWN6RkVNRUlHQTFVRUF3dzdRWEJ3YkdVZ1YyOXliR1IzYVdSbElFUmxkbVZzYjNCbGNpQlNaV3hoZEdsdmJuTWdRMlZ5ZEdsbWFXTmhkR2x2YmlCQmRYUm9iM0pwZEhrd0hoY05NVFV4TVRFek1ESXhOVEE1V2hjTk1qTXdNakEzTWpFME9EUTNXakNCaVRFM01EVUdBMVVFQXd3dVRXRmpJRUZ3Y0NCVGRHOXlaU0JoYm1RZ2FWUjFibVZ6SUZOMGIzSmxJRkpsWTJWcGNIUWdVMmxuYm1sdVp6RXNNQ29HQTFVRUN3d2pRWEJ3YkdVZ1YyOXliR1IzYVdSbElFUmxkbVZzYjNCbGNpQlNaV3hoZEdsdmJuTXhFekFSQmdOVkJBb01Da0Z3Y0d4bElFbHVZeTR4Q3pBSkJnTlZCQVlUQWxWVE1JSUJJakFOQmdrcWhraUc5dzBCQVFFRkFBT0NBUThBTUlJQkNnS0NBUUVBcGMrQi9TV2lnVnZXaCswajJqTWNqdUlqd0tYRUpzczl4cC9zU2cxVmh2K2tBdGVYeWpsVWJYMS9zbFFZbmNRc1VuR09aSHVDem9tNlNkWUk1YlNJY2M4L1cwWXV4c1FkdUFPcFdLSUVQaUY0MWR1MzBJNFNqWU5NV3lwb041UEM4cjBleE5LaERFcFlVcXNTNCszZEg1Z1ZrRFV0d3N3U3lvMUlnZmRZZUZScjZJd3hOaDlLQmd4SFZQTTNrTGl5a29sOVg2U0ZTdUhBbk9DNnBMdUNsMlAwSzVQQi9UNXZ5c0gxUEttUFVockFKUXAyRHQ3K21mNy93bXYxVzE2c2MxRkpDRmFKekVPUXpJNkJBdENnbDdaY3NhRnBhWWVRRUdnbUpqbTRIUkJ6c0FwZHhYUFEzM1k3MkMzWmlCN2o3QWZQNG83UTAvb21WWUh2NGdOSkl3SURBUUFCbzRJQjF6Q0NBZE13UHdZSUt3WUJCUVVIQVFFRU16QXhNQzhHQ0NzR0FRVUZCekFCaGlOb2RIUndPaTh2YjJOemNDNWhjSEJzWlM1amIyMHZiMk56Y0RBekxYZDNaSEl3TkRBZEJnTlZIUTRFRmdRVWthU2MvTVIydDUrZ2l2Uk45WTgyWGUwckJJVXdEQVlEVlIwVEFRSC9CQUl3QURBZkJnTlZIU01FR0RBV2dCU0lKeGNKcWJZWVlJdnM2N3IyUjFuRlVsU2p0ekNDQVI0R0ExVWRJQVNDQVJVd2dnRVJNSUlCRFFZS0tvWklodmRqWkFVR0FUQ0IvakNCd3dZSUt3WUJCUVVIQWdJd2diWU1nYk5TWld4cFlXNWpaU0J2YmlCMGFHbHpJR05sY25ScFptbGpZWFJsSUdKNUlHRnVlU0J3WVhKMGVTQmhjM04xYldWeklHRmpZMlZ3ZEdGdVkyVWdiMllnZEdobElIUm9aVzRnWVhCd2JHbGpZV0pzWlNCemRHRnVaR0Z5WkNCMFpYSnRjeUJoYm1RZ1kyOXVaR2wwYVc5dWN5QnZaaUIxYzJVc0lHTmxjblJwWm1sallYUmxJSEJ2YkdsamVTQmhibVFnWTJWeWRHbG1hV05oZEdsdmJpQndjbUZqZEdsalpTQnpkR0YwWlcxbGJuUnpMakEyQmdnckJnRUZCUWNDQVJZcWFIUjBjRG92TDNkM2R5NWhjSEJzWlM1amIyMHZZMlZ5ZEdsbWFXTmhkR1ZoZFhSb2IzSnBkSGt2TUE0R0ExVWREd0VCL3dRRUF3SUhnREFRQmdvcWhraUc5Mk5rQmdzQkJBSUZBREFOQmdrcWhraUc5dzBCQVFVRkFBT0NBUUVBRGFZYjB5NDk0MXNyQjI1Q2xtelQ2SXhETUlKZjRGelJqYjY5RDcwYS9DV1MyNHlGdzRCWjMrUGkxeTRGRkt3TjI3YTQvdncxTG56THJSZHJqbjhmNUhlNXNXZVZ0Qk5lcGhtR2R2aGFJSlhuWTR3UGMvem83Y1lmcnBuNFpVaGNvT0FvT3NBUU55MjVvQVE1SDNPNXlBWDk4dDUvR2lvcWJpc0IvS0FnWE5ucmZTZW1NL2oxbU9DK1JOdXhUR2Y4YmdwUHllSUdxTktYODZlT2ExR2lXb1IxWmRFV0JHTGp3Vi8xQ0tuUGFObVNBTW5CakxQNGpRQmt1bGhnd0h5dmozWEthYmxiS3RZZGFHNllRdlZNcHpjWm04dzdISG9aUS9PamJiOUlZQVlNTnBJcjdONFl0UkhhTFNQUWp2eWdhWndYRzU2QWV6bEhSVEJoTDhjVHFBPT0iOwoJInB1cmNoYXNlLWluZm8iID0gImV3b0pJbTl5YVdkcGJtRnNMWEIxY21Ob1lYTmxMV1JoZEdVdGNITjBJaUE5SUNJeU1ERTJMVEV3TFRFM0lEQTFPakE0T2pVNElFRnRaWEpwWTJFdlRHOXpYMEZ1WjJWc1pYTWlPd29KSW5WdWFYRjFaUzFwWkdWdWRHbG1hV1Z5SWlBOUlDSXhZMk5tWldGa056Qm1NekJoTWpobE5ETTFOamM1WmpobVlqTTFNRFZpTVRCbFpqZzBNV1UxSWpzS0NTSnZjbWxuYVc1aGJDMTBjbUZ1YzJGamRHbHZiaTFwWkNJZ1BTQWlNVEF3TURBd01ESTBNamszT1RZME5DSTdDZ2tpWW5aeWN5SWdQU0FpTVM0d0xqRWlPd29KSW5SeVlXNXpZV04wYVc5dUxXbGtJaUE5SUNJeE1EQXdNREF3TWpReU9UYzVOalEwSWpzS0NTSnhkV0Z1ZEdsMGVTSWdQU0FpTVNJN0Nna2liM0pwWjJsdVlXd3RjSFZ5WTJoaGMyVXRaR0YwWlMxdGN5SWdQU0FpTVRRM05qY3dOakV6T0RNNE15STdDZ2tpZFc1cGNYVmxMWFpsYm1SdmNpMXBaR1Z1ZEdsbWFXVnlJaUE5SUNKRU5VVkdSRVpDTXkwd05rTXdMVFF6TUVJdFFqRTNOaTAxUlRrM01qQTBOREEyUVRZaU93b0pJbkJ5YjJSMVkzUXRhV1FpSUQwZ0ltTnZiUzVwYTJ0dmN5NXRkV3gwYVhOd2IzSjBjeTV3WVdOcllXZGxYekV1T1RraU93b0pJbWwwWlcwdGFXUWlJRDBnSWpFeE5qWXdPRE0wTmpraU93b0pJbUpwWkNJZ1BTQWlZMjl0TG1scmEyOXpMbWxyYTI5emFXOXpJanNLQ1NKd2RYSmphR0Z6WlMxa1lYUmxMVzF6SWlBOUlDSXhORGMyTnpBMk1UTTRNemd6SWpzS0NTSndkWEpqYUdGelpTMWtZWFJsSWlBOUlDSXlNREUyTFRFd0xURTNJREV5T2pBNE9qVTRJRVYwWXk5SFRWUWlPd29KSW5CMWNtTm9ZWE5sTFdSaGRHVXRjSE4wSWlBOUlDSXlNREUyTFRFd0xURTNJREExT2pBNE9qVTRJRUZ0WlhKcFkyRXZURzl6WDBGdVoyVnNaWE1pT3dvSkltOXlhV2RwYm1Gc0xYQjFjbU5vWVhObExXUmhkR1VpSUQwZ0lqSXdNVFl0TVRBdE1UY2dNVEk2TURnNk5UZ2dSWFJqTDBkTlZDSTdDbjA9IjsKCSJlbnZpcm9ubWVudCIgPSAiU2FuZGJveCI7CgkicG9kIiA9ICIxMDAiOwoJInNpZ25pbmctc3RhdHVzIiA9ICIwIjsKfQ==', 13, 33, 2, CAST(N'2016-10-17 12:08:59.187' AS DateTime), CAST(N'2016-11-16 12:08:59.187' AS DateTime), N'IAP', 1, N'com.ikkos.multisports.package_1.99')
INSERT [dbo].[PaymentInfo] ([Id], [Token], [UserId], [PackageId], [IsSubscription], [CreatedDate], [ExpiryDate], [PaymentType], [DeviceType], [SKUID]) VALUES (9, N'fflffmknlafijbcmeppbkbdf.AO-J1OyYdOwbtArOtXKRdtQQujwyFO34qpFbvpTH9DYsYCXHXF92gH7E_rUeFcLiaqEKZ4Y5dAmD8mczn1oGLHMFeML2IM2IQD1o_bkhMSX687I8SLPyOlSvqv0MjDp6QjJdwo4ALU9WNWS63tLr9dMts_Z1ljHe07R6W2bEsfUU0cZuOMou9L4', 2, 8, 2, CAST(N'2016-10-17 19:50:02.890' AS DateTime), CAST(N'2016-11-16 19:50:02.890' AS DateTime), N'IAP', 2, N'com.ikkos.multisports.package_FREE')
INSERT [dbo].[PaymentInfo] ([Id], [Token], [UserId], [PackageId], [IsSubscription], [CreatedDate], [ExpiryDate], [PaymentType], [DeviceType], [SKUID]) VALUES (10, N'egkcanfpphliallligpfdghe.AO-J1OyhhORHsUIb6gBs45QhHRc0iKyOmZ1hPnxtJTNj37hcX7k0A0MR_c7YY9YG1YH1zJiU9wLSfvUeiYU-pW4xD9OxhCOTWPcCMOZH768PIGAt4oqtnq0I5YoRlbNj12SZVuSu29117HNoB8Tw99HGMZhoarQ4TapBXSjNN8hMqOIEd9uCs1w', 2, 1, 2, CAST(N'2016-10-17 19:50:22.827' AS DateTime), CAST(N'2016-11-16 19:50:22.827' AS DateTime), N'IAP', 2, NULL)
INSERT [dbo].[PaymentInfo] ([Id], [Token], [UserId], [PackageId], [IsSubscription], [CreatedDate], [ExpiryDate], [PaymentType], [DeviceType], [SKUID]) VALUES (11, N'bacghkihlnmcajoapeliihog.AO-J1OzgtdZKtyQOl8xVgX-Fg_cMkb0BIEZ5qeMLK7BIlgN6fThp3FG690NqFJRpdsaA9N1WDq38SUdmr9mtIACy_Sx843Lxd9ul4i3ANkY0IIE_Bnq5KyOqktF6cbB2ZsgSfuJnO1ZsCD_fXbU6yt6s2eRMQHchLNPSx1Mn8Ifsaso8QInmPjw', 2, 7, 2, CAST(N'2016-10-17 19:50:46.593' AS DateTime), CAST(N'2016-11-16 19:50:46.593' AS DateTime), N'IAP', 2, N'com.ikkos.multisports.package_0.99')
SET IDENTITY_INSERT [dbo].[PaymentInfo] OFF
SET IDENTITY_INSERT [dbo].[PricingCatalog] ON 

INSERT [dbo].[PricingCatalog] ([Id], [Price], [SKUID], [CreatedDate], [pricingtype]) VALUES (1, N'0.99', N'com.ikkos.multisports.package_0.99', CAST(N'2016-09-01 11:09:10.327' AS DateTime), 2)
INSERT [dbo].[PricingCatalog] ([Id], [Price], [SKUID], [CreatedDate], [pricingtype]) VALUES (2, N'1.99', N'com.ikkos.multisports.package_1.99', CAST(N'2016-09-01 11:09:10.330' AS DateTime), 2)
INSERT [dbo].[PricingCatalog] ([Id], [Price], [SKUID], [CreatedDate], [pricingtype]) VALUES (3, N'2.99', N'com.ikkos.multisports.package_2.99', CAST(N'2016-09-01 11:09:10.350' AS DateTime), 2)
INSERT [dbo].[PricingCatalog] ([Id], [Price], [SKUID], [CreatedDate], [pricingtype]) VALUES (4, N'3.99', N'com.ikkos.multisports.package_3.99', CAST(N'2016-09-01 11:09:10.353' AS DateTime), 2)
INSERT [dbo].[PricingCatalog] ([Id], [Price], [SKUID], [CreatedDate], [pricingtype]) VALUES (5, N'4.99', N'com.ikkos.multisports.package_4.99', CAST(N'2016-09-01 11:09:10.360' AS DateTime), 2)
INSERT [dbo].[PricingCatalog] ([Id], [Price], [SKUID], [CreatedDate], [pricingtype]) VALUES (6, N'5.99', N'com.ikkos.multisports.package_5.99', CAST(N'2016-09-01 11:09:10.360' AS DateTime), 2)
INSERT [dbo].[PricingCatalog] ([Id], [Price], [SKUID], [CreatedDate], [pricingtype]) VALUES (7, N'6.99', N'com.ikkos.multisports.package_6.99', CAST(N'2016-09-01 11:09:10.367' AS DateTime), 2)
INSERT [dbo].[PricingCatalog] ([Id], [Price], [SKUID], [CreatedDate], [pricingtype]) VALUES (8, N'7.99', N'com.ikkos.multisports.package_7.99', CAST(N'2016-09-01 11:09:10.370' AS DateTime), 2)
INSERT [dbo].[PricingCatalog] ([Id], [Price], [SKUID], [CreatedDate], [pricingtype]) VALUES (9, N'8.99', N'com.ikkos.multisports.package_8.99', CAST(N'2016-09-01 11:09:10.373' AS DateTime), 2)
INSERT [dbo].[PricingCatalog] ([Id], [Price], [SKUID], [CreatedDate], [pricingtype]) VALUES (10, N'9.99', N'com.ikkos.multisports.package_9.99', CAST(N'2016-09-01 11:09:10.377' AS DateTime), 2)
INSERT [dbo].[PricingCatalog] ([Id], [Price], [SKUID], [CreatedDate], [pricingtype]) VALUES (11, N'Free', N'com.ikkos.multisports.package_FREE', CAST(N'2016-09-07 08:46:02.040' AS DateTime), 2)
INSERT [dbo].[PricingCatalog] ([Id], [Price], [SKUID], [CreatedDate], [pricingtype]) VALUES (12, N'19.99', N'com.ikkos.multisports.channel_19.99', CAST(N'2016-09-20 09:15:35.643' AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[PricingCatalog] OFF
INSERT [dbo].[Sports] ([SportId], [SportName], [SportsImageUrl], [BannerImageUrl], [IsGreyOut], [SportsImageUrl1]) VALUES (1, N'Strength & Fitness', N'https://ikkosvideos.blob.core.windows.net/imagesstore/7268524541551.png', N'', 1, N'https://ikkosvideos.blob.core.windows.net/imagesstore/40505734262246.png')
INSERT [dbo].[Sports] ([SportId], [SportName], [SportsImageUrl], [BannerImageUrl], [IsGreyOut], [SportsImageUrl1]) VALUES (2, N'Baseball', N'https://ikkosvideos.blob.core.windows.net/imagesstore/17678794817386.png', N'NULL', 0, N'https://ikkosvideos.blob.core.windows.net/imagesstore/3488241893214.png')
INSERT [dbo].[Sports] ([SportId], [SportName], [SportsImageUrl], [BannerImageUrl], [IsGreyOut], [SportsImageUrl1]) VALUES (3, N'BasketBall', N'https://ikkosvideos.blob.core.windows.net/imagesstore/72224451366253.png', N'NULL', 0, N'https://ikkosvideos.blob.core.windows.net/imagesstore/19061561076840.png')
INSERT [dbo].[Sports] ([SportId], [SportName], [SportsImageUrl], [BannerImageUrl], [IsGreyOut], [SportsImageUrl1]) VALUES (4, N'Boxing', N'https://ikkosvideos.blob.core.windows.net/imagesstore/26574858019409.png', N'NULL', 0, N'https://ikkosvideos.blob.core.windows.net/imagesstore/79082341054669.png')
INSERT [dbo].[Sports] ([SportId], [SportName], [SportsImageUrl], [BannerImageUrl], [IsGreyOut], [SportsImageUrl1]) VALUES (5, N'Cycling', N'https://ikkosvideos.blob.core.windows.net/imagesstore/28481967317275.png', N'NULL', 0, N'https://ikkosvideos.blob.core.windows.net/imagesstore/3959269882439.png')
INSERT [dbo].[Sports] ([SportId], [SportName], [SportsImageUrl], [BannerImageUrl], [IsGreyOut], [SportsImageUrl1]) VALUES (6, N'Football', N'https://ikkosvideos.blob.core.windows.net/imagesstore/94135717766259.png', N'https://copymesportsmediaservice.blob.core.windows.net/sportsimages/American-FootbalBanner.png', 0, N'https://ikkosvideos.blob.core.windows.net/imagesstore/8933456508480.png')
INSERT [dbo].[Sports] ([SportId], [SportName], [SportsImageUrl], [BannerImageUrl], [IsGreyOut], [SportsImageUrl1]) VALUES (7, N'Golf', N'https://ikkosvideos.blob.core.windows.net/imagesstore/71882081693426.png', N'NULL', 0, N'https://ikkosvideos.blob.core.windows.net/imagesstore/52653395710150.png')
INSERT [dbo].[Sports] ([SportId], [SportName], [SportsImageUrl], [BannerImageUrl], [IsGreyOut], [SportsImageUrl1]) VALUES (8, N'Gymnastics', N'https://ikkosvideos.blob.core.windows.net/imagesstore/21990489972779.png', N'https://copymesportsmediaservice.blob.core.windows.net/sportsimages/GymnasticsfinalBanner.png', 0, N'https://ikkosvideos.blob.core.windows.net/imagesstore/70001678231555.png')
INSERT [dbo].[Sports] ([SportId], [SportName], [SportsImageUrl], [BannerImageUrl], [IsGreyOut], [SportsImageUrl1]) VALUES (9, N'KungFu', N'https://ikkosvideos.blob.core.windows.net/imagesstore/74596815552657.png', N'https://copymesportsmediaservice.blob.core.windows.net/sportsimages/kungfufinalBanner.png', 0, N'https://ikkosvideos.blob.core.windows.net/imagesstore/63159641276184.png')
INSERT [dbo].[Sports] ([SportId], [SportName], [SportsImageUrl], [BannerImageUrl], [IsGreyOut], [SportsImageUrl1]) VALUES (10, N'Lacrosse', N'https://ikkosvideos.blob.core.windows.net/imagesstore/713040844337.png', N'NULL', 0, N'https://ikkosvideos.blob.core.windows.net/imagesstore/35144512619518.png')
INSERT [dbo].[Sports] ([SportId], [SportName], [SportsImageUrl], [BannerImageUrl], [IsGreyOut], [SportsImageUrl1]) VALUES (11, N'Running', N'https://ikkosvideos.blob.core.windows.net/imagesstore/59639795339571.png', N'NULL', 0, N'https://ikkosvideos.blob.core.windows.net/imagesstore/7461325028224.png')
INSERT [dbo].[Sports] ([SportId], [SportName], [SportsImageUrl], [BannerImageUrl], [IsGreyOut], [SportsImageUrl1]) VALUES (12, N'Soccer', N'https://ikkosvideos.blob.core.windows.net/imagesstore/7089516412903.png', N'NULL', 0, N'https://ikkosvideos.blob.core.windows.net/imagesstore/87946381985566.png')
INSERT [dbo].[Sports] ([SportId], [SportName], [SportsImageUrl], [BannerImageUrl], [IsGreyOut], [SportsImageUrl1]) VALUES (13, N'Swimming', N'https://ikkosvideos.blob.core.windows.net/imagesstore/7512096268671.png', N'https://copymesportsmediaservice.blob.core.windows.net/sportsimages/swimmingfinalBanner.png', 1, N'https://ikkosvideos.blob.core.windows.net/imagesstore/96655350482368.png')
INSERT [dbo].[Sports] ([SportId], [SportName], [SportsImageUrl], [BannerImageUrl], [IsGreyOut], [SportsImageUrl1]) VALUES (14, N'Tennis', N'https://ikkosvideos.blob.core.windows.net/imagesstore/96355810664532.png', N'NULL', 0, N'https://ikkosvideos.blob.core.windows.net/imagesstore/31597999456451.png')
INSERT [dbo].[Sports] ([SportId], [SportName], [SportsImageUrl], [BannerImageUrl], [IsGreyOut], [SportsImageUrl1]) VALUES (15, N'VolleyBall', N'https://ikkosvideos.blob.core.windows.net/imagesstore/14549278686244.png', N'NULL', 0, N'https://ikkosvideos.blob.core.windows.net/imagesstore/16491043748758.png')
INSERT [dbo].[Users] ([UserId], [Name], [email], [ProfilePicUrl], [FacebookId], [Gender], [LoginType], [DeviceId], [DeviceType], [createddate], [CreatedBy], [SportId], [Password]) VALUES (1, N'Rakesh Kumar', N'kumarmapakshi@gmail.com', N'https://graph.facebook.com/1144133312346273/picture?type=large', N'1144133312346273', 0, 1, N'FEBDF9AA-624D-4E08-B4E1-11F125FB549C', 1, CAST(N'2016-10-12 13:17:27.713' AS DateTime), 1, 13, N'')
INSERT [dbo].[Users] ([UserId], [Name], [email], [ProfilePicUrl], [FacebookId], [Gender], [LoginType], [DeviceId], [DeviceType], [createddate], [CreatedBy], [SportId], [Password]) VALUES (2, N'Crystal Hyd', N'elitecrest13@gmail.com', N'https://graph.facebook.com/302853546758036/picture?type=large', N'302853546758036', 0, 1, N'Android', 2, CAST(N'2016-10-12 13:18:47.943' AS DateTime), 2, 13, N'null')
INSERT [dbo].[Users] ([UserId], [Name], [email], [ProfilePicUrl], [FacebookId], [Gender], [LoginType], [DeviceId], [DeviceType], [createddate], [CreatedBy], [SportId], [Password]) VALUES (3, N'shaik', N'osmanandroid143@gmail.com', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/12614978067497.png', N'null', 0, 2, N'Android', 2, CAST(N'2016-10-12 13:20:29.327' AS DateTime), 3, 13, N'123')
INSERT [dbo].[Users] ([UserId], [Name], [email], [ProfilePicUrl], [FacebookId], [Gender], [LoginType], [DeviceId], [DeviceType], [createddate], [CreatedBy], [SportId], [Password]) VALUES (4, N'Sridevi', N'lamsridevi72@gmail.com', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/35574472333447.png', N'', 0, 2, N'FEBDF9AA-624D-4E08-B4E1-11F125FB549C', 1, CAST(N'2016-10-12 14:04:26.600' AS DateTime), 4, 13, N'123456')
INSERT [dbo].[Users] ([UserId], [Name], [email], [ProfilePicUrl], [FacebookId], [Gender], [LoginType], [DeviceId], [DeviceType], [createddate], [CreatedBy], [SportId], [Password]) VALUES (5, N'Shaik Abdul Osman', N'shaikabdulosman@gmail.com', N'https://graph.facebook.com/1058607100927103/picture?type=large', N'1058607100927103', 0, 1, N'Android', 2, CAST(N'2016-10-13 07:20:28.300' AS DateTime), 5, 1, N'null')
INSERT [dbo].[Users] ([UserId], [Name], [email], [ProfilePicUrl], [FacebookId], [Gender], [LoginType], [DeviceId], [DeviceType], [createddate], [CreatedBy], [SportId], [Password]) VALUES (6, N'Nidhi Sri', N'nidhilam6@gmail.com', N'https://graph.facebook.com/241563162910024/picture?type=large', N'241563162910024', 1, 1, N'Android', 2, CAST(N'2016-10-13 08:51:54.130' AS DateTime), 6, 1, N'null')
INSERT [dbo].[Users] ([UserId], [Name], [email], [ProfilePicUrl], [FacebookId], [Gender], [LoginType], [DeviceId], [DeviceType], [createddate], [CreatedBy], [SportId], [Password]) VALUES (7, N'Mahi', N'111@gmail.com', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/50887678699767.png', N'', 0, 2, N'53566E6F-CF64-4D69-B71C-11ABE1727E64', 1, CAST(N'2016-10-13 09:21:36.180' AS DateTime), 7, 13, N'1111')
INSERT [dbo].[Users] ([UserId], [Name], [email], [ProfilePicUrl], [FacebookId], [Gender], [LoginType], [DeviceId], [DeviceType], [createddate], [CreatedBy], [SportId], [Password]) VALUES (8, N'Shivaji Hyd', N'elitecrest10@gmail.com', N'https://graph.facebook.com/313655369010905/picture?type=large', N'313655369010905', 0, 1, N'A352246B-F810-469F-A23B-2B3EF486FA71', 1, CAST(N'2016-10-13 09:29:30.300' AS DateTime), 8, 13, N'')
INSERT [dbo].[Users] ([UserId], [Name], [email], [ProfilePicUrl], [FacebookId], [Gender], [LoginType], [DeviceId], [DeviceType], [createddate], [CreatedBy], [SportId], [Password]) VALUES (9, N'Ios', N'Ios@gmail.com', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/52323462656346.png', N'', 0, 2, N'F4E591A8-582D-44F8-AA6E-82FC6B40B97D', 1, CAST(N'2016-10-15 06:11:59.630' AS DateTime), 9, 13, N'1234')
INSERT [dbo].[Users] ([UserId], [Name], [email], [ProfilePicUrl], [FacebookId], [Gender], [LoginType], [DeviceId], [DeviceType], [createddate], [CreatedBy], [SportId], [Password]) VALUES (10, N'Theju Thej', N'theju254@gmail.com', N'https://graph.facebook.com/1088475977915165/picture?type=large', N'1088475977915165', 1, 1, N'Android', 2, CAST(N'2016-10-17 09:03:08.970' AS DateTime), 10, 13, N'null')
INSERT [dbo].[Users] ([UserId], [Name], [email], [ProfilePicUrl], [FacebookId], [Gender], [LoginType], [DeviceId], [DeviceType], [createddate], [CreatedBy], [SportId], [Password]) VALUES (11, N'Abhi Ak', N'elitecrest1.2@gmail.com', N'https://graph.facebook.com/296382290723162/picture?type=large', N'296382290723162', 0, 1, N'Android', 2, CAST(N'2016-10-17 09:06:34.897' AS DateTime), 11, 13, N'null')
INSERT [dbo].[Users] ([UserId], [Name], [email], [ProfilePicUrl], [FacebookId], [Gender], [LoginType], [DeviceId], [DeviceType], [createddate], [CreatedBy], [SportId], [Password]) VALUES (12, N'Irfan SMd', N'irfan.atjava@gmail.com', N'https://graph.facebook.com/687975041355804/picture?type=large', N'687975041355804', 0, 1, N'Android', 2, CAST(N'2016-10-17 10:24:10.113' AS DateTime), 12, 8, N'null')
INSERT [dbo].[Users] ([UserId], [Name], [email], [ProfilePicUrl], [FacebookId], [Gender], [LoginType], [DeviceId], [DeviceType], [createddate], [CreatedBy], [SportId], [Password]) VALUES (13, N'Kiran Crest', N'elitecrest18@gmail.com', N'https://graph.facebook.com/229424697459966/picture?type=large', N'229424697459966', 1, 1, N'Android', 2, CAST(N'2016-10-17 10:32:48.677' AS DateTime), 13, 1, N'null')
INSERT [dbo].[Users] ([UserId], [Name], [email], [ProfilePicUrl], [FacebookId], [Gender], [LoginType], [DeviceId], [DeviceType], [createddate], [CreatedBy], [SportId], [Password]) VALUES (14, N'tej', N'teju.tej@gmail.com', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/12594374390437.png', N'null', 0, 2, N'Android', 2, CAST(N'2016-10-17 12:57:04.980' AS DateTime), 14, 13, N'123456')
INSERT [dbo].[Users] ([UserId], [Name], [email], [ProfilePicUrl], [FacebookId], [Gender], [LoginType], [DeviceId], [DeviceType], [createddate], [CreatedBy], [SportId], [Password]) VALUES (15, N'Adi Sri', N'elitecrest1.1@gmail.com', N'https://graph.facebook.com/289214504770890/picture?type=large', N'289214504770890', 0, 1, N'Android', 2, CAST(N'2016-10-17 13:26:06.610' AS DateTime), 15, 13, N'null')
INSERT [dbo].[Users] ([UserId], [Name], [email], [ProfilePicUrl], [FacebookId], [Gender], [LoginType], [DeviceId], [DeviceType], [createddate], [CreatedBy], [SportId], [Password]) VALUES (16, N'Gustavo Calado', N'gmc@ikkostraining.com', N'https://graph.facebook.com/10104740404353533/picture?type=large', N'10104740404353533', 0, 1, N'Android', 2, CAST(N'2016-10-17 14:34:31.253' AS DateTime), 16, 13, N'null')
INSERT [dbo].[Users] ([UserId], [Name], [email], [ProfilePicUrl], [FacebookId], [Gender], [LoginType], [DeviceId], [DeviceType], [createddate], [CreatedBy], [SportId], [Password]) VALUES (17, N'Andy Griffiths', N'andygy2k2@hotmail.com', N'https://graph.facebook.com/10154122371871379/picture?type=large', N'10154122371871379', 0, 1, N'Android', 2, CAST(N'2016-10-18 22:10:55.777' AS DateTime), 17, 13, N'null')
INSERT [dbo].[Users] ([UserId], [Name], [email], [ProfilePicUrl], [FacebookId], [Gender], [LoginType], [DeviceId], [DeviceType], [createddate], [CreatedBy], [SportId], [Password]) VALUES (18, N'Neetha Tuluri', N'neethasumana@yahoo.com', N'https://graph.facebook.com/10154630658667760/picture?type=large', N'10154630658667760', 0, 1, N'6749DD75-8D37-4F8E-9C7E-E30CF14B373F', 1, CAST(N'2016-10-19 04:24:06.327' AS DateTime), 18, 13, N'')
INSERT [dbo].[Users] ([UserId], [Name], [email], [ProfilePicUrl], [FacebookId], [Gender], [LoginType], [DeviceId], [DeviceType], [createddate], [CreatedBy], [SportId], [Password]) VALUES (19, N'Test', N'Flyflyerson@gmail.com', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/7429906451990.png', N'', 0, 2, N'E87DC0F5-C588-4573-B784-A70A9E912816', 1, CAST(N'2016-10-19 18:08:46.350' AS DateTime), 19, 1, N'Apple123')
SET IDENTITY_INSERT [dbo].[Videos] ON 

INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (4, N'Bubbles 1 skill video 2', N'Bubbles 1 skill video 2', N'mp4', NULL, N'https://ikkosvideos.blob.core.windows.net/videosstore/79419940481852.mp4', N'https://ikkosvideos.blob.core.windows.net/imagesstore/76636840421034.png', 1, 11, CAST(N'2016-09-14 09:37:07.593' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (5, N'broad jump-HD.mp4', N'broad jump-HD.mp4', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-f6835527-a117-4d6b-be24-da7684848738/fn1437906650.mp4?sv=2012-02-12&sr=c&si=d67c66d4-1b8f-4850-9bcc-1320b82c79ed&sig=%2B1aybwUmHeca%2FZ3JsULqQ5A6eRkQ4Scy6Z2bWnY9Jd0%3D&se=2017-09-14T10%3A26%3A47Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/429394419672.jpg', 1, 19, CAST(N'2016-09-14 10:27:07.947' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (6, N'20 yard shuttle-HD.mp4', N'20 yard shuttle-HD.mp4', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-aaed7100-97b4-4794-a5f9-08a04b0b5984/fn68227155.mp4?sv=2012-02-12&sr=c&si=480de78f-1170-411e-9f2b-fccf42990f67&sig=MoMdqpeINA3CUlnra4b%2BOJHsI6y2evIyVtY42SoQ2lY%3D&se=2017-09-14T10%3A31%3A24Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/81899740468.jpg', 1, 19, CAST(N'2016-09-14 10:31:44.067' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (7, N'20 yard shuttle-HD.mp4', N'20 yard shuttle-HD.mp4', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-1d9cfd4c-3005-4e95-8a0b-1f2ee78f7de1/fn541738390.mp4?sv=2012-02-12&sr=c&si=f632cd0f-b8bc-44dd-886b-eb812e9d5737&sig=H79kTOyA7jhn94BAyNR3kF46oFmQbKYPLzxmbTUaVP8%3D&se=2017-09-14T12%3A06%3A29Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/57437520120542.jpg', 1, 19, CAST(N'2016-09-14 12:07:18.240' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (8, N'broad jump-HD.mp4', N'broad jump-HD.mp4', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-3bad5ee4-b5bf-45f4-9404-e8561018e5a6/fn1783906008.mp4?sv=2012-02-12&sr=c&si=528ca4f0-b80c-4012-a6c5-465fb1e2ddfd&sig=CW4r2AF8nQZb%2BeWp%2B19qNk3aBFlYZvaZ%2FovZYLY6Mp4%3D&se=2017-09-14T12%3A07%3A49Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/57925243735257.jpg', 1, 19, CAST(N'2016-09-14 12:08:09.043' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (9, N'Diving example 2.m4v', N'Diving example 2.m4v', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-7ea12456-c0e8-4f2b-95fc-63b8a41c1356/fn1259967956.mp4?sv=2012-02-12&sr=c&si=91e81615-b977-4fa4-978a-ae65e95f9d0a&sig=aKNbPqPZ%2FxoHYj8yVriQHG%2FY261bBdbsCO33Z19fo40%3D&se=2017-09-14T12%3A08%3A25Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/15048980410199.jpg', 1, 19, CAST(N'2016-09-14 12:08:41.527' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (10, N'Diving example.m4v', N'Diving example.m4v', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-1a7491d7-27f0-40de-b1dc-4404bc147f89/fn335239170.mp4?sv=2012-02-12&sr=c&si=85c2e5d7-d120-416d-a390-0f7c208e6519&sig=Fa97R3kCQTFxROwgIvSTmUgbWGCw1fngcGANc%2F3L96I%3D&se=2017-09-14T12%3A08%3A59Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/64772797115034.jpg', 1, 19, CAST(N'2016-09-14 12:09:13.910' AS DateTime), NULL, NULL, 30, N'https://ikkosmultisports.blob.core.windows.net:443/asset-7ea12456-c0e8-4f2b-95fc-63b8a41c1356/fn1259967956.mp4?sv=2012-02-12&sr=c&si=91e81615-b977-4fa4-978a-ae65e95f9d0a&sig=aKNbPqPZ%2FxoHYj8yVriQHG%2FY261bBdbsCO33Z19fo40%3D&se=2017-09-14T12%3A08%3A25Z', N'https://ikkosmultisports.blob.core.windows.net:443/asset-b47b1215-1352-45fd-80a0-df1e8342b260/fn1513381502.mp4?sv=2012-02-12&sr=c&si=cc10a71d-92c4-49a4-ab6a-4bf53036ea00&sig=Tkp957Q40Di0hi86c5zMvxrMK2NWlzMbIbg60j%2F0CYU%3D&se=2017-09-14T12%3A09%3A53Z', 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (11, N'Kettle Bell - Side up.mp4', N'Kettle Bell - Side up.mp4', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-b47b1215-1352-45fd-80a0-df1e8342b260/fn1513381502.mp4?sv=2012-02-12&sr=c&si=cc10a71d-92c4-49a4-ab6a-4bf53036ea00&sig=Tkp957Q40Di0hi86c5zMvxrMK2NWlzMbIbg60j%2F0CYU%3D&se=2017-09-14T12%3A09%3A53Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/19718199441827.jpg', 1, 19, CAST(N'2016-09-14 12:10:16.500' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (12, N'test package', NULL, N'mp4', NULL, N'https://ikkosvideos.blob.core.windows.net/videosstore/79419940481852.mp4', N'https://ikkosvideos.blob.core.windows.net/imagesstore/76636840421034.png', 1, 11, CAST(N'2016-09-14 12:21:23.040' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (13, N'Med ball chest toss-HD (1).mp4', N'Med ball chest toss-HD (1).mp4', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-fd95cb83-02ec-4c89-8853-97a78bcb1440/fn15338766.mp4?sv=2012-02-12&sr=c&si=793462fc-d09b-48af-bb45-9e50b7a7d704&sig=TpLePIpd%2BfTW20cMpP5yY2GO0UMCHaoTm92fxkfonsQ%3D&se=2017-09-14T12%3A21%3A31Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/2803680231289.jpg', 1, 19, CAST(N'2016-09-14 12:22:12.487' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (14, N'R. McIlroy - Driver down the line.mp4', N'R. McIlroy - Driver down the line.mp4', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-9769bbe7-8a88-4a78-9dbf-5e2272cb92ec/fn294999538.mp4?sv=2012-02-12&sr=c&si=41b28527-4e20-4a24-b4e3-77824eba2f96&sig=R5m9BJEQqMtgo40rehGjq8tp3KXGZ74%2Bg%2BUrG4aTA%2BM%3D&se=2017-09-14T12%3A22%3A52Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/12011896597466.jpg', 1, 19, CAST(N'2016-09-14 12:23:09.217' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (15, N'rotational med ball toss-HD.mp4', N'rotational med ball toss-HD.mp4', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-b73c8ec8-0304-4448-a069-95acdaddee3e/fn1690577024.mp4?sv=2012-02-12&sr=c&si=3a1f0bef-4a0a-4bf8-bb55-44f3a5f30a27&sig=b8VF%2BREu%2FBPen%2FMyKcTnPyGSdxINYAeR9IjNdvBFsCQ%3D&se=2017-09-14T12%3A23%3A37Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/45605223349712.jpg', 1, 19, CAST(N'2016-09-14 12:23:52.773' AS DateTime), NULL, NULL, 30, NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-9769bbe7-8a88-4a78-9dbf-5e2272cb92ec/fn294999538.mp4?sv=2012-02-12&sr=c&si=41b28527-4e20-4a24-b4e3-77824eba2f96&sig=R5m9BJEQqMtgo40rehGjq8tp3KXGZ74%2Bg%2BUrG4aTA%2BM%3D&se=2017-09-14T12%3A22%3A52Z', 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (16, N'testing packages', NULL, N'mp4', NULL, N'https://ikkosvideos.blob.core.windows.net/videosstore/79419940481852.mp4', N'https://ikkosvideos.blob.core.windows.net/imagesstore/76636840421034.png', 1, 11, CAST(N'2016-09-14 12:25:43.730' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (17, N'20 yard shuttle-HD.mp4', N'20 yard shuttle-HD.mp4', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-2cf0bd79-9902-480b-b8f7-8edfbdcd5106/fn1653973043.mp4?sv=2012-02-12&sr=c&si=451c94d1-106d-46ca-b001-49dbb071c15e&sig=314%2FVMYJJoUuC4iO8MY%2BsbBxokM%2FqWRJqftgyBGupaM%3D&se=2017-09-14T12%3A59%3A49Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/98161295468069.jpg', 1, 19, CAST(N'2016-09-14 13:00:03.280' AS DateTime), NULL, NULL, 30, NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-bfc134e2-fe44-4d15-b60f-583876129eb4/fn1494398948.mp4?sv=2012-02-12&sr=c&si=ad91703d-9c9b-46f9-9788-f74268549f64&sig=3Yv4v%2B7XRU5TGa5cpkbFf1%2FV5%2FvLdnBqG4eDkpDkxBI%3D&se=2017-09-14T13%3A05%3A05Z', 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (18, N'broad jump-HD.mp4', N'broad jump-HD.mp4', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-48ab6b09-97aa-4a97-a78c-65f5fe88b97b/fn488943525.mp4?sv=2012-02-12&sr=c&si=3e485c46-1203-43fd-8411-9eab78935f54&sig=YJ6uaefYomXCa1tZd9ZjWqCD8sxudalkNMgqVkwkZtE%3D&se=2017-09-14T13%3A00%3A31Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/4129340984323.jpg', 1, 19, CAST(N'2016-09-14 13:00:38.910' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (19, N'7665855686233.m4v', N'7665855686233.m4v', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-bfc134e2-fe44-4d15-b60f-583876129eb4/fn1494398948.mp4?sv=2012-02-12&sr=c&si=ad91703d-9c9b-46f9-9788-f74268549f64&sig=3Yv4v%2B7XRU5TGa5cpkbFf1%2FV5%2FvLdnBqG4eDkpDkxBI%3D&se=2017-09-14T13%3A05%3A05Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/5080399522324.jpg', 1, 19, CAST(N'2016-09-14 13:05:19.880' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (20, N'Channel_Intro_Video.mp4', N'Channel_Intro_Video.mp4', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-ca086745-a7ea-4fd0-9439-ad397143ff08/fn352706044.mp4?sv=2012-02-12&sr=c&si=7e3d40f2-f82b-4b30-a5c9-1467ae36ffd3&sig=IkB5Kyzc11RYmZD7KoMfSIBrtXhkpwzRNGpHId4iBAQ%3D&se=2017-09-15T08%3A49%3A25Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/21228783038628.jpg', 1, 19, CAST(N'2016-09-15 08:49:47.607' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (21, N'Kettle Bell - Side up.mp4', N'Kettle Bell - Side up.mp4', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-5c07be9c-ab87-4887-89b0-c4419f04bda2/fn2018398427.mp4?sv=2012-02-12&sr=c&si=727ef067-d9c9-4322-9ec3-aace66367373&sig=cSuulzXl9U5bm77zLUQ3k4u8FOkV0kI1PHn3lq1E02c%3D&se=2017-09-15T08%3A54%3A43Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/34967423122914.jpg', 1, 19, CAST(N'2016-09-15 08:55:20.543' AS DateTime), NULL, NULL, 30, N'https://ikkosmultisports.blob.core.windows.net:443/asset-3bad5ee4-b5bf-45f4-9404-e8561018e5a6/fn1783906008.mp4?sv=2012-02-12&sr=c&si=528ca4f0-b80c-4012-a6c5-465fb1e2ddfd&sig=CW4r2AF8nQZb%2BeWp%2B19qNk3aBFlYZvaZ%2FovZYLY6Mp4%3D&se=2017-09-14T12%3A07%3A49Z', N'https://ikkosmultisports.blob.core.windows.net:443/asset-fd95cb83-02ec-4c89-8853-97a78bcb1440/fn15338766.mp4?sv=2012-02-12&sr=c&si=793462fc-d09b-48af-bb45-9e50b7a7d704&sig=TpLePIpd%2BfTW20cMpP5yY2GO0UMCHaoTm92fxkfonsQ%3D&se=2017-09-14T12%3A21%3A31Z', 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (22, N'broad jump-HD.mp4', N'broad jump-HD.mp4', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-83963baf-ecac-4ea0-80ae-991e15037ae0/fn637929513.mp4?sv=2012-02-12&sr=c&si=4e607eab-8dad-4b9a-b810-857f7dca8c89&sig=JQJ2nw3nHEA%2FFZ7yGfdZUyIZ0q7OSZevV3H57vRQ0to%3D&se=2017-09-15T08%3A58%3A14Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/64669489328121.jpg', 1, 19, CAST(N'2016-09-15 08:58:26.750' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (23, N'Diving example.m4v', N'Diving example.m4v', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-d4dcfc90-0901-4066-8abc-5f9ad0e546d5/fn1990170411.mp4?sv=2012-02-12&sr=c&si=eb8aaf13-c3a1-47a7-93f1-41881c248775&sig=nZHa8nDKKP9oiVHIzgNgAB5bty3PahGa64m6HphV7zM%3D&se=2017-09-15T08%3A58%3A45Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/80213030859209.jpg', 1, 19, CAST(N'2016-09-15 08:58:53.933' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (24, N'fn378900669.mp4', N'fn378900669.mp4', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-b59aa8bb-276d-465f-a992-5f2095a5e396/fn1373259240.mp4?sv=2012-02-12&sr=c&si=ed8f0ea9-d79a-4371-af27-c89ae708aef1&sig=3QQgA%2B4hxfFEnOhiHznDXONcq1zRFbHHCGS74wpfM1M%3D&se=2017-09-15T08%3A59%3A10Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/89347558756209.jpg', 1, 19, CAST(N'2016-09-15 08:59:19.607' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (25, N'Kettle Bell - Side up(1).mp4', N'Kettle Bell - Side up(1).mp4', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-02a15dd0-0161-4a57-abd9-5ba5ca3ea0fb/fn1878270747.mp4?sv=2012-02-12&sr=c&si=1eb5b945-0148-4f21-a097-c4b3e912c235&sig=jDYRDxDfvbcv%2FNtq2nGdEk4c5S3ZZFi7bMEAZtJp91c%3D&se=2017-09-15T09%3A25%3A46Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/2532314853478.jpg', 1, 19, CAST(N'2016-09-15 09:25:54.980' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (26, N'Kettle Bell - Side up(1).mp4', N'Kettle Bell - Side up(1).mp4', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-249bbc3b-3838-48f3-a6b7-a166299e0401/fn321420858.mp4?sv=2012-02-12&sr=c&si=d0776b5d-0b87-459f-87f0-d12cfdb6b5ef&sig=FTOdsuOTNKyDRoWTgiaHWGtidx53G6pco%2FlFaI9znTY%3D&se=2017-09-15T09%3A25%3A56Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/31763598497972.jpg', 1, 19, CAST(N'2016-09-15 09:26:02.190' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (27, N'Station 1_Bubbles Intro.mp4', N'Station 1_Bubbles Intro.mp4', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-28d28b4d-c553-4d45-9b47-cea510d68a85/fn1248497300.mp4?sv=2012-02-12&sr=c&si=9ae9e57a-11b4-47d3-bbc7-379dd66968ae&sig=yuzdpqZhANZZZJ2V6XL6rBFN1z5EMJP5VycFU33tXY4%3D&se=2017-09-15T11%3A12%3A36Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/2858451190837.jpg', 1, 19, CAST(N'2016-09-15 11:12:41.567' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (28, N'lohit package', NULL, N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-c88944c9-6eb4-4e7d-bb3e-e2c80404d64b/fn1213856472.mp4?sv=2012-02-12&sr=c&si=77a37dcb-b2d6-4472-8417-9cb47a8d63fb&sig=nbxFipm5vRbHSxy7B4ESAerrUHruYVhN9B5OnCyZhaY%3D&se=2017-09-16T08%3A41%3A17Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/38351265528916.jpg', 1, 19, CAST(N'2016-09-16 08:42:01.490' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (29, N'Vertical Jump - BlastMotion-HD.mp4', N'Vertical Jump - BlastMotion-HD.mp4', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-3ed8f2f7-89dc-4196-b660-a7c6cca8a311/fn83525536.mp4?sv=2012-02-12&sr=c&si=24094b13-e549-4649-974f-96d82374e2ef&sig=L2OJvmJFtW4%2B%2Fcp066rRWqyJ6bZJvhw%2FrYDpnxXAW6U%3D&se=2017-09-19T05%3A38%3A24Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/21423914265359.jpg', 1, 19, CAST(N'2016-09-19 05:39:47.763' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (30, N'asdasd', NULL, N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-2129b561-ca2b-43c3-81d1-b1fa7baf75ff/fn1949083633.mp4?sv=2012-02-12&sr=c&si=a909ea92-a8e5-47fd-aa03-3ce1fdf2fba0&sig=boCGuNfockrs8TlGngtAq3WBo60RbWR87jFvV%2BhHaVU%3D&se=2017-09-20T07%3A21%3A07Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/33023698177018.jpg', 1, 19, CAST(N'2016-09-20 07:21:21.677' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (31, N'asdasd', NULL, N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-cf9b0788-0181-4cd2-a24c-a6c1bda3f76c/fn730708299.mp4?sv=2012-02-12&sr=c&si=8b06de85-a955-43f9-a518-23928301ad90&sig=SRfZ%2Fz0Fy%2F55XhOSZJriOyfn0DnVBT5mlZreirLgu9U%3D&se=2017-09-20T07%3A23%3A09Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/5237372730775.jpg', 1, 19, CAST(N'2016-09-20 07:23:21.880' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (2587, N'UW Backstroke Pull Front View', N'Nick Thomas', N'mp4', NULL, N'https://ikkosvideos.blob.core.windows.net/videosstore/24379716761138.mp4', N'https://ikkosvideos.blob.core.windows.net/imagesstore/83060177747637.png', 1, 1, CAST(N'2016-08-19 08:49:14.180' AS DateTime), 13, CAST(N'2016-08-19 08:49:14.180' AS DateTime), 30, N'https://ikkosblob.blob.core.windows.net/videosstore/34320133880024.mp4', N'https://ikkosblob.blob.core.windows.net/videosstore/89622369255726.mp4', 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (3003, N'Backstroke Front Above Water', N'Jared White', N'mp4', NULL, NULL, N'https://ikkosvideos.blob.core.windows.net/imagesstore/54287893055959.jpg', 4, 2, CAST(N'2016-08-19 08:38:34.280' AS DateTime), 13, CAST(N'2016-08-19 08:38:34.280' AS DateTime), 30, NULL, NULL, 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (3032, N'Butterfly Front', N'Tyler McGill', N'mp4', NULL, NULL, N'https://ikkosvideos.blob.core.windows.net/imagesstore/3020309580267.png', 1, 1, CAST(N'2016-08-19 08:50:56.697' AS DateTime), 13, CAST(N'2016-08-19 08:50:56.697' AS DateTime), 30, NULL, NULL, 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (3037, N'Freestyle Front', N'Adam Brown', N'mp4', NULL, NULL, N'https://ikkosvideos.blob.core.windows.net/imagesstore/4337309931675.png', 1, 1, CAST(N'2016-08-19 08:52:03.790' AS DateTime), 13, CAST(N'2016-08-19 08:52:03.790' AS DateTime), 30, NULL, NULL, 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (4380, N'UW Streamline Side View', N'Charlie Houchin', N'mp4', NULL, NULL, N'https://ikkosvideos.blob.core.windows.net/imagesstore/10990766679641.png', 1, 1, CAST(N'2016-08-19 08:57:42.570' AS DateTime), 13, CAST(N'2016-08-19 08:57:42.570' AS DateTime), 30, NULL, NULL, 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (6995, N'UW Backstroke Pull Full Side View', N'Nick Thomas', N'mp4', NULL, N'https://ikkosvideos.blob.core.windows.net/videosstore/6366774074570.mp4', N'https://ikkosvideos.blob.core.windows.net/imagesstore/8442223762633.png', 1, 10, CAST(N'2016-08-12 09:14:53.297' AS DateTime), NULL, NULL, 30, N'https://ikkosblob.blob.core.windows.net/videosstore/55751537496549.mp4', N'https://ikkosblob.blob.core.windows.net/videosstore/52722152423140.mp4', 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (6996, N'Backstroke Front Above Water', N'Jared White', N'mp4', NULL, N'https://ikkosvideos.blob.core.windows.net/videosstore/76165293094835.mp4', N'https://ikkosvideos.blob.core.windows.net/imagesstore/2091287369216.PNG', 1, 10, CAST(N'2016-08-12 09:16:04.587' AS DateTime), NULL, NULL, 30, N'https://ikkosblob.blob.core.windows.net/videosstore/32342518957258.mp4', N'https://ikkosblob.blob.core.windows.net/videosstore/49260365576841.mp4', 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (6997, N'UW Backstroke Side Full Stroke', N'Nick Thomas', N'mp4', NULL, N'https://ikkosvideos.blob.core.windows.net/videosstore/6366774074570.mp4', N'https://ikkosvideos.blob.core.windows.net/imagesstore/8442223762633.png', 1, 10, CAST(N'2016-08-12 09:17:49.480' AS DateTime), NULL, NULL, 30, N'https://ikkosblob.blob.core.windows.net/videosstore/77972290014759.mp4', N'https://ikkosblob.blob.core.windows.net/videosstore/1722516727643.mp4', 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (6998, N'Package 1_Intro Video', N'Package 1_Intro Video', N'mp4', NULL, N'https://ikkosblob.blob.core.windows.net/videosstore/24522928796985.mp4', N'https://ikkosblob.blob.core.windows.net/imagesstore/60804228730483.PNG', 1, 10, CAST(N'2016-08-12 09:18:48.380' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (6999, N'Channel Intro Video', N'Sean', N'mp4', NULL, N'https://ikkosblob.blob.core.windows.net/videosstore/69667818272028.mp4', N'https://ikkosblob.blob.core.windows.net/imagesstore/52380275783930.PNG', 4, 10, CAST(N'2016-08-12 09:20:27.803' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (7000, N'Intro Video', N'Adam Scott', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-ca086745-a7ea-4fd0-9439-ad397143ff08/fn352706044.mp4?sv=2012-02-12&sr=c&si=7e3d40f2-f82b-4b30-a5c9-1467ae36ffd3&sig=IkB5Kyzc11RYmZD7KoMfSIBrtXhkpwzRNGpHId4iBAQ%3D&se=2017-09-15T08%3A49%3A25Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/21228783038628.jpg', 1, 11, CAST(N'2016-08-12 09:22:20.850' AS DateTime), NULL, NULL, 30, NULL, NULL, 2, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (7001, N'Free Above Sum', N'Satya', N'mp4', NULL, NULL, N'https://ikkosvideos.blob.core.windows.net/imagesstore/95118383573585.jpg', 1, 4, CAST(N'2016-08-23 09:15:16.317' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (7002, N'Fly Above Sum', N'Satya', N'mp4', NULL, NULL, N'https://ikkosvideos.blob.core.windows.net/imagesstore/98258464545691.jpg', 1, 4, CAST(N'2016-08-23 09:20:14.860' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (7003, N'Breaststroke Kick above', N'Satya', N'mp4', NULL, NULL, N'https://ikkosvideos.blob.core.windows.net/imagesstore/5598983896245.jpg', 1, 4, CAST(N'2016-08-23 09:20:14.863' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (7004, N'back above', N'Satya', N'mp4', NULL, NULL, N'https://ikkosvideos.blob.core.windows.net/imagesstore/629788739255.jpg', 1, 4, CAST(N'2016-08-23 09:20:14.870' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (7006, N'Package Introduction', N'Kim', N'mp4', NULL, N'https://ikkosblob.blob.core.windows.net/videosstore/3507039729859.mp4', N'https://ikkosblob.blob.core.windows.net/imagesstore/85460797290103.PNG', 4, 22, CAST(N'2016-09-09 08:13:26.693' AS DateTime), NULL, NULL, 30, N'https://ikkosblob.blob.core.windows.net/videosstore/56518838387485.mp4', NULL, 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (7007, N'Taking Bubbles', N'Tim', N'mp4', NULL, N'https://ikkosvideos.blob.core.windows.net/videosstore/55929691646.mp4', N'https://ikkosvideos.blob.core.windows.net/imagesstore/68628696983998.png', 4, 22, CAST(N'2016-09-09 08:13:46.523' AS DateTime), NULL, NULL, 30, N'https://ikkosblob.blob.core.windows.net/videosstore/56518838387485.mp4', NULL, 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (7008, N'Bubbles 1 skill video 2', N'jim', N'mp4', NULL, N'https://ikkosvideos.blob.core.windows.net/videosstore/79419940481852.mp4', N'https://ikkosvideos.blob.core.windows.net/imagesstore/76636840421034.png', 4, 22, CAST(N'2016-09-09 08:39:22.997' AS DateTime), NULL, NULL, 30, N'https://ikkosblob.blob.core.windows.net/videosstore/67037392543664.mp4', NULL, 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (7009, N'Bubbles 2 Skill Video 1', N'jim', N'mp4', NULL, N'https://ikkosvideos.blob.core.windows.net/videosstore/11172140669490.mp4', N'https://ikkosvideos.blob.core.windows.net/imagesstore/41627440630408.png', 4, 22, CAST(N'2016-09-09 08:42:40.723' AS DateTime), NULL, NULL, 30, N'https://ikkosblob.blob.core.windows.net/videosstore/20709113394934.mp4', NULL, 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (7010, N'Bubbles2-skillvideo2', N'jim', N'mp4', NULL, N'https://ikkosvideos.blob.core.windows.net/videosstore/93712932619663.mp4', N'https://ikkosvideos.blob.core.windows.net/imagesstore/94672532662682.png', 4, 22, CAST(N'2016-09-09 08:48:11.327' AS DateTime), NULL, NULL, 30, N'https://ikkosblob.blob.core.windows.net/videosstore/8378046572743.mp4', NULL, 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (7011, N'Channel Intro Video.MOV', N'Channel Intro Video.MOV', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-ecf3fa45-80b0-4067-83f5-78f6ef478c77/fn516094714.quicktime?sv=2012-02-12&sr=c&si=a9aa3383-6edf-4ae0-8627-d961c4987eef&sig=Z0%2FhpHiRfwRWvDmUWwUj7hW2iTEnAY9JGeEV%2FixKAE8%3D&se=2017-09-21T09%3A34%3A57Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/65279993960858.jpg', 1, 19, CAST(N'2016-09-21 09:35:10.860' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (7012, N'testing package', NULL, N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-e1bfff8c-e400-4bbf-ad11-8cc6482be972/fn1512219979.mp4?sv=2012-02-12&sr=c&si=6043a7c9-2000-4971-a1cc-e45c657a0c75&sig=JMSPRFDoD5hVqMUkabf6xMbU5O0m2GPZUc7ZkGTtGv0%3D&se=2017-09-24T09%3A43%3A00Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/5771037207121.jpg', 1, 19, CAST(N'2016-09-24 09:43:44.943' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (7013, N'test', NULL, N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-e1741829-bfb7-46db-b580-fefff6fcaa12/fn1242697521.mp4?sv=2012-02-12&sr=c&si=d0dfbb8e-8d4e-47ac-8ffe-51f3e19ab770&sig=OVuo%2F%2FCnAsoYhmDJtsAl4B2ec1UP6s9wf4O3EYIQHuc%3D&se=2017-09-24T09%3A56%3A02Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/52032235405.jpg', 1, 19, CAST(N'2016-09-24 09:57:06.350' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (7014, N'', NULL, N'mp4', NULL, N'', N'', 1, 0, CAST(N'2016-09-24 10:07:20.353' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (7015, N'TestingIkkosAdmin', NULL, N'mp4', NULL, N'https://ikkosvideos.blob.core.windows.net/videosstore/79419940481852.mp4', N'https://ikkosvideos.blob.core.windows.net/imagesstore/76636840421034.png', 1, 19, CAST(N'2016-09-24 10:13:55.990' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (7016, N'Channel Intro Video', N'Channel', N'mp4', NULL, N'https://ikkosblob.blob.core.windows.net/videosstore/69667818272028.mp4', N'https://ikkosblob.blob.core.windows.net/imagesstore/52380275783930.PNG', 1, 23, CAST(N'2016-09-26 06:05:46.790' AS DateTime), NULL, NULL, 1, NULL, NULL, 2, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (7017, N'Channel Intro Video', N'Channel', N'mp4', NULL, N'https://ikkosblob.blob.core.windows.net/videosstore/69667818272028.mp4', N'https://ikkosblob.blob.core.windows.net/imagesstore/52380275783930.PNG', 1, 24, CAST(N'2016-09-26 06:22:25.397' AS DateTime), NULL, NULL, 1, NULL, NULL, 2, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (7018, N'Channel Intro Video', N'Channel', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-788ba61a-312c-41ef-9458-15e2c8f5de15/fn2122850393.mp4?sv=2012-02-12&sr=c&si=22a02e27-71d8-46de-bb11-138232e9da6b&sig=7lYPoWNU3NEzxAuRQOwHHIVSFkoRWs%2FXOCTodHY6ebU%3D&se=2017-09-26T07%3A33%3A20Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/9582897967945.jpg', 1, 25, CAST(N'2016-09-26 07:33:53.020' AS DateTime), NULL, NULL, 1, NULL, NULL, 2, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (7019, N'testing volley', NULL, N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-aec6033b-1e4b-4cc5-977c-359c76daca11/fn95284557.mp4?sv=2012-02-12&sr=c&si=6f2f4cd8-6e07-48d8-ab63-c93d8fc35d22&sig=d211DgdXHl2EEZCcG9sPbRAOTnA9fzIcBca1vnqqPaY%3D&se=2017-09-26T08%3A29%3A11Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/55666368613096.jpg', 1, 19, CAST(N'2016-09-26 08:29:42.630' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (7020, N'20 yard shuttle-HD.mp4', N'20 yard shuttle-HD.mp4', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-f686cb04-7c3a-41cb-b514-e2919b5d4a05/fn1164618751.mp4?sv=2012-02-12&sr=c&si=8848d6fe-f684-497c-9a59-860d6ac164ac&sig=YH2h6grcHtSqxI6tO1HyJfgQStG7O5VAUlK33tmfFSc%3D&se=2017-09-26T08%3A32%3A14Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/38271271338322.jpg', 1, 25, CAST(N'2016-09-26 08:32:23.037' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (7021, N'7665855686233.m4v', N'7665855686233.m4v', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-cc013a06-f5ad-493f-9ae6-415f3bbc0c96/fn338895764.mp4?sv=2012-02-12&sr=c&si=9ab9b7de-f7dc-47a3-8311-63ececaf398c&sig=PcU9Q7BXr%2FnAMbl6CeXC%2BFYZo8QvE6KMn%2FeF4MC4djM%3D&se=2017-09-26T08%3A32%3A43Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/79227674126085.jpg', 1, 25, CAST(N'2016-09-26 08:32:49.617' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (7022, N'63277951860656.m4v', N'63277951860656.m4v', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-0f75c7bb-7736-4096-a31d-ef55e1070eee/fn1532278215.mp4?sv=2012-02-12&sr=c&si=27744b92-743d-4342-9ed4-f66c1ed7e5d7&sig=tQ739P7wOKdndKjv0hPuTt5xCJJWDjdgDoVQ82zc0u0%3D&se=2017-09-26T08%3A33%3A08Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/32302861053339.jpg', 1, 25, CAST(N'2016-09-26 08:33:15.913' AS DateTime), NULL, NULL, 30, NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-f686cb04-7c3a-41cb-b514-e2919b5d4a05/fn1164618751.mp4?sv=2012-02-12&sr=c&si=8848d6fe-f684-497c-9a59-860d6ac164ac&sig=YH2h6grcHtSqxI6tO1HyJfgQStG7O5VAUlK33tmfFSc%3D&se=2017-09-26T08%3A32%3A14Z', 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (7023, N'broad jump-HD.mp4', N'broad jump-HD.mp4', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-816e79d2-aec1-49ea-9137-bec0b89183c2/fn397996198.mp4?sv=2012-02-12&sr=c&si=deadf1ab-3942-4845-9c10-a759b2654445&sig=NQTQBlJzSPz0nkJUB%2FCdOtybTRZPY5E2KuE%2Bq%2B0DEao%3D&se=2017-09-26T08%3A33%3A38Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/27337491538813.jpg', 1, 25, CAST(N'2016-09-26 08:33:43.203' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (7024, N'Diving example.m4v', N'Diving example.m4v', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-7afd40ea-932c-4e53-a72f-1c37e03570ed/fn956587126.mp4?sv=2012-02-12&sr=c&si=98f86874-a6d7-4d53-9645-1e93fbbbf79b&sig=5%2Fm3qwNbOkj9Np9nBAskxnnHRCq2ruKCoDiGvlFYEXA%3D&se=2017-09-26T08%3A34%3A02Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/36471920535814.jpg', 1, 25, CAST(N'2016-09-26 08:34:10.370' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (7025, N'fn1531408604.mp4', N'fn1531408604.mp4', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-7d285518-3394-41ab-ad51-d50732ae4d89/fn1655938461.mp4?sv=2012-02-12&sr=c&si=a4ace582-d15c-47b3-9ebc-3b0375a42b65&sig=C91p5mUQcOam9Xz68hCDSq5yEh22nXdtJORbEsq8v10%3D&se=2017-09-26T08%3A34%3A25Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/17931686627794.jpg', 1, 25, CAST(N'2016-09-26 08:34:31.590' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (7026, N'The Labs USA', N'Richard', N'mp4', NULL, N'', N'https://ikkosvideos.blob.core.windows.net/imagesstore/98858180558176.png', 1, 29, CAST(N'2016-09-26 09:02:22.060' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (7027, N'basic volley package22', NULL, N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-ef0a7c1b-9f76-4bea-9ffc-fa06b69f1fe0/fn410565395.mp4?sv=2012-02-12&sr=c&si=c9946e93-1b23-4889-a662-2a514a7bc0f3&sig=%2BX%2BTlF%2F7hH7%2BmAMWAKiP4yVNT1cthleU2B4dOxDQdgc%3D&se=2017-09-26T09%3A07%3A10Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/15850942336694.jpg', 1, 19, CAST(N'2016-09-26 09:07:57.270' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (7028, N'Kim Brackin', N'Kim', N'mp4', NULL, N'', N'https://ikkosmultisports.blob.core.windows.net/asset-76f157b4-c887-4895-a4b3-e21b13a083da/fn217339468.png?sv=2012-02-12&sr=c&si=32cccfc5-9ea7-4fd2-a776-a812a6360bf8&sig=lFDF8nxEx7LeYEJddWe6ohBrQZ9imT3Uz0536LuALy8%3D&se=2017-09-15T09%3A46%3A32Z', 1, 7, CAST(N'2016-09-26 09:10:13.423' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (7029, N'Box Squat Front View', N'Jack', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-14d74b43-8807-4201-8809-0dbd3c3d612e/fn1630816083.mp4?sv=2012-02-12&sr=c&si=ed992933-5548-48b7-9dcb-1529d713587c&sig=lMwNq%2BTIo3njnMgT%2FnorpCWMEx1y8v92oaXfHnIM2f0%3D&se=2017-10-10T11%3A19%3A03Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/28196636187794.jpg', 1, 29, CAST(N'2016-10-10 11:19:11.687' AS DateTime), NULL, NULL, 30, N'https://ikkosmultisports.blob.core.windows.net:443/asset-ab6dfedf-ede5-4926-aa6a-3e045ea252e7/fn819492650.mp4?sv=2012-02-12&sr=c&si=c62d214d-193f-40cc-aae0-61e566caf576&sig=k3fdbVkmpqiSopugUulK5kjXaYx7RWKisFAO2GO8dG0%3D&se=2017-10-10T11%3A19%3A38Z', N'https://ikkosmultisports.blob.core.windows.net:443/asset-010f6fb7-8817-4c9f-ad9d-8366e304f2b3/fn430144399.mp4?sv=2012-02-12&sr=c&si=66cabf46-d6a7-42c6-8085-ded0e7db7b7a&sig=WcP8AcpvAto7V47tuKwA6d6t0xDeACOjgheY6wXPosg%3D&se=2017-10-10T11%3A19%3A22Z', 1, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (7030, N'Box Squat Post-skill Instruction.mp4', N'Box Squat Post-skill Instruction.mp4', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-8c756d34-35fa-4174-9c87-43dac8e56ca7/fn853820099.mp4?sv=2012-02-12&sr=c&si=520a03a8-8da2-43ab-9914-116ea73e49e2&sig=LAF6wTqe8xauPq3fqfxUY3YPvvQ3JnkMKG%2BVBLkYJt4%3D&se=2017-10-10T11%3A19%3A16Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/6703095676695.jpg', 1, 29, CAST(N'2016-10-10 11:19:18.657' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (7031, N'Box Squat Post-skill Instruction1.mp4', N'Box Squat Post-skill Instruction1.mp4', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-010f6fb7-8817-4c9f-ad9d-8366e304f2b3/fn430144399.mp4?sv=2012-02-12&sr=c&si=66cabf46-d6a7-42c6-8085-ded0e7db7b7a&sig=WcP8AcpvAto7V47tuKwA6d6t0xDeACOjgheY6wXPosg%3D&se=2017-10-10T11%3A19%3A22Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/97136864646530.jpg', 1, 29, CAST(N'2016-10-10 11:19:24.733' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (7032, N'Box Squat Side View', N'Jack', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-ddb84ccf-a761-4d6c-b57a-21320e73bf0f/fn776996627.mp4?sv=2012-02-12&sr=c&si=6e5efa17-b5c7-4be1-867a-762171c9f1d6&sig=znoXL%2BNlQ4Y35tInYJBxZuodKgK3o2a8J05x3QokwHA%3D&se=2017-10-10T11%3A19%3A27Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/5821182117176.jpg', 1, 29, CAST(N'2016-10-10 11:19:29.063' AS DateTime), NULL, NULL, 30, N'https://ikkosmultisports.blob.core.windows.net:443/asset-509aac48-164e-4570-82d5-3ca3b458eb37/fn779399784.mp4?sv=2012-02-12&sr=c&si=ed04d8f7-cde1-4181-a3a7-92fa85020bfc&sig=SqHqtW3qNA5wcSk7dgbO342wW9kN5LL1uOUUe2p8fi8%3D&se=2017-10-10T11%3A19%3A32Z', N'https://ikkosmultisports.blob.core.windows.net:443/asset-8c756d34-35fa-4174-9c87-43dac8e56ca7/fn853820099.mp4?sv=2012-02-12&sr=c&si=520a03a8-8da2-43ab-9914-116ea73e49e2&sig=LAF6wTqe8xauPq3fqfxUY3YPvvQ3JnkMKG%2BVBLkYJt4%3D&se=2017-10-10T11%3A19%3A16Z', 1, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (7033, N'Squat Pre-skill Instruction.mp4', N'Squat Pre-skill Instruction.mp4', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-509aac48-164e-4570-82d5-3ca3b458eb37/fn779399784.mp4?sv=2012-02-12&sr=c&si=ed04d8f7-cde1-4181-a3a7-92fa85020bfc&sig=SqHqtW3qNA5wcSk7dgbO342wW9kN5LL1uOUUe2p8fi8%3D&se=2017-10-10T11%3A19%3A32Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/63565487291015.jpg', 1, 29, CAST(N'2016-10-10 11:19:35.017' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (7034, N'Squat Pre-skill Instruction1 (2).mp4', N'Squat Pre-skill Instruction1 (2).mp4', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-ab6dfedf-ede5-4926-aa6a-3e045ea252e7/fn819492650.mp4?sv=2012-02-12&sr=c&si=c62d214d-193f-40cc-aae0-61e566caf576&sig=k3fdbVkmpqiSopugUulK5kjXaYx7RWKisFAO2GO8dG0%3D&se=2017-10-10T11%3A19%3A38Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/84620632834050.jpg', 1, 29, CAST(N'2016-10-10 11:19:41.407' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (7035, N'Box Squat Front', N'Jack', N'mp4', NULL, N'', N'', 1, 29, CAST(N'2016-10-10 11:48:16.810' AS DateTime), NULL, NULL, 30, NULL, NULL, 3, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (7036, N'Box Squat Front', NULL, N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-c7c3cec9-f277-4892-b6a8-f788469a94db/fn1702130100.mp4?sv=2012-02-12&sr=c&si=584455d8-f84f-40d4-9f79-7cc042c14f3c&sig=Yj78ZiDBGhmmNjUIjRbJkTIs%2BVADOo7to1bR%2BhcT5vw%3D&se=2017-10-10T11%3A52%3A36Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/66836082970337.jpg', 1, 29, CAST(N'2016-10-10 11:52:39.500' AS DateTime), NULL, NULL, 30, NULL, NULL, 3, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (7037, N'Cross Over Approach Front - Skill Intro.mp4', N'Cross Over Approach Front - Skill Intro.mp4', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-b8220b45-91b1-40a1-84a8-3268cec8550c/fn261819337.mp4?sv=2012-02-12&sr=c&si=7417ba6b-b199-4355-93f6-fa1ba1a2d1e5&sig=rUafBVXcpGcIg56VnVdMt4UTDtuSkicG%2FFSylgKPoAs%3D&se=2017-10-10T11%3A55%3A05Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/54182667730409.jpg', 1, 7, CAST(N'2016-10-10 11:55:08.177' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (7038, N'Cross Over Approach Front - Skill Intro1.mp4', N'Cross Over Approach Front - Skill Intro1.mp4', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-abf77bc6-072f-40c5-a214-d1a627edba1a/fn1231516266.mp4?sv=2012-02-12&sr=c&si=2f638788-7c18-4bf3-97e3-e8fb6a45c619&sig=om8QrTjs%2Fcf2KBtrD1cLJAfp0NeUnTYIDMsdKQP4MpE%3D&se=2017-10-10T11%3A55%3A14Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/174534228373.jpg', 1, 7, CAST(N'2016-10-10 11:55:16.880' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (7039, N'Cross Over Approach Front - Skill Next Step.mp4', N'Cross Over Approach Front - Skill Next Step.mp4', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-ecd19eb3-900b-470c-8624-aa8ed868dff7/fn2013941441.mp4?sv=2012-02-12&sr=c&si=8085546d-8cc2-4b79-b1c5-67d77a616759&sig=RagRfD1l48%2FmX632XsCp5s7wzFHh%2BV6Qh1zAmq%2Fxd8o%3D&se=2017-10-10T11%3A55%3A20Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/36754963879267.jpg', 1, 7, CAST(N'2016-10-10 11:55:23.570' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (7040, N'Cross Over Approach Front - Skill Next Step1.mp4', N'Cross Over Approach Front - Skill Next Step1.mp4', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-23ca4005-6ca9-477f-ad9b-e1e0474e36d8/fn1964977362.mp4?sv=2012-02-12&sr=c&si=b01bc56a-87c6-4bd5-9822-d04e377dc10d&sig=Tw9JAUhxPtnZYenn2btCVwhWaQCaT3Y167vxUFeKGzY%3D&se=2017-10-10T11%3A55%3A27Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/93690610291771.jpg', 1, 7, CAST(N'2016-10-10 11:55:29.990' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (7041, N'Cross Over Approach_Turn Front View.mp4', N'Cross Over Approach_Turn Front View.mp4', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-ad44f02e-b3ca-4a3b-9919-1d14df3d4cb9/fn1122145625.mp4?sv=2012-02-12&sr=c&si=f9bd1006-9655-4d05-820d-7a1fdc932664&sig=6ED95CJxybB3wyFSFzHsMzkuVksLOVfPLZitbajx8MY%3D&se=2017-10-10T11%3A55%3A31Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/63816310359218.jpg', 1, 7, CAST(N'2016-10-10 11:55:33.990' AS DateTime), NULL, NULL, 30, N'https://ikkosmultisports.blob.core.windows.net:443/asset-b8220b45-91b1-40a1-84a8-3268cec8550c/fn261819337.mp4?sv=2012-02-12&sr=c&si=7417ba6b-b199-4355-93f6-fa1ba1a2d1e5&sig=rUafBVXcpGcIg56VnVdMt4UTDtuSkicG%2FFSylgKPoAs%3D&se=2017-10-10T11%3A55%3A05Z', N'https://ikkosmultisports.blob.core.windows.net:443/asset-ecd19eb3-900b-470c-8624-aa8ed868dff7/fn2013941441.mp4?sv=2012-02-12&sr=c&si=8085546d-8cc2-4b79-b1c5-67d77a616759&sig=RagRfD1l48%2FmX632XsCp5s7wzFHh%2BV6Qh1zAmq%2Fxd8o%3D&se=2017-10-10T11%3A55%3A20Z', 1, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (7042, N'Cross Over Approach_Turn Side View.mp4', N'Cross Over Approach_Turn Side View.mp4', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-ed824300-aa3f-4247-b19f-c630133822a6/fn1251127323.mp4?sv=2012-02-12&sr=c&si=6330d5ed-429e-4224-8396-478414105e46&sig=i1Dao%2FGp0LHIoPwEVaWjDfK9Hryr4b347Bl4cKHAQho%3D&se=2017-10-10T11%3A55%3A35Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/28708190070267.jpg', 1, 7, CAST(N'2016-10-10 11:55:37.770' AS DateTime), NULL, NULL, 30, N'https://ikkosmultisports.blob.core.windows.net:443/asset-abf77bc6-072f-40c5-a214-d1a627edba1a/fn1231516266.mp4?sv=2012-02-12&sr=c&si=2f638788-7c18-4bf3-97e3-e8fb6a45c619&sig=om8QrTjs%2Fcf2KBtrD1cLJAfp0NeUnTYIDMsdKQP4MpE%3D&se=2017-10-10T11%3A55%3A14Z', N'https://ikkosmultisports.blob.core.windows.net:443/asset-23ca4005-6ca9-477f-ad9b-e1e0474e36d8/fn1964977362.mp4?sv=2012-02-12&sr=c&si=b01bc56a-87c6-4bd5-9822-d04e377dc10d&sig=Tw9JAUhxPtnZYenn2btCVwhWaQCaT3Y167vxUFeKGzY%3D&se=2017-10-10T11%3A55%3A27Z', 1, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (7043, N'Turn Front  and Side', NULL, N'mp4', NULL, N'', N'', 1, 7, CAST(N'2016-10-10 12:30:14.073' AS DateTime), NULL, NULL, 30, NULL, NULL, 3, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode]) VALUES (7044, N'Channel Intro Video', N'Channel', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-584690e0-9a43-431b-808e-dd73ad8d828e/fn1377247995.mp4?sv=2012-02-12&sr=c&si=9809f802-83cc-4db9-8b8b-dabebcf9facb&sig=L%2BOEQfa%2BHuTVa6ngzPAZm94UKu06CC1ERS3DlcbKn1A%3D&se=2017-10-12T13%3A00%3A42Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/16927369622292.jpg', 1, 30, CAST(N'2016-10-12 13:00:54.213' AS DateTime), NULL, NULL, 1, NULL, NULL, 2, 0)
SET IDENTITY_INSERT [dbo].[Videos] OFF
SET IDENTITY_INSERT [dbo].[VideoTypes] ON 

INSERT [dbo].[VideoTypes] ([Id], [VideoType]) VALUES (1, N'Actual Video')
INSERT [dbo].[VideoTypes] ([Id], [VideoType]) VALUES (2, N'Channel Intro Video')
INSERT [dbo].[VideoTypes] ([Id], [VideoType]) VALUES (3, N'Package Intro Video')
SET IDENTITY_INSERT [dbo].[VideoTypes] OFF
SET ANSI_PADDING ON

GO
/****** Object:  Index [ucShardMappingsLocal_ShardMapId_MinValue]    Script Date: 21-10-2016 11:55:40 ******/
ALTER TABLE [__ShardManagement].[ShardMappingsLocal] ADD  CONSTRAINT [ucShardMappingsLocal_ShardMapId_MinValue] UNIQUE NONCLUSTERED 
(
	[ShardMapId] ASC,
	[MinValue] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ucShardsLocal_ShardMapId_Location]    Script Date: 21-10-2016 11:55:40 ******/
ALTER TABLE [__ShardManagement].[ShardsLocal] ADD  CONSTRAINT [ucShardsLocal_ShardMapId_Location] UNIQUE NONCLUSTERED 
(
	[ShardMapId] ASC,
	[Protocol] ASC,
	[ServerName] ASC,
	[DatabaseName] ASC,
	[Port] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
ALTER TABLE [__ShardManagement].[ShardMappingsLocal]  WITH CHECK ADD  CONSTRAINT [fkShardMappingsLocal_ShardId] FOREIGN KEY([ShardId])
REFERENCES [__ShardManagement].[ShardsLocal] ([ShardId])
GO
ALTER TABLE [__ShardManagement].[ShardMappingsLocal] CHECK CONSTRAINT [fkShardMappingsLocal_ShardId]
GO
ALTER TABLE [__ShardManagement].[ShardMappingsLocal]  WITH CHECK ADD  CONSTRAINT [fkShardMappingsLocal_ShardMapId] FOREIGN KEY([ShardMapId])
REFERENCES [__ShardManagement].[ShardMapsLocal] ([ShardMapId])
GO
ALTER TABLE [__ShardManagement].[ShardMappingsLocal] CHECK CONSTRAINT [fkShardMappingsLocal_ShardMapId]
GO
ALTER TABLE [__ShardManagement].[ShardsLocal]  WITH CHECK ADD  CONSTRAINT [fkShardsLocal_ShardMapId] FOREIGN KEY([ShardMapId])
REFERENCES [__ShardManagement].[ShardMapsLocal] ([ShardMapId])
GO
ALTER TABLE [__ShardManagement].[ShardsLocal] CHECK CONSTRAINT [fkShardsLocal_ShardMapId]
GO
USE [master]
GO
ALTER DATABASE [ElasticScaleStarterKit_Shard0] SET  READ_WRITE 
GO
