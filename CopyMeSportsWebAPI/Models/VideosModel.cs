﻿using CopyMeSportsWebAPI.ShardUtils;
using Microsoft.Azure.SqlDatabase.ElasticScale.Query;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CopyMeSportsWebAPI.Models
{
    public class VideosModel
    {
        internal static List<VideosInfoDTO> AllVideos(int userId)
        {
            List<VideosInfoDTO> videos = new List<VideosInfoDTO>();

            try
            {
                SqlCommand sqlCommand = new SqlCommand("[GetAllVideos]");
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@UserID", userId);
                SqlDataReader myData;

                BaseUtils.executeReaderCommandOnSingleShard(userId, sqlCommand, out myData);

                while (myData != null && myData.Read())
                {
                    VideosInfoDTO v = new VideosInfoDTO();
                    v.ID = myData["ID"] == DBNull.Value ? 0 : Convert.ToInt32(myData["ID"]);
                    v.Segment = myData["segment"] == DBNull.Value ? string.Empty : Convert.ToString(myData["segment"]);
                    v.Category = myData["category"] == DBNull.Value ? string.Empty : Convert.ToString(myData["category"]);
                    v.SubCategory = myData["subcategory"] == DBNull.Value ? string.Empty : Convert.ToString(myData["subcategory"]);
                    v.MovementName = myData["movementname"] == DBNull.Value ? string.Empty : Convert.ToString(myData["movementname"]);
                    v.ActorName = myData["actorname"] == DBNull.Value ? string.Empty : Convert.ToString(myData["actorname"]);
                    v.Orientation = myData["orientation"] == DBNull.Value ? 0 : Convert.ToInt32(myData["orientation"]);
                    v.Facing = myData["facing"] == DBNull.Value ? 0 : Convert.ToInt32(myData["facing"]);
                    int rate = myData["averagerating"] == DBNull.Value ? 0 : Convert.ToInt32(myData["averagerating"]);
                    v.Rating = rate >= 0 ? (float)(rate / 20.0) : (float)0.0;
                    v.URL = myData["url"] == DBNull.Value ? string.Empty : Convert.ToString(myData["url"]);
                    v.ThumbNailURL = myData["thumbnailurl"] == DBNull.Value ? string.Empty : Convert.ToString(myData["thumbnailurl"]);
                    v.PlayCount = myData["PlayCount"] == DBNull.Value ? 0 : Convert.ToInt32(myData["PlayCount"]);
                    int t = myData["type"] == DBNull.Value ? 0 : Convert.ToInt32(myData["type"]);
                    v.Paid = t == 13 ? 1 : 0;
                    int VideoId = myData["videoId"] == DBNull.Value ? 0 : Convert.ToInt32(myData["videoId"]);
                    v.isPurchased = (VideoId == 0) ? false : true;
                    videos.Add(v);
                }
                myData.Close();
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return videos;
        }

        internal static VideosInfoDTO GetVideo(int videoId)
        {
            VideosInfoDTO video = new VideosInfoDTO();

            try
            {
                SqlCommand sqlCommand = new SqlCommand("[GetVideo]");
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader myData;

                BaseUtils.executeReaderCommandOnSingleShard(videoId, sqlCommand, out myData);

                while (myData.Read())
                {
                    video.ID = myData["ID"] == DBNull.Value ? 0 : Convert.ToInt32(myData["ID"]);
                    video.Segment = myData["segment"] == DBNull.Value ? string.Empty : Convert.ToString(myData["segment"]);
                    video.Category = myData["category"] == DBNull.Value ? string.Empty : Convert.ToString(myData["category"]);
                    video.SubCategory = myData["subcategory"] == DBNull.Value ? string.Empty : Convert.ToString(myData["subcategory"]);
                    video.MovementName = myData["movementname"] == DBNull.Value ? string.Empty : Convert.ToString(myData["movementname"]);
                    video.ActorName = myData["actorname"] == DBNull.Value ? string.Empty : Convert.ToString(myData["actorname"]);
                    video.Orientation = myData["orientation"] == DBNull.Value ? 0 : Convert.ToInt32(myData["orientation"]);
                    video.Facing = myData["facing"] == DBNull.Value ? 0 : Convert.ToInt32(myData["facing"]);
                    int rate = myData["averagerating"] == DBNull.Value ? 0 : Convert.ToInt32(myData["averagerating"]);
                    video.Rating = rate >= 0 ? (float)(rate / 20.0) : (float)0.0;
                    video.URL = myData["url"] == DBNull.Value ? string.Empty : Convert.ToString(myData["url"]);
                    video.ThumbNailURL = myData["thumbnailurl"] == DBNull.Value ? string.Empty : Convert.ToString(myData["thumbnailurl"]);
                    video.PlayCount = myData["PlayCount"] == DBNull.Value ? 0 : Convert.ToInt32(myData["PlayCount"]);
                }
                myData.Close();
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return video;
        }
        /// <summary>
        /// Getting Video of the Day based on the UserId and CategoryId
        /// </summary>
        /// <param name="userId">UserId</param>
        /// <param name="categoryId">CategoryId</param>
        /// <param name="DeviceId">Device unique Id</param>
        /// <returns>PromotionalContentDto</returns>
        internal static PromotionalContentDTO GetVideoOfTheDay(int userId, int categoryId, int DeviceId)
        {
            PromotionalContentDTO promoContent = null;

            try
            {

                SqlCommand sqlCommand = new SqlCommand("[GetFeaturedVideo]");
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@UserID", userId);
                sqlCommand.Parameters.AddWithValue("@CategoryID", categoryId);
                sqlCommand.Parameters.AddWithValue("@Date", DateTime.Now);
                sqlCommand.Parameters.AddWithValue("@deviceId", DeviceId);
                SqlDataReader myData;

                BaseUtils.executeReaderCommandOnSingleShard(userId, sqlCommand, out myData);

                while (myData != null && myData.Read())
                {
                    promoContent = new PromotionalContentDTO
                    {
                        Id = myData["ID"] == DBNull.Value ? 0 : Convert.ToInt32(myData["ID"]),
                        Title = myData["Title"] == DBNull.Value ? string.Empty : Convert.ToString(myData["Title"]),
                        Name = myData["Name"] == DBNull.Value ? string.Empty : Convert.ToString(myData["Name"]),
                        Description = myData["Description"] == DBNull.Value ? string.Empty : Convert.ToString(myData["Description"]),
                        ThumbNailURL = myData["ThumbNailURL"] == DBNull.Value ? string.Empty : Convert.ToString(myData["ThumbNailURL"]),
                        URL = myData["URL"] == DBNull.Value ? string.Empty : Convert.ToString(myData["URL"]),
                        ActionType = myData["ActionType"] == DBNull.Value ? string.Empty : Convert.ToString(myData["ActionType"]),
                        EventStartDate = (DateTime)myData["EventStartDate"],
                        EventEndDate = (DateTime)myData["EventEndDate"],
                        ActionTypeID = myData["Type"] == DBNull.Value ? 0 : Convert.ToInt32(myData["Type"]),
                        Status = myData["Status"] == DBNull.Value ? 0 : Convert.ToInt32(myData["Status"])
                    };
                }
                myData.Close();
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return promoContent;
        }

        internal static object GetChannelIntroVideos(int libraryId, int userId)
        {
            VideosInfoDTO v = null;

            try
            {
                SqlCommand sqlCommand = new SqlCommand("[GetChannelIntroVideos]");
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@LibraryId", libraryId);
                sqlCommand.Parameters.AddWithValue("@UserId", userId);
                SqlDataReader myData;

                BaseUtils.executeReaderCommandOnSingleShard(userId, sqlCommand, out myData);

                while (myData != null && myData.Read())
                {
                    v = new VideosInfoDTO();
                    v.ID = myData["ID"] == DBNull.Value ? 0 : Convert.ToInt32(myData["ID"]);
                    v.Description = myData["Description"] == DBNull.Value ? string.Empty : Convert.ToString(myData["Description"]);
                    v.MovementName = myData["movementname"] == DBNull.Value ? string.Empty : Convert.ToString(myData["movementname"]);
                    v.ActorName = myData["actorname"] == DBNull.Value ? string.Empty : Convert.ToString(myData["actorname"]);
                    v.URL = myData["VideoUrl"] == DBNull.Value ? string.Empty : Convert.ToString(myData["VideoUrl"]);
                    v.ThumbNailURL = myData["thumbnailurl"] == DBNull.Value ? string.Empty : Convert.ToString(myData["thumbnailurl"]);
                    v.PlayCount = myData["PlayCount"] == DBNull.Value ? 0 : Convert.ToInt32(myData["PlayCount"]);
                    v.Price = myData["Price"] == DBNull.Value ? string.Empty : Convert.ToString(myData["Price"]);
                    v.SKUID = myData["SKUID"] == DBNull.Value ? string.Empty : Convert.ToString(myData["SKUID"]);
                }
                myData.Close();
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return v;
        }

        /// <summary>
        /// Getting the introduction video for every library(Club)
        /// </summary>
        /// <param name="libraryId">Library Id(Club Id)</param>
        /// <param name="userId">User unique Id</param>
        /// <returns>Video Information DTO</returns>

        internal static VideosInfoDTO GetLibraryIntroVideos(int libraryId, int userId)
        {
           VideosInfoDTO v = null;

            try
            {
                SqlCommand sqlCommand = new SqlCommand("[GetLibraryIntroVideos]");
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@LibraryId", libraryId);
                sqlCommand.Parameters.AddWithValue("@UserId", userId);
                SqlDataReader myData;

                BaseUtils.executeReaderCommandOnSingleShard(userId, sqlCommand, out myData);

                while (myData != null && myData.Read())
                {
                     v = new VideosInfoDTO();
                    v.ID = myData["ID"] == DBNull.Value ? 0 : Convert.ToInt32(myData["ID"]);
                    v.Description = myData["Description"] == DBNull.Value ? string.Empty : Convert.ToString(myData["Description"]);
                    v.Segment = myData["segment"] == DBNull.Value ? string.Empty : Convert.ToString(myData["segment"]);
                    v.Category = myData["category"] == DBNull.Value ? string.Empty : Convert.ToString(myData["category"]);
                    v.SubCategory = myData["subcategory"] == DBNull.Value ? string.Empty : Convert.ToString(myData["subcategory"]);
                    v.MovementName = myData["movementname"] == DBNull.Value ? string.Empty : Convert.ToString(myData["movementname"]);
                    v.ActorName = myData["actorname"] == DBNull.Value ? string.Empty : Convert.ToString(myData["actorname"]);
                    v.Orientation = myData["orientation"] == DBNull.Value ? 0 : Convert.ToInt32(myData["orientation"]);
                    v.Facing = myData["facing"] == DBNull.Value ? 0 : Convert.ToInt32(myData["facing"]);
                    int rate = myData["averagerating"] == DBNull.Value ? 0 : Convert.ToInt32(myData["averagerating"]);
                    v.Rating = rate >= 0 ? (float)(rate / 20.0) : (float)0.0;
                    v.URL = myData["VideoUrl"] == DBNull.Value ? string.Empty : Convert.ToString(myData["VideoUrl"]);
                    v.ThumbNailURL = myData["thumbnailurl"] == DBNull.Value ? string.Empty : Convert.ToString(myData["thumbnailurl"]);
                    v.PlayCount = myData["PlayCount"] == DBNull.Value ? 0 : Convert.ToInt32(myData["PlayCount"]);
                    int VideoId = myData["ID"] == DBNull.Value ? 0 : Convert.ToInt32(myData["ID"]);
                    v.isPurchased = (VideoId == 0) ? false : true;
                }
                myData.Close();
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return v;
        }

        internal static List<VideoInfoDTO> GetVideos(int adminUserId)
        {
            List<VideoInfoDTO> videos = new List<VideoInfoDTO>();
            try
            {
                SqlCommand sqlCommand = new SqlCommand("[GetVideos]");
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@UserId", adminUserId);
                SqlDataReader reader;

                BaseUtils.executeReaderCommandOnSingleShard(33, sqlCommand, out reader);

                while (reader != null && reader.Read())
                {
                    VideoInfoDTO video = new VideoInfoDTO();
                    video.Id = reader["Id"] == DBNull.Value ? 0 : Convert.ToInt32(reader["Id"]);
                    video.MomentName = reader["MovementName"] == DBNull.Value ? string.Empty : Convert.ToString(reader["MovementName"]);
                    video.ActorName = reader["ActorName"] == DBNull.Value ? string.Empty : Convert.ToString(reader["ActorName"]);
                    video.VideoURL = reader["VideoURL"] == DBNull.Value ? string.Empty : Convert.ToString(reader["VideoURL"]);
                    video.ThumbNailURL = reader["ThumbNailURL"] == DBNull.Value ? string.Empty : Convert.ToString(reader["ThumbNailURL"]);
                    video.Status = reader["Status"] == DBNull.Value ? 0 : Convert.ToInt32(reader["Status"]);
                    video.CreatedDate = reader["CreateDate"] == DBNull.Value ? DateTime.Now : Convert.ToDateTime(reader["CreateDate"]);
                    video.IntroVideoURL = reader["IntroVideoURL"] == DBNull.Value ? string.Empty : Convert.ToString(reader["IntroVideoURL"]);
                    video.VideoMode = reader["VideoMode"] == DBNull.Value ? 0 : Convert.ToInt32(reader["VideoMode"]);
                    videos.Add(video);
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return videos;
        }

        internal static int DeleteVideo(int videoId)
        {
            int result = 0;

            try
            {
                MultiShardConnection multiShardConnection = BaseUtils.getMultiShardConnection();
                MultiShardCommand command = multiShardConnection.CreateCommand();
                command.CommandText = "[DeleteVideos]";
                command.CommandType = System.Data.CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@VideoId", videoId);
                MultiShardDataReader reader = BaseUtils.ExecuteReaderCommandOnMultiShard(command);

                while (reader != null && reader.Read())
                {
                    result = reader["result"] == DBNull.Value ? 0 : Convert.ToInt32(reader["result"]);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return result;
        }

        internal static int EditAdminVideo(VideoInfoDTO videoInfo)
        {
            int result = 0;
            try
            {
                MultiShardConnection multiShardConnection = BaseUtils.getMultiShardConnection();
                MultiShardCommand command = multiShardConnection.CreateCommand();
                command.CommandText = "[UpdateAdminVideo]";
                command.CommandType = System.Data.CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@ActorName", videoInfo.ActorName);
                command.Parameters.AddWithValue("@MomentName", videoInfo.MomentName);
                command.Parameters.AddWithValue("@CreatedBy", videoInfo.CreatedBy);
                command.Parameters.AddWithValue("@VideoMode", videoInfo.VideoMode);
                command.Parameters.AddWithValue("@Id", videoInfo.Id);
                MultiShardDataReader reader = BaseUtils.ExecuteReaderCommandOnMultiShard(command);

                while (reader != null && reader.Read())
                {
                    result = reader["result"] == DBNull.Value ? 0 : Convert.ToInt32(reader["result"]);
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return result;
        }

        internal static int AddAdminVideo(VideoInfoDTO videoInfo)
        {
            int result = 0;
            try
            {
                MultiShardConnection multiShardConnection = BaseUtils.getMultiShardConnection();
                MultiShardCommand command = multiShardConnection.CreateCommand();
                command.CommandText = "[CreateAdminVideo]";
                command.CommandType = System.Data.CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@VideoURL", videoInfo.VideoURL);
                command.Parameters.AddWithValue("@MomentName", videoInfo.MomentName);
                command.Parameters.AddWithValue("@CreatedBy", videoInfo.CreatedBy);
                command.Parameters.AddWithValue("@ThumbNailURL", videoInfo.ThumbNailURL);
                command.Parameters.AddWithValue("@VideoMode", videoInfo.VideoMode);
                command.Parameters.AddWithValue("@Id", videoInfo.Id);
                MultiShardDataReader reader = BaseUtils.ExecuteReaderCommandOnMultiShard(command);
               
                while (reader != null && reader.Read())
                {
                    result = reader["result"] == DBNull.Value ? 0 : Convert.ToInt32(reader["result"]);
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return result;
        }

        internal static List<PackagesInfoDTO> GetPackageIntroVideos(int libraryId, int userId, bool isPaidChannel)
        {
            List<PackagesInfoDTO> videos = new List<PackagesInfoDTO>();
            string price = string.Empty;
            try
            {
                SqlCommand sqlCommand = new SqlCommand("[GetPackagesIntroVideos]");
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@LibraryId", libraryId);
                sqlCommand.Parameters.AddWithValue("@UserId", userId);
                SqlDataReader myData;

                BaseUtils.executeReaderCommandOnSingleShard(userId, sqlCommand, out myData);
                while (myData != null && myData.Read())
                {
                    PackagesInfoDTO v = new PackagesInfoDTO();
                    v.PackageId = myData["PackageID"] == DBNull.Value ? 0 : Convert.ToInt32(myData["PackageID"]);
                    v.PackageName = myData["PackageName"] == DBNull.Value ? string.Empty : Convert.ToString(myData["PackageName"]);
                    v.Description = myData["Description"] == DBNull.Value ? string.Empty : Convert.ToString(myData["Description"]);
                    v.Format = myData["Format"] == DBNull.Value ? string.Empty : Convert.ToString(myData["Format"]);
                    v.LibraryId = myData["LibraryId"] == DBNull.Value ? 0 : Convert.ToInt32(myData["LibraryId"]);                    
                    v.VideoURL = myData["VideoURL"] == DBNull.Value ? string.Empty : Convert.ToString(myData["VideoURL"]);
                    v.ThumbNailURL = myData["thumbnailurl"] == DBNull.Value ? string.Empty : Convert.ToString(myData["thumbnailurl"]);
                    v.CreatedDate = myData["CreatedDate"] == DBNull.Value ? DateTime.Now : Convert.ToDateTime(myData["CreatedDate"]);
                    v.PricingCatalogId = myData["PricingCatalogId"] == DBNull.Value ? 0 : Convert.ToInt32(myData["PricingCatalogId"]);
                    v.Price = myData["Price"] == DBNull.Value ? string.Empty : Convert.ToString(myData["Price"]);
                    v.SkuId = myData["SKUID"] == DBNull.Value ? string.Empty : Convert.ToString(myData["SKUID"]);
                    v.IsFavourite = myData["IsFavourite"] == DBNull.Value ? 0 : (Convert.ToInt32(myData["IsFavourite"]) > 0 ? 1 : 0);
                    v.Rating = myData["Rating"] == DBNull.Value ? 0 : Convert.ToInt32(myData["Rating"]);
                    v.IsPaid = isPaidChannel ? 1 : (myData["IsPaid"] == DBNull.Value ? 0 : (Convert.ToInt32(myData["IsPaid"]) > 0 ? 1 : 0));
                    videos.Add(v);
                }
                myData.Close();
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return videos;
        }

        internal static List<PackagesInfoDTO> GetPackageIntroVideosV1(int libraryId, int userId, bool isPaidChannel)
        {
            List<PackagesInfoDTO> videos = new List<PackagesInfoDTO>();

            try
            {
                SqlCommand sqlCommand = new SqlCommand("[GetPackagesIntroVideos]");
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@LibraryId", libraryId);
                sqlCommand.Parameters.AddWithValue("@UserId", userId);
                SqlDataReader myData;

                BaseUtils.executeReaderCommandOnSingleShard(userId, sqlCommand, out myData);
                while (myData != null && myData.Read())
                {
                    PackagesInfoDTO v = new PackagesInfoDTO();
                    v.PackageId = myData["PackageID"] == DBNull.Value ? 0 : Convert.ToInt32(myData["PackageID"]);
                    v.PackageName = myData["PackageName"] == DBNull.Value ? string.Empty : Convert.ToString(myData["PackageName"]);
                    v.Description = myData["Description"] == DBNull.Value ? string.Empty : Convert.ToString(myData["Description"]);
                    v.Format = myData["Format"] == DBNull.Value ? string.Empty : Convert.ToString(myData["Format"]);
                    v.LibraryId = myData["LibraryId"] == DBNull.Value ? 0 : Convert.ToInt32(myData["LibraryId"]);
                    v.VideoURL = myData["VideoURL"] == DBNull.Value ? string.Empty : Convert.ToString(myData["VideoURL"]);
                    v.ThumbNailURL = myData["thumbnailurl"] == DBNull.Value ? string.Empty : Convert.ToString(myData["thumbnailurl"]);
                    v.CreatedDate = myData["CreatedDate"] == DBNull.Value ? DateTime.Now : Convert.ToDateTime(myData["CreatedDate"]);
                    v.PricingCatalogId = myData["PricingCatalogId"] == DBNull.Value ? 0 : Convert.ToInt32(myData["PricingCatalogId"]);
                    v.Price = myData["Price"] == DBNull.Value ? string.Empty : Convert.ToString(myData["Price"]);
                    v.SkuId = myData["SKUID"] == DBNull.Value ? string.Empty : Convert.ToString(myData["SKUID"]);
                    v.IsFavourite = myData["IsFavourite"] == DBNull.Value ? 0 : Convert.ToInt32(myData["IsFavourite"]);
                    v.Rating = myData["Rating"] == DBNull.Value ? 0 : Convert.ToInt32(myData["Rating"]);
                    v.IsPaid = isPaidChannel ? 1 : (myData["IsPaid"] == DBNull.Value ? 0 : Convert.ToInt32(myData["IsPaid"]));
                    videos.Add(v);
                }
                myData.Close();
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return videos;
        }

        internal static List<PackageVideosDTO> GetAllPackagesVideos(int packageId, int userId)
        {
            List<PackageVideosDTO> videos = new List<PackageVideosDTO>();

            try
            {
                SqlCommand sqlCommand = new SqlCommand("[GetVideosByPackageId]");
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@PackageId", packageId);
                sqlCommand.Parameters.AddWithValue("@UserId", userId);
                SqlDataReader myData;

                BaseUtils.executeReaderCommandOnSingleShard(userId, sqlCommand, out myData);
                while (myData != null && myData.Read())
                {
                    PackageVideosDTO v = new PackageVideosDTO();
                    v.ID = myData["ID"] == DBNull.Value ? 0 : Convert.ToInt32(myData["ID"]);
                    v.Format = myData["Format"] == DBNull.Value ? string.Empty : Convert.ToString(myData["Format"]);
                    v.MovementName = myData["movementname"] == DBNull.Value ? string.Empty : Convert.ToString(myData["movementname"]);
                    v.ActorName = myData["actorname"] == DBNull.Value ? string.Empty : Convert.ToString(myData["actorname"]);
                    v.URL = myData["VideoURL"] == DBNull.Value ? string.Empty : Convert.ToString(myData["VideoURL"]);
                    v.ThumbNailURL = myData["ThumbNailURL"] == DBNull.Value ? string.Empty : Convert.ToString(myData["ThumbNailURL"]);
                    v.PlayCount = myData["PlayCount"] == DBNull.Value ? 0 : Convert.ToInt32(myData["PlayCount"]);
                    v.CreatedDate = myData["CreatedDate"] == DBNull.Value ? DateTime.Now : Convert.ToDateTime(myData["CreatedDate"]);
                    v.InstructionsVideoURL = myData["IntroVideoUrl"] == DBNull.Value ? string.Empty : Convert.ToString(myData["IntroVideoUrl"]);
                    v.InstructionsVideoURL2 = myData["IntroVideoUrl2"] == DBNull.Value ? string.Empty : Convert.ToString(myData["IntroVideoUrl2"]);
                    v.PackageId = packageId;
                    videos.Add(v);
                }
                myData.Close();
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return videos;
        }
    }
}