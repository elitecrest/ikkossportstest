﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Net.Http;
using CopyMeSportsWebAPI.Models;
using CopyMeSportsWebAPI.ShardUtils;

namespace CopyMeSportsWebAPI.Controllers
{
    public class VideosController : ApiController
    {
        CustomResponse Result = new CustomResponse();
        // GET: Videos
        // #region [HttpGet] Methods
        [System.Web.Http.HttpGet]
        public CustomResponse AllVideos()
        {
            try
            {
                int userId = 0;
                if (BaseUtils.didUniqueKeyExists(Request.Headers, out userId))
                {
                    if (userId > 0)
                    {
                        var videos = VideosModel.AllVideos(userId);
                        if (videos.Count > 0)
                        {
                            Result.Status = CustomResponseStatus.Successful;
                            Result.Response = videos;
                            Result.Message = Constants.DETAILS_GET_SUCCESSFULLY;
                        }
                        else
                        {
                            Result.Status = CustomResponseStatus.UnSuccessful;
                            Result.Message = Constants.NO_RECORDS;
                        }
                    }
                }
                else
                {
                    Result.Status = CustomResponseStatus.UnSuccessful;
                    Result.Message = Constants.GENERIC_NO_HEADER_MSG;
                }
            }
            catch (Exception ex)
            {
                Result.Status = CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [System.Web.Http.HttpGet]
        public CustomResponse GetVideo(int videoId, string userName)
        {
            try
            {
                //if (UsersDAL.CheckAdminByEmail(userName))
                VideosInfoDTO video = VideosModel.GetVideo(videoId);

                if (video.ID > 0)
                {
                    Result.Status = CustomResponseStatus.Successful;
                    Result.Response = video;
                    Result.Message = Constants.DETAILS_GET_SUCCESSFULLY;
                }
                else
                {
                    Result.Status = CustomResponseStatus.UnSuccessful;
                    Result.Message = Constants.NO_RECORDS;
                }
            }
            catch (Exception ex)
            {
                Result.Status = CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [System.Web.Http.HttpGet]
        public CustomResponse VideoOfTheDay(int categoryId)
        {
            CustomResponse res = new CustomResponse();
            try
            {
                int userId = 0;
                if (BaseUtils.didUniqueKeyExists(Request.Headers, out userId))
                {
                    int DeviceId = 0;

                    //var headers = System.Web.HttpContext.Current.Request.Headers;

                    //if (headers.AllKeys.Contains("APPID"))
                    //{

                    //    string AppId = headers.GetValues("APPID").First();
                    //    string[] arryAppId = AppId.Split('_');
                    //    DeviceId = Convert.ToInt32(arryAppId[1]);
                    //}
                    //else if (headers.AllKeys.Contains("AppID"))
                    //{

                    //    string AppId = headers.GetValues("AppID").First();
                    //    string[] arryAppId = AppId.Split('_');
                    //    DeviceId = Convert.ToInt32(arryAppId[1]);
                    //}
                    var videooftheday = VideosModel.GetVideoOfTheDay(userId, categoryId, DeviceId);
                    if (videooftheday != null)
                    {
                        Result.Status = CustomResponseStatus.Successful;
                        Result.Response = videooftheday;
                        Result.Message = Constants.DETAILS_GET_SUCCESSFULLY;
                    }
                    else
                    {
                        Result.Status = CustomResponseStatus.UnSuccessful;
                        Result.Message = Constants.NO_RECORDS;
                    }
                }
                else
                {
                    Result.Status = CustomResponseStatus.UnSuccessful;
                    Result.Message = Constants.GENERIC_NO_HEADER_MSG;
                }
                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result; // javaScriptSerializer.Serialize(Result);
            }
        }
        // #endregion

        [System.Web.Http.HttpGet] // This api was not using now.
        public CustomResponse LibraryIntroVideo(int libraryId)
        {
            try
            {
                int userId = 0;
                if (BaseUtils.didUniqueKeyExists(Request.Headers, out userId))
                {
                    if (userId > 0)
                    {
                        var videos = VideosModel.GetLibraryIntroVideos(libraryId, userId);
                        if (videos != null)
                        {
                            Result.Status = CustomResponseStatus.Successful;
                            Result.Response = videos;
                            Result.Message = Constants.DETAILS_GET_SUCCESSFULLY;
                        }
                        else
                        {
                            Result.Status = CustomResponseStatus.UnSuccessful;
                            Result.Message = Constants.NO_RECORDS;
                        }
                    }
                }
                else
                {
                    Result.Status = CustomResponseStatus.UnSuccessful;
                    Result.Message = Constants.GENERIC_NO_HEADER_MSG;
                }
            }
            catch (Exception ex)
            {
                Result.Status = CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;

        }

        [System.Web.Http.HttpGet]
        public CustomResponse ChannelIntroVideo(int libraryId)
        {
            try
            {
                int userId = 0;
                if (BaseUtils.didUniqueKeyExists(Request.Headers, out userId))
                {
                    if (userId > 0)
                    {
                        var videos = VideosModel.GetChannelIntroVideos(libraryId, userId);
                        if (videos != null)
                        {
                            Result.Status = CustomResponseStatus.Successful;
                            Result.Response = videos;
                            Result.Message = Constants.DETAILS_GET_SUCCESSFULLY;
                        }
                        else
                        {
                            Result.Status = CustomResponseStatus.UnSuccessful;
                            Result.Message = Constants.NO_RECORDS;
                        }
                    }
                }
                else
                {
                    Result.Status = CustomResponseStatus.UnSuccessful;
                    Result.Message = Constants.GENERIC_NO_HEADER_MSG;
                }
            }
            catch (Exception ex)
            {
                Result.Status = CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;

        }

        [System.Web.Http.HttpGet]
        public CustomResponse PackageIntroVideos(int libraryId, bool isPaidChannel)
        {
            try
            {
                int userId = 0;
                if (BaseUtils.didUniqueKeyExists(Request.Headers, out userId))
                {
                    if (userId > 0)
                    {
                        var videos = VideosModel.GetPackageIntroVideos(libraryId, userId, isPaidChannel);
                        if (videos.Count > 0)
                        {
                            Result.Status = CustomResponseStatus.Successful;
                            Result.Response = videos;
                            Result.Message = Constants.DETAILS_GET_SUCCESSFULLY;
                        }
                        else
                        {
                            Result.Status = CustomResponseStatus.UnSuccessful;
                            Result.Message = Constants.NO_RECORDS;
                        }
                    }
                }
                else
                {
                    Result.Status = CustomResponseStatus.UnSuccessful;
                    Result.Message = Constants.GENERIC_NO_HEADER_MSG;
                }
            }
            catch (Exception ex)
            {
                Result.Status = CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;

        }

        [System.Web.Http.HttpGet]
        public CustomResponse GetAllPackageVideos(int packageId)
        {
            try
            {
                int userId = 0;
                if (BaseUtils.didUniqueKeyExists(Request.Headers, out userId))
                {
                    if (userId > 0)
                    {
                        var videos = VideosModel.GetAllPackagesVideos(packageId, userId);
                        if (videos.Count > 0)
                        {
                            Result.Status = CustomResponseStatus.Successful;
                            Result.Response = videos;
                            Result.Message = Constants.DETAILS_GET_SUCCESSFULLY;
                        }
                        else
                        {
                            Result.Status = CustomResponseStatus.UnSuccessful;
                            Result.Message = Constants.NO_RECORDS;
                        }
                    }
                }
                else
                {
                    Result.Status = CustomResponseStatus.UnSuccessful;
                    Result.Message = Constants.GENERIC_NO_HEADER_MSG;
                }
            }
            catch (Exception ex)
            {
                Result.Status = CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [System.Web.Http.HttpPost]
        public CustomResponse AddOrUpdateAdminVideo(VideoInfoDTO videoInfo)
        {
            try
            {
                int count = 0;
                if (videoInfo != null && videoInfo.Id > 0)
                {
                    count = VideosModel.EditAdminVideo(videoInfo);
                    if (count > 0)
                    {
                        Result.Status = CustomResponseStatus.Successful;
                        Result.Response = count;
                        Result.Message = Constants.VIDEO_UPDATED;
                    }
                    else
                    {
                        Result.Status = CustomResponseStatus.UnSuccessful;
                        Result.Message = Constants.VIDEO_NOT_UPDATED;
                    }
                }
                else
                {
                    count = VideosModel.AddAdminVideo(videoInfo);
                    if (count > 0)
                    {
                        Result.Status = CustomResponseStatus.Successful;
                        Result.Response = count;
                        Result.Message = Constants.VIDEO_ADDED;
                    }
                    else
                    {
                        Result.Status = CustomResponseStatus.UnSuccessful;
                        Result.Message = Constants.VIDEO_NOT_ADDED;
                    }
                }

            }
            catch (Exception ex)
            {
                Result.Status = CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [System.Web.Http.HttpGet]
        public CustomResponse GetAdminVideos(int adminUserId)
        {
            try
            {
                var videos = VideosModel.GetVideos(adminUserId);
                if (videos.Count > 0)
                {
                    Result.Status = CustomResponseStatus.Successful;
                    Result.Response = videos;
                    Result.Message = Constants.DETAILS_GET_SUCCESSFULLY;
                }
                else
                {
                    Result.Status = CustomResponseStatus.UnSuccessful;
                    Result.Message = Constants.NO_RECORDS;
                }

            }
            catch (Exception ex)
            {
                Result.Status = CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [System.Web.Http.HttpDelete]
        public CustomResponse DeleteVideo(int videoId)
        {
            try
            {
                var video = VideosModel.DeleteVideo(videoId);
                if (video > 0)
                {
                    Result.Status = CustomResponseStatus.Successful;
                    Result.Response = video;
                    Result.Message = Constants.DELETE_VIDEO_SUCCESS;
                }
                else
                {
                    Result.Status = CustomResponseStatus.UnSuccessful;
                    Result.Message = Constants.DELETE_VIDEO_UNSUCCESS;
                }
            }
            catch (Exception ex)
            {
                Result.Status = CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

    }
}