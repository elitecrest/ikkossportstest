﻿using CopyMeSportsWebAPI.Models;
using CopyMeSportsWebAPI.ShardUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CopyMeSportsWebAPI.Controllers
{
    public class TrainingController : ApiController
    {
        CustomResponse customResponse = new CustomResponse();
        // GET: Training
        [HttpGet]
        public CustomResponse TrainingPlanVideos(int TrainingPlanId)
        {
            try
            {
                int uId = 0;
                if (BaseUtils.didUniqueKeyExists(Request.Headers, out uId))
                {
                    List<VideosInfoDTO> videos = TrainingModel.GetTrainingPlanVideos(TrainingPlanId, uId);
                    if (videos.Count > 0)
                    {
                        customResponse.Status = CustomResponseStatus.Successful;
                        customResponse.Response = videos;
                        customResponse.Message = Constants.DETAILS_GET_SUCCESSFULLY;
                    }
                    else
                    {
                        customResponse.Status = CustomResponseStatus.UnSuccessful;
                        customResponse.Message = Constants.NO_RECORDS;
                    }
                }
                else
                {
                    customResponse.Status = CustomResponseStatus.UnSuccessful;
                    customResponse.Message = Constants.GENERIC_NO_HEADER_MSG;
                }
            }
            catch (Exception ex)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Message = ex.Message;
            }
            return customResponse;
        }

        [HttpGet]
        public CustomResponse GetEventTypes(int age, int gender)
        {
            try
            {
                int uId = 0;
                if (BaseUtils.didUniqueKeyExists(Request.Headers, out uId))
                {
                    List<EventTypeDTO> eventType = new List<EventTypeDTO>();
                    eventType = TrainingModel.GetEventTypes(age, gender, uId);
                    if (eventType.Count > 0)
                    {
                        customResponse.Status = CustomResponseStatus.Successful;
                        customResponse.Response = eventType;
                        customResponse.Message = Constants.DETAILS_GET_SUCCESSFULLY;
                    }
                    else
                    {
                        customResponse.Status = CustomResponseStatus.UnSuccessful;
                        customResponse.Message = Constants.NO_RECORDS;
                    }
                }
                else
                {
                    customResponse.Status = CustomResponseStatus.UnSuccessful;
                    customResponse.Message = Constants.GENERIC_NO_HEADER_MSG;
                }

            }
            catch (Exception ex)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Message = ex.Message;
            }
            return customResponse;
        }

        [HttpGet]
        public CustomResponse GetBestTimes(int eventid, int age, int gender)
        {
            try
            {
                int uId = 0;
                if (BaseUtils.didUniqueKeyExists(Request.Headers, out uId))
                {
                    List<BestTimesValuesDTO> BestTimes = new List<BestTimesValuesDTO>();
                    BestTimes = TrainingModel.GetBestTimes(eventid, age, gender, uId);
                    if (BestTimes.Count > 0)
                    {
                        customResponse.Status = CustomResponseStatus.Successful;
                        customResponse.Response = BestTimes;
                        customResponse.Message = Constants.DETAILS_GET_SUCCESSFULLY;
                    }
                    else
                    {
                        customResponse.Status = CustomResponseStatus.UnSuccessful;
                        customResponse.Message = Constants.NO_RECORDS;
                    }
                }
                else
                {
                    customResponse.Status = CustomResponseStatus.UnSuccessful;
                    customResponse.Message = Constants.GENERIC_NO_HEADER_MSG;
                }
            }
            catch (Exception ex)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Message = ex.Message;
            }
            return customResponse;
        }

        [HttpGet]
        public CustomResponse UserTrainingPlanDetails(int TrainingTemplateID, int UserId)
        {
            try
            {

                var TrainingPlanDetails = TrainingModel.GetUserTrainingPlanDetails(TrainingTemplateID, UserId);
                if (TrainingPlanDetails.TrainingPlanId > 0)
                {
                    customResponse.Status = CustomResponseStatus.Successful;
                    customResponse.Response = TrainingPlanDetails;
                    customResponse.Message = Constants.DETAILS_GET_SUCCESSFULLY;
                }
                else
                {
                    customResponse.Status = CustomResponseStatus.UnSuccessful;
                    customResponse.Message = Constants.NO_RECORDS;
                }
            }
            catch (Exception ex)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Message = ex.Message;
            }
            return customResponse;
        }

    }
}