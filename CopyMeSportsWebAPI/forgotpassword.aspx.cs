﻿using CopyMeSportsWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CopyMeSportsWebAPI
{
    public partial class forgotpassword : System.Web.UI.Page
    {
        CustomResponse _res = new CustomResponse();
        protected void Page_Load(object sender, EventArgs e)
        {
            lblmsg.Visible = false;
            txtPassword.Visible = true;
            btnSubmit.Visible = true;
            lblpwd.Visible = true;
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {

            // string header = Request.Headers.GetValues("AppID").ToString();
            string baseUrl = ConfigurationManager.AppSettings["BaseURL"];
            string token = Request.QueryString["token"];
            string username = Request.QueryString["username"];
            string password = txtPassword.Text.Trim();

            HttpClient client = new HttpClient { BaseAddress = new Uri(baseUrl) };
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var response = client.GetAsync("Users/ResetPassword?userName=" + username + "&token=" + token + "&confirmPassword=" + password).Result;
            if (response.IsSuccessStatusCode)
            {
                _res = response.Content.ReadAsAsync<CustomResponse>().Result;
                if (_res.Status == CustomResponseStatus.Successful)
                {

                    lblmsg.Visible = true;
                    lblmsg.Text = _res.Message;
                    lblmsg.ForeColor = System.Drawing.Color.Black;
                    txtPassword.Visible = false;
                    btnSubmit.Visible = false;
                    lblpwd.Visible = false;
                }
                else
                {
                    lblmsg.ForeColor = System.Drawing.Color.Red;
                    lblmsg.Visible = true;
                    lblmsg.Text = _res.Message;
                }

            }


        }
    }
}